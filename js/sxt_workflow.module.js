/**
 * @file sxt_workflow.module.js
 *
 * Defines functions of sxt_workflow toolbars.
 */
(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.sxt_workflow = {};
  var _xtwf = Drupal.sxt_workflow;


  /**
   * Sxt workflow namespace.
   */
  var xtwfExt = {
    getNodetypeWorkflows: function (nType) {
      var nodeTypes = drupalSettings.sxt_workflow.nodeTypes || {};
      return (nodeTypes[nType] || false);
    }
  };
  $.extend(true, _xtwf, xtwfExt);
  

})(jQuery, Drupal, drupalSettings);
