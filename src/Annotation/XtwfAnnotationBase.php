<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Annotation\XtwfState.
 */

namespace Drupal\sxt_workflow\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 */
abstract class XtwfAnnotationBase extends Plugin {
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label = '';

  /**
   * The plugin status (enabled/disabled).
   *
   * @var boolean
   */
  public $status = TRUE;

  /**
   * The default weight.
   *
   * @var int (optional)
   */
  public $weight = 0;
  
  /**
   * Additional settings of the plugin.
   *
   * @var array
   */
  public $settings = [];
  
}
