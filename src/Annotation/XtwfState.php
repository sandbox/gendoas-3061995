<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Annotation\XtwfState.
 */

namespace Drupal\sxt_workflow\Annotation;

/**
 * Defines Sxt Workflow State annotation object.
 *
 * @Annotation
 */
class XtwfState extends XtwfAnnotationBase {

  /**
   * Default settings for state plugins.
   */
  public $settings = [
    'state_type' => 'undefined',
    'is_subworkflow' => FALSE,
    'use_severity' => FALSE,
    'use_dynamic_severity' => FALSE,
  ];

}
