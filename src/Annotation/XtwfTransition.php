<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Annotation\XtwfTransition.
 */

namespace Drupal\sxt_workflow\Annotation;

/**
 * Defines Sxt Workflow Transition annotation object.
 *
 * @Annotation
 */
class XtwfTransition extends XtwfAnnotationBase {
  /**
   * Default settings for transition plugins.
   */
  public $settings = [
      'exclusive_from' => FALSE,
      'required_from' => [],
      'exclude_from' => [],
  ];

}
