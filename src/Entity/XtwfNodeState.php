<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Entity\XtwfNodeState.
 */

namespace Drupal\sxt_workflow\Entity;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\XtDateTimeTrait;
use Drupal\slogxt\XtDateFormaterTrait;
use Drupal\sxt_workflow\SxtWorkflowStateInterface;
use Drupal\sxt_workflow\XtwfNodeSubscribe;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the Content moderation state entity.
 *
 * @ContentEntityType(
 *   id = "xtwf_node_state",
 *   label = @Translation("Xtwf node state"),
 *   base_table = "xtwf_node_state",
 *   entity_keys = {
 *     "id" = "nid",
 *     "state" = "state",
 *     "workflow" = "workflow",
 *     "severity" = "severity",
 *     "expire" = "expire",
 *   }
 * )
 */
class XtwfNodeState extends ContentEntityBase implements XtwfNodeStateInterface {

  use XtDateTimeTrait;
  use XtDateFormaterTrait;

  protected $node;
  protected $state_workflow;
  protected $xtwf_type_plugin;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];
    $fields['nid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Node ID'))
        ->setReadOnly(TRUE)
        ->setRequired(TRUE)
        ->setSetting('unsigned', TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Xtwf state'))
        ->setDescription(t('The state of the referenced content. Maybe the state of a nested workflow.'))
        ->setRequired(TRUE);

    $fields['workflow'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Xtwf workflow'))
        ->setDescription(t('The root workflow of the referenced content. Maybe this is a nested workflow.'))
        ->setRequired(TRUE);

    $fields['severity'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Severity'))
        ->setDescription(t('The severity for the workflow.'))
        ->setRequired(TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
        ->setLabel(t('State data'))
        ->setDescription(t('A serialized array of the current state data.'));

    $fields['history'] = BaseFieldDefinition::create('map')
        ->setLabel(t('History data'))
        ->setDescription(t('A serialized array of the history data for past states.'));

    $fields['expire'] = BaseFieldDefinition::create('timestamp')
        ->setLabel(t('Expire'))
        ->setDescription(t('The timestamp where to perform a transition.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->setData(REQUEST_TIME, 'changed');
  }

  public function getValues() {
    return [
      'state' => $this->getStateId(),
      'workflow' => $this->getWorkflowId(),
      'severity' => $this->getSeverityId(),
      'data' => $this->getData(),
      'history' => $this->getHistory(),
      'expire' => $this->getExpire(),
    ];
  }

  public function setValues(array $values) {
    $values += $this->getValues();
    $this->setStateId($values['state'])
        ->setWorkflowId($values['workflow'])
        ->setSeverity($values['severity'])
        ->setData($values['data'])
        ->setHistory($values['history'])
        ->setExpire($values['expire']);
    return $this;
  }

  public function getStateId() {
    return $this->state->value;
  }

  public function setStateId($value) {
    $this->state->setValue($value);
    return $this;
  }

  public function resetCache() {
    $this->state_workflow = $this->xtwf_type_plugin = NULL;
  }

  public function getWorkflowId() {
    return $this->workflow->value;
  }

  public function setWorkflowId($value) {
    if ($value !== $this->workflow->value) {
      $this->workflow->setValue($value);
      $this->resetCache();
    }
    return $this;
  }

  public function getSeverityId() {
    return $this->severity->value;
  }

  public function setSeverity($value) {
    $this->severity->setValue($value);
    return $this;
  }

  public function getExpire() {
    return $this->expire->value;
  }

  public function setExpire($timestamp) {
    $this->expire->setValue($timestamp);
    return $this;
  }

  public function getNode() {
    if (empty($this->node)) {
      $this->node = Node::load($this->id());
    }
    return $this->node;
  }

  public function getNodeTypeId() {
    return $this->getNode()->getType();
  }

  public function buildNodeTitleByState() {
    $date_formater = $this->getXtDateFormater();
    $timestamp = $this->getStarted();
    $f_changed = $date_formater->format($timestamp, 'xt_date_only');
    $state_label = $this->getWfState()->label();

    return "$f_changed: $state_label";
  }

  public function getWorkflows() {
    return SlogXtwf::getWorkflows($this->getNodeTypeId(), $this->getWfTypeBundle());
  }

  public function getWorkflowLabels() {
    $workflows = $this->getWorkflows();
    return SlogXtwf::getWorkflowOptions($workflows);
  }

  public function getWorkflow() {
    if (empty($this->state_workflow)) {
      $this->state_workflow = SlogXtwf::getWorkflow($this->getWorkflowId());
    }
    return $this->state_workflow;
  }

  public function getWorkflowTypePlugin() {
    if (empty($this->xtwf_type_plugin)) {
      $this->xtwf_type_plugin = $this->getWorkflow()->getTypePlugin();
    }
    return $this->xtwf_type_plugin;
  }

  public function getWfTypeBundle() {
    return $this->getWorkflowTypePlugin()->getBundle();
  }

  public function getWfState($state_id = NULL) {
    if (empty($state_id)) {
      $state_id = $this->getStateId();
    }
    return $this->getWorkflowTypePlugin()->getState($state_id);
  }

  public function getWfStatePlugin($state_id = NULL) {
    if (empty($state_id)) {
      $state_id = $this->getStateId();
    }
    $state_plugin = SlogXtwf::getStatePlugin($state_id);
    $state_plugin->setNodeState($this);
    return $state_plugin;
  }

  public function getWfStateNumSuffix($state_id = NULL) {
    if (empty($state_id)) {
      $state_id = $this->getStateId();
    }
    $xtwf_state = $this->getWfState($state_id);
    return ucfirst($xtwf_state->getNumberSuffix());
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicRequired() {
    return (integer) $this->getData('dynamic_required');
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityRequired() {
    return $this->getWfState()->getSeverityRequired($this->getSeverityId(), $this->getDynamicRequired());
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityTimeout() {
    return $this->getWfState()->getSeverityTimeout($this->getSeverityId());
  }

  public function wfHasState($state_id) {
    return $this->getWorkflowTypePlugin()->hasState($state_id);
  }

  public function getRequestNotice() {
    $notice = '';
    if ($this->stateIsRequestType()) {
      $request_data = $this->getData('request_data') ?? [];
      $notice = $request_data['notice'] ?? '_???_getRequestNotice';
    }

    return $notice;
  }

  public function getDiscussionState() {
    return $this->getWfState(SlogXtwf::XTWF_DISCUSSION);
  }

  public function hasDiscussionState() {
    return $this->wfHasState(SlogXtwf::XTWF_DISCUSSION);
  }

  public function canDiscussRequest() {
    if ($this->stateIsRequestType() && $this->hasDiscussionState()) {
      $history_data = $this->getHistoryData($this->getStarted());
      if (!empty($history_data['state'])) {
        return ($history_data['state'] !== SlogXtwf::XTWF_DISCUSSION);
      }
      else {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function isDiscussForRequest() {
    if ($this->isStateDiscussion()) {
      $history_data = $this->getHistoryData($this->getStarted());
      return $this->getWfState($history_data['state'])->isRequestType();
    }

    return FALSE;
  }

  public function hasNextCompetition($from_state_id) {
    return $this->getWorkflowTypePlugin()->hasNextCompetition($from_state_id);
  }

  public function hasNextFinishRenewable($from_state_id) {
    return $this->getWorkflowTypePlugin()->hasNextFinishRenewable($from_state_id);
  }

  public function getNodeInfo() {
    $info = [];
    $node = $this->getNode();
    $base_info = $this->getStateBaseInfo(TRUE);
    $history = $this->getHistory();
    $uids = XtwfNodeSubscribe::getSubscribedUserIds($node->id());
    $args = [
      '@hcount' => count($history),
      '@ucount' => count($uids),
    ];

    $info[] = t('Node: ') . SlogXt::htmlHighlightText($node->label());
    $info[] = $base_info['workflow'];
    $info[] = $base_info['severity'];
    $info[] = $this->infoCurrentState(TRUE);
    $hinfo = t('@hcount items', $args);
    $info[] = t('History: ') . SlogXt::htmlHighlightText($hinfo);
    $uinfo = t('@ucount users', $args);
    $info[] = t('Current subscribed: ') . SlogXt::htmlHighlightText($uinfo);

    return implode('<br />', $info);
  }

  public function getStateInfo($verbose = TRUE, $prepend_cur_state = FALSE, $prepend_base = TRUE, $as_array = FALSE) {
    $info = $this->getWfStatePlugin()->getStateInfo($prepend_base);
    $info = is_array($info) ? $info : [$info];

    if (!$verbose && isset($info['description'])) {
      unset($info['description']);
    }

    if ($prepend_cur_state) {
      $current_state = $this->infoCurrentState(TRUE);
      array_unshift($info, $current_state);
    }

    if ($as_array) {
      $info = array_values($info);
      return $info;
    }

    $glue = $verbose ? '<br />' : ', ';
    return implode($glue, $info);
  }

  public function getStateBaseInfo($as_array = FALSE) {
    return $this->getWfStatePlugin()->getInfoBase($as_array);
  }

  public function getSubtitleInfo() {
    return $this->getWfStatePlugin()->getSubtitleInfo();
  }

  /**
   * Throw exception on state or workflow mismatch.
   * 
   * @param string $state_id
   * @param string $workflow_id
   * @throws \LogicException
   */
  public function assertState($state_id, $workflow_id) {
    if ($this->getStateId() !== $state_id || $this->getWorkflowId() !== $workflow_id) {
      $message = t('State or workflow mismatch in node state.');
      throw new \LogicException($message);
    }
  }

  public function getHistory($desc = FALSE) {
    $item = $this->history->get(0);
    if (empty($item)) {
      $this->history->set(0, []);
      $item = $this->history->get(0);
    }

    $history_data = $item->getValue();
    if ($desc) {
      krsort($history_data);
    }

    return $history_data;
  }

  public function getHistoryData($timestamp) {
    $history = $this->getHistory();
    return ($history[$timestamp] ?? []);
  }

  public function getHistoryStateIds($desc = FALSE) {
    $state_ids = [];
    $history_data = $this->getHistory($desc);
    foreach ($history_data as $timestamp => $values) {
      $state_ids[$timestamp] = $values['state'];
    }

    return $state_ids;
  }

  public function getPreviousHistoryData($state_id = NULL, $do_log = TRUE) {
    $history_data = FALSE;
    $hstate_ids = $this->getHistoryStateIds(TRUE);
    if (!empty($state_id)) {
      if (in_array($state_id, $hstate_ids)) {
        foreach ($hstate_ids as $timestamp => $hstate_id) {
          if ($hstate_id === $state_id) {
            $history_data = $this->getHistoryData($timestamp);
            break;
          }
        }
      }
    }
    elseif (!empty($hstate_ids)) {
      $timestamp = array_key_first($hstate_ids);
      $history_data = $this->getHistoryData($timestamp);
    }

    if (empty($history_data) && $do_log) {
      $error_msg = t('Logical error: history data not found.');
      SlogXtwf::logger()->error($error_msg);
    }

    return $history_data ?? FALSE;
  }

  public function setHistory($value) {
    if (empty($value) || !is_array($value)) {
      $value = [];
    }
    $this->history->set(0, $value);
    return $this;
  }

  public function getData($key = NULL) {
    $item = $this->data->get(0);
    if (empty($item)) {
      $this->data->set(0, []);
      $item = $this->data->get(0);
    }

    $data = $item->getValue();
    if (!is_array($data)) {
      $message = t('Value of data is not an array, @type given', ['@type' => gettype($data)]);
      throw new \LogicException($message);
    }

    if (!$this->stateIsFinishedType()) {
      $data += [
        'user_data' => [], // data by user, is deleted by state finalize
        'state_data' => [], // cumulated data, not by user
        'finalize_by_timeout' => FALSE,
        'finalize_strict' => FALSE,
      ];
    }

    if (!empty($key)) {
      return isset($data[$key]) ? $data[$key] : NULL;
    }

    return ($data ?? []);
  }

  public function resetStateDataAll($preserve_base = FALSE) {
    $preserved_data = [];
    if ($preserve_base) {
      $data = $this->getData();
      $preserve_keys = ['finalize_by_timeout', 'finalize_strict'];
      if (!empty($data['preserve_prepared'])) {
        $preserve_keys = array_merge($preserve_keys, $data['preserve_prepared']);
      }
      foreach ($preserve_keys as $key) {
        if (isset($data[$key])) {
          $preserved_data[$key] = $data[$key];
        }
      }
    }
    else {
      $this->setHistory([]);
    }

    $this->setData($preserved_data);
    return $this;
  }

  public function resetStateDataState() {
    $data = $this->getData();
    $data['started'] = [];
    $data['user_data'] = [];
    $data['state_data'] = [];
    $this->setData($data);
    return $this;
  }

  public function getStarted() {
    return $this->getData('started');
  }

  public function getChanged() {
    return $this->getData('changed');
  }

  /**
   * Whether this is a node state with history
   * 
   * @return boolean
   */
  public function hasHistory() {
    $history = $this->getHistory();
    return !empty($history);
  }

  /**
   * Whether state is a past state (from history).
   * 
   * Each history state has the data field 'finalized_by'.
   * 
   * @return boolean
   */
  public function isHistoryState() {
    return !empty($this->getData('finalized_by'));
  }

  /**
   * Remove additional values added by setHistoryStateData
   * 
   * @param array $data
   * @return array
   */
  protected function cleanHistoryStateData(array $data) {
    $keys = ['finalized_by', 'admin_data', 'generated'];
    foreach ($keys as $key) {
      if (isset($data[$key])) {
        unset($data[$key]);
      }
    }
    return $data;
  }

  protected function setHistoryStateData($values, $finalized_by, $args) {
    $history = $values['history'] ?? [];
    unset($values['history']);

    $values['data']['finalized_by'] = $finalized_by;
    if (is_array($values['data']['user_data'])) {
      $values['data']['user_data'] = count($values['data']['user_data']);
    }
    if ($finalized_by === 'admin') {
      $admin_data = ['admin_uid' => \Drupal::currentUser()->id()] + $args['admin_data'];
      $values['data']['admin_data'] = $admin_data;
    }

    $history_key = $args['history_key'];
    $history[$history_key] = $values;

    $this->setHistory($history);
    return $this;
  }

  public function setData($value, $key = NULL) {
    if (!empty($key)) {
      $data = $this->getData();
      $data[$key] = $value;
    }
    else {
      if (!is_array($value)) {
        $message = t('Value of data must be an array, @type given', ['@type' => gettype($data)]);
        throw new \LogicException($message);
      }

      $data = $value;
    }

    $data += [
      'started' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    ];
    $this->data->set(0, $data);
    return $this;
  }

  public function getStateData() {
    return $this->getData('state_data');
  }

  public function setStateData($state_data) {
    if (is_array($state_data)) {
      $this->setData($state_data, 'state_data');
    }
    return $this;
  }

  public function getUserData($uid = NULL) {
    $user_data = $this->getData('user_data');
    if ($uid && $uid < 0) {
      $uid = \Drupal::currentUser()->id();
    }
    if (!empty($uid)) {
      return ($user_data[$uid] ?? FALSE);
    }

    return ($user_data ?? []);
  }

  public function hasUserData($uid = NULL) {
    $user_data = $this->getUserData($uid);
    return !empty($user_data);
  }

  public function setUserData($uid = NULL, array $value = []) {
    if (!empty($uid)) {
      $uid = (integer) $uid;
      if ($uid < 0) {
        $this->setData($value, 'user_data');
      }
      else {
        $this->getWfStatePlugin()->setUserData($uid, $value);
      }
    }
    return $this;
  }

  public function deleteUserData($uid, $preserve = FALSE) {
    $this->getWfStatePlugin()->deleteUserData($uid, $preserve);
  }

  public function shareUserData($uid) {
    if ($this->stateIsCollectContentType()) {
      return $this->getWfStatePlugin()->shareUserData($uid);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stateIsRequestType() {
    return $this->getWfState()->isRequestType();
  }

  /**
   * {@inheritdoc}
   */
  public function stateIsCollectContentType() {
    return $this->getWfState()->isCollectContentType();
  }

  /**
   * {@inheritdoc}
   */
  public function stateIsCompetitionType() {
    return $this->getWfState()->isCompetitionType();
  }

  /**
   * {@inheritdoc}
   */
  public function stateIsFinishedType() {
    return $this->getWfState()->isFinishedType();
  }

  /**
   * {@inheritdoc}
   */
  public function isState($state_id) {
    return ($this->getStateId() === $state_id);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateRequestCollab() {
    return $this->isState(SlogXtwf::XTWF_REQUEST_COLLABORATE);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateRequestFinishFinally() {
    return $this->isState(SlogXtwf::XTWF_REQUEST_FINISH_FINALLY);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateRequestRenew() {
    return $this->isState(SlogXtwf::XTWF_REQUEST_RENEW);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateFinishedRenewable() {
    return $this->isState(SlogXtwf::XTWF_FINISH_RENEWABLE);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateFinishedFinally() {
    return $this->isState(SlogXtwf::XTWF_FINISH_FINALLY);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateCompetition() {
    return $this->isState(SlogXtwf::XTWF_COMPETITION);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateDiscussion() {
    return $this->isState(SlogXtwf::XTWF_DISCUSSION);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateCollectContent() {
    return $this->isState(SlogXtwf::XTWF_COLLECT_CONTENT);
  }

  /**
   * {@inheritdoc}
   */
  public function isStateFreeContribution() {
    return $this->isState(SlogXtwf::XTWF_FREE_CONTRIB);
  }

  public function isLastRequestVote() {
    if ($this->stateIsRequestType()) {
      return ($this->getPostsRelevant() === 1);
    }

    return FALSE;
  }

  public function setAdminData($data) {
    if (!is_array($data)) {
      $message = t('Admin data is not an array, @type given', ['@type' => gettype($data)]);
      throw new \LogicException($message);
    }
    if (empty($data['admin_uid'])) {
      $data['admin_uid'] = \Drupal::currentUser()->id();
    }

    $this->setData($data, 'admin_data');
    return $this;
  }

  public function getAdminData($key = NULL) {
    $admin_data = $this->getData('admin_data');
    if (!empty($key)) {
      return ($admin_data[$key] ?? FALSE);
    }
    return ($admin_data ?? []);
  }

  public function getAdminAction() {
    return $this->getAdminData('admin_action');
  }

  public function isAdminAction($action) {
    return ($this->getAdminAction() === $action);
  }

  public function getCompetitionData($key = NULL) {
    $competition_data = $this->getData('competition_data');
    if (!empty($key)) {
      return ($competition_data[$key] ?? FALSE);
    }
    return ($competition_data ?? []);
  }

  /**
   * {@inheritdoc}
   */
  public function canRequestFinishFinally() {
    if ($this->isStateFinishedRenewable()) {
      return $this->wfHasState(SlogXtwf::XTWF_REQUEST_FINISH_FINALLY);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function canRequestRenew() {
    if ($this->isStateFinishedRenewable() || $this->isStateRequestRenew()) {
      return $this->wfHasState(SlogXtwf::XTWF_REQUEST_RENEW);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFinalizeByTimeout() {
    return (boolean) $this->getData('finalize_by_timeout');
  }

  public function getFinalizeByTimeoutKey($by_timeout = NULL) {
    if (!isset($by_timeout)) {
      $by_timeout = $this->getFinalizeByTimeout();
    }
    return (!empty($by_timeout) ? 'timeout' : 'requests');
  }

  public function getFinalizeByTimeoutFlag($key) {
    return (isset($key) && $key === 'timeout');
  }

  public function getFinalizeStrict() {
    return (boolean) $this->getData('finalize_strict');
  }

  public function getFinalizeStrictKey($strict = NULL) {
    if (!isset($strict)) {
      $strict = $this->getFinalizeStrict();
    }
    return (!empty($strict) ? SlogXtwf::XTWF_SELECT_YES : SlogXtwf::XTWF_SELECT_NO);
  }

  public function getFinalizeStrictFlag($key) {
    return (isset($key) && $key === SlogXtwf::XTWF_SELECT_YES);
  }

  public function getDiscussFirstKey($discuss = NULL) {
    return (!empty($discuss) ? SlogXtwf::XTWF_SELECT_YES : SlogXtwf::XTWF_SELECT_NO);
  }

  public function getDiscussFirstFlag($key) {
    return (isset($key) && $key === SlogXtwf::XTWF_SELECT_YES);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionsRequestRenew() {
    $options = [];
    if (!$this->isStateFinishedRenewable()) {
      $options[SlogXtwf::XTWF_SELECT_NO] = (string) t('No, do NOT renew collaboration');
    }
    $renewable = $this->getStatesRenewable();
    foreach ($renewable as $state_id => $xtwf_state) {
      $options[$state_id] = $xtwf_state->label();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getPostsRelevant() {
    return $this->getWfStatePlugin()->getPostsRelevant();
  }

  /**
   * {@inheritdoc}
   */
  public function hasAnyPosts() {
    $posts = $this->getPostsRelevant();
    return !empty($posts);
  }

  /**
   * {@inheritdoc}
   */
  public function hasRequiredPosts() {
    $required = $this->getSeverityRequired();
    $posts_number = $this->getPostsRelevant();
    return ($required > 0 && $posts_number >= $required);
  }

  /**
   * {@inheritdoc}
   */
  public function infoCurrentState($highlight = FALSE) {
    $state_label = $this->getWfState()->label();
    if ($highlight) {
      $state_label = SlogXt::htmlHighlightText($state_label);
    }
    return t('Current state: ') . $state_label;
  }

  /**
   * {@inheritdoc}
   */
  public function collabRenewByAdmin($args) {
    $success = $this->collaborateRenew('admin', $args);
    $msg = $success ? t('Collaboration has been renewed.') : t('Error on renewing collaboration.');
    if ($success) {
      if ($args['renew_state_id'] === SlogXtwf::XTWF_DISCUSSION) {
        $msg = t('Discussion has been started for renewing collaboration.');
      }
      elseif ($args['renew_state_id'] === SlogXtwf::XTWF_SELECT_NO) {
        $msg = t('Request for renewing has been canceled.');
      }
    }
    return ['success' => $success, 'message' => $msg];
  }

  /**
   * {@inheritdoc}
   */
  public function restoreFromHistoryByAdmin($args) {
    $success = $this->restoreFromHistory('admin', $args);
    $msg = $success ? t('State has been restored from history.') : t('Error on restoring from history.');
    return ['success' => $success, 'message' => $msg];
  }

  /**
   * {@inheritdoc}
   */
  public function stateFinalizeByAdmin($args) {
    $success = $this->stateFinalize('admin', $args);
    $msg = $success ? t('State has been finalized.') : t('Error on finalizing state.');
    return ['success' => $success, 'message' => $msg];
  }

  /**
   * {@inheritdoc}
   */
  public function stateTerminateByAdmin($args) {
    $success = $this->stateFinalize('admin', $args);
    $msg = $success ? t('Collaboration has been terminated.') : t('Error on terminating collaboration.');
    if ($success && $this->isStateFinishedFinally()) {
      $msg = t('Collaboration has been terminated permanently.');
    }
    return ['success' => $success, 'message' => $msg];
  }

  /**
   * {@inheritdoc}
   */
  public function stateFinalizeByTimeout() {
    $args = [];
    if ($this->stateIsRequestType()) {
      $wf_state_plugin = $this->getWfStatePlugin();
      $finalize_id = $wf_state_plugin->getPreferredBest('finalize');
      $strict_id = $wf_state_plugin->getPreferredBest('strict');
      $discuss_id = $wf_state_plugin->getPreferredBest('discuss');
      $args = [
        'workflow' => $wf_state_plugin->getPreferredBest('workflow'),
        'severity' => $wf_state_plugin->getPreferredBest('severity'),
        'finalize_by_timeout' => $this->getFinalizeByTimeoutFlag($finalize_id),
        'finalize_strict' => $this->getFinalizeStrictFlag($strict_id),
        'discuss_first' => $this->getDiscussFirstFlag($discuss_id),
      ];
    }
    $success = $this->stateFinalize('timeout', $args);
    $msg = $success ? t('State has been finalized.') : t('Error on finalizing state.');
    return ['success' => $success, 'message' => $msg];
  }

  /**
   * {@inheritdoc}
   */
  public function stateFinalizeByCancelRequest() {
    $success = $this->stateFinalize('cancel_request');
    $msg = $success ? t('The request has been aborted.') : t('Error on on aborting request.');
    return ['success' => $success, 'message' => $msg];
  }

  /**
   * {@inheritdoc}
   */
  public function checkFinalizeByRequired() {
    if (!$this->getFinalizeByTimeout() && $this->hasRequiredPosts()) {
      $success = $this->stateFinalize('required');
      $msg = $success ? t('State has been finalized.') : t('Error on finalizing state.');
      return ['success' => $success, 'message' => $msg];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatesRenewable() {
    $renewable = [];
    $xtwf_states = $this->getWorkflowTypePlugin()->getStatesContentCollect();
    $hstate_ids = $this->getHistoryStateIds();

    if (!empty($xtwf_states) && !empty($hstate_ids)) {
      foreach ($xtwf_states as $state_id => $xtwf_state) {
        if (in_array($state_id, $hstate_ids)) {
          $renewable[$state_id] = $xtwf_state;
        }
      }
    }

    $state_id = SlogXtwf::XTWF_COMPETITION;
    if (empty($renewable) && in_array($state_id, $hstate_ids)) {
      $xtwf_state = $this->getWfState($state_id);
      $renewable[$state_id] = $xtwf_state;
    }

    if (empty($renewable)) {
      $state_id = SlogXtwf::XTWF_REQUEST_COLLABORATE;
      $xtwf_state = $this->getWfState($state_id);
      $renewable[$state_id] = $xtwf_state;
    }

    return $renewable;
  }

  /**
   * {@inheritdoc}
   */
  protected function stateFinalize($finalized_by, array $args = []) {
    if (!in_array($finalized_by, ['required', 'timeout', 'admin', 'cancel_request'])) {
      $message = t('Not a valid parameter $finalized_by:  @finalized_by', ['@finalized_by' => $finalized_by]);
      throw new \InvalidArgumentException($message);
    }

    $rollback_values = $this->getValues();
    $error_msg = FALSE;
    try {
      // read required befor changing workflow
      $action = $args['action'] ?? FALSE;
      $on_admin_terminate = ($action === 'terminate' && !empty($args['admin_data']));
      $strict = $this->getFinalizeStrict();
      $num_required = $this->getSeverityRequired();
      $has_required = $this->hasRequiredPosts();
      $wf_state_plugin = $this->getWfStatePlugin();
      $this->resetCache();

      if (!$on_admin_terminate && $this->stateIsRequestType()) {
        if (!empty($args['workflow'])) {
          $this->setWorkflowId($args['workflow']);
        }
        if (!empty($args['severity'])) {
          $this->setSeverity($args['severity']);
        }
        if ($wf_state_plugin->toDiscuss()) {
          $args['discuss_first'] = TRUE;
        }
      }

      if ($on_admin_terminate) {
        $next_state = $wf_state_plugin->preFinalize($finalized_by, $args);
      }
      elseif ($this->isStateDiscussion()) {
        $request_data = [
          'from_state' => $this->getStateId(),
          'notice' => (string) t('Resumed after discussion'), 1
        ];
        $history_data = $this->getHistoryData($this->getStarted());
        $next_state = $this->getWfState($history_data['state'] ?? FALSE);
        if (!$next_state) {
          throw new \LogicException('State not found after discussion.');
        }
      }
      elseif (!empty($args['discuss_first'])) {
        $next_state = $this->getWfState(SlogXtwf::XTWF_DISCUSSION);
      }
      elseif ($num_required > 0 && $strict && !$has_required) {
        $next_state = $this->getWfState(SlogXtwf::XTWF_FINISH_RENEWABLE);
      }
      elseif ($this->stateIsRequestType() && $finalized_by === 'cancel_request') {
        $next_state = $this->getWfState(SlogXtwf::XTWF_FINISH_RENEWABLE);
      }
      else {
        $next_state = $wf_state_plugin->preFinalize($finalized_by, $args);
      }

      if (empty($next_state)) {
        $next_state = $this->findNextState();
      }

      if (empty($next_state) || !$next_state instanceof SxtWorkflowStateInterface) {
        $args = [
          '@workflow' => $this->getWorkflowId(),
          '@state' => $this->getStateId(),
          '@competition' => !empty($competitions) ? 'TRUE' : 'FALSE',
        ];
        $message = t('There is no next reachable state:  @workflow/@state/competition=@competition', $args);
        throw new \Exception($message);
      }

      $severity_id = $args['severity'] ?? $this->getSeverityId();
      $new_values = [
        'state' => $next_state->id(),
        'expire' => $this->getExpireTimestamp($next_state->getSeverityTimeout($severity_id)),
      ];

//    ********************************************************
//todo::current::debug::tmp
      // prevent changes for debug
//      $xx = 0;
//      if (TRUE /* todo::current::debug */) {
//        $xx_history = $this->getHistory();
//        $xx_values = $this->getValues();
//      throw new \LogicException('...for debug.....stateFinalize');
//      }
//    ********************************************************

      $finalize_by_timeout = (boolean) $this->getData('finalize_by_timeout');
      if (isset($args['finalize_by_timeout'])) {
        $finalize_by_timeout = (boolean) $args['finalize_by_timeout'];
      }
      $finalize_strict = (boolean) $this->getData('finalize_strict');
      if (isset($args['finalize_strict'])) {
        $finalize_strict = (boolean) $args['finalize_strict'];
      }

      $started = $args['history_key'] = REQUEST_TIME;
      $this->setHistoryStateData($rollback_values, $finalized_by, $args)
          ->resetStateDataAll(TRUE)
          ->setValues($new_values)
          ->setData($started, 'started')
          ->setData($finalize_by_timeout, 'finalize_by_timeout')
          ->setData($finalize_strict, 'finalize_strict');
      if ($this->stateIsFinishedType()) {
        $data = $this->getData();
        unset($data['user_data']);
        unset($data['state_data']);
        $this->setData($data);
      }
      elseif ($this->stateIsRequestType() && !empty($request_data)) {
        $this->setData($request_data, 'request_data');
      }
      elseif (!empty($args['discuss_first']) && !empty($args['admin_data'])) {
        $this->setAdminData($args['admin_data']);
      }
      $this->save();

      $wf_state_plugin->postFinalize();




//todo::current::debug::tmp
//      $this->resetCache();
//      
//      
//      if (TRUE /* todo::current::debug */) {
//        $xx_history = $this->getHistory();
//        $xx_values = $this->getValues();
//        throw new \LogicException('...for debug.....stateFinalize');
//      }
    }
    catch (\InvalidArgumentException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\LogicException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\Exception $e) {
      $error_msg = $e->getMessage();
    }
    finally {
      if (!empty($error_msg)) {
        $this->setValues($rollback_values)->save();
        SlogXtwf::logger()->error($error_msg);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeRequest($request_state_id, $notice) {
    if (!$this->isStateFinishedRenewable()) {
      $message = t('New request ist supported for finished renewable only.');
      throw new \LogicException($message);
    }

    $xtwf_state = $this->getWfState($request_state_id);
    if (!$xtwf_state || !$xtwf_state->isRequestType()) {
      $message = t('Not a valid parameter $request_state_id:  @request_state_id', ['@request_state_id' => $request_state_id]);
      throw new \InvalidArgumentException($message);
    }

    $rollback_values = $this->getValues();
    $error_msg = FALSE;
    try {
      $this->resetCache();


      $new_values = [
        'state' => $request_state_id,
        'expire' => $this->getExpireTimestamp($xtwf_state->getSeverityTimeout($this->getSeverityId())),
      ];

      $request_data = [
        'from_state' => $this->getStateId(),
        'notice' => $notice,
      ];

      $finalized_by = 'request';
      $started = $args['history_key'] = REQUEST_TIME;
      $this->setHistoryStateData($rollback_values, $finalized_by, $args)
          ->resetStateDataAll(TRUE)
          ->setValues($new_values)
          ->setData($started, 'started')
          ->setData($request_data, 'request_data');

//    ********************************************************
//todo::current::debug::tmp
      // prevent changes for debug
//      $xx = 0;
//      if (TRUE /* todo::current::debug */) {
//        $xx_values = $this->getValues();
//        throw new \LogicException('...for debug...initializeRequest');
//      }
//    ********************************************************
    }
    catch (\InvalidArgumentException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\LogicException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\Exception $e) {
      $error_msg = $e->getMessage();
    }
    finally {
      if (!empty($error_msg)) {
        $this->setValues($rollback_values);
        SlogXtwf::logger()->error($error_msg);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function collaborateRenew($finalized_by, $args) {
    if (!in_array($finalized_by, ['required', 'admin'])) {
      $message = t('Not a valid parameter $finalized_by:  @finalized_by', ['@finalized_by' => $finalized_by]);
      throw new \InvalidArgumentException($message);
    }

    $required = ['severity', 'finalize_by_timeout', 'finalize_strict'];
    if ($finalized_by === 'admin') {
      $required[] = 'renew_state_id';
    }
    foreach ($required as $key) {
      if (!isset($args[$key])) {
        $message = t('Required arg not found:  @key', ['@key' => $key]);
        throw new \InvalidArgumentException($message);
      }
    }

    $rollback_values = $this->getValues();
    $error_msg = FALSE;
    try {
      $this->resetCache();

      $wf_state_plugin = $this->getWfStatePlugin();
      if ($finalized_by === 'admin') {
        $next_state_id = $args['renew_state_id'];

        if ($next_state_id === SlogXtwf::XTWF_SELECT_NO) {
          //  ****************************************
          //  ****************************************
          $args['action'] = $args['admin_data']['admin_action'] = 'abort';
          return $this->stateFinalize('admin', $args);
          //  ****************************************
          //  ****************************************
        }
        elseif (!empty($args['discuss_first'])) {
          $next_state = $this->getWfState(SlogXtwf::XTWF_DISCUSSION);
          $new_values = [
            'state' => $next_state->id(),
            'expire' => $this->getExpireTimestamp($next_state->getSeverityTimeout($args['severity'])),
          ];
        }
        else {
          $new_values = $this->getPreviousHistoryData($next_state_id);
          $new_values['data'] = $this->cleanHistoryStateData($new_values['data']);
        }
      }
      else {
        $next_state = $wf_state_plugin->preFinalize($args);
        $new_values = [
          'state' => $next_state->id(),
          'expire' => $this->getExpireTimestamp($next_state->getSeverityTimeout($args['severity'])),
        ];
      }



//    ********************************************************
//todo::current::debug::tmp
//      prevent changes for debug
//      $xx = 0;
//      if (TRUE /* todo::current::debug */) {
//        $xx_history = $this->getHistory();
//        $xx_values = $this->getValues();
//        throw new \LogicException('...for debug......collaborateRenew');
//      }
//    ********************************************************
      //
      $new_values['severity'] = $args['severity'];
      $finalize_by_timeout = (boolean) $args['finalize_by_timeout'];
      $finalize_strict = (boolean) $args['finalize_strict'];
      $started = $args['history_key'] = REQUEST_TIME;
      $this->setHistoryStateData($rollback_values, $finalized_by, $args)
          ->setValues($new_values)
          ->resetStateDataState()
          ->setData($started, 'started')
          ->setData($finalize_by_timeout, 'finalize_by_timeout')
          ->setData($finalize_strict, 'finalize_strict')
          ->save();



//todo::current::debug::tmp
//      prevent changes for debug
//      $xx = 0;
//      if (TRUE /* todo::current::debug */) {
//        $xx_history = $this->getHistory();
//        $xx_values = $this->getValues();
//        throw new \LogicException('...for debug......collaborateRenew');
//      }
//    ********************************************************
    }
    catch (\InvalidArgumentException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\LogicException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\Exception $e) {
      $error_msg = $e->getMessage();
    }
    finally {
      if (!empty($error_msg)) {
        $this->setValues($rollback_values)->save();
        SlogXtwf::logger()->error($error_msg);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function restoreFromHistory($restored_by, $args) {
    if ($restored_by !== 'admin') {
      $message = t('Restore from history not allowed for:  @restored_by', ['@restored_by' => $restored_by]);
      throw new \InvalidArgumentException($message);
    }

    $rollback_values = $this->getValues();
    $error_msg = FALSE;
    try {
      $this->resetCache();

      $args['action'] = 'restore';
      $restore_history_key = (integer) $args['restore_history_key'];
      $new_values = $this->getHistoryData($restore_history_key);
      if (empty($new_values)) {
        $args = [
          '@node_id' => $this->id(),
          '@history_key' => $restore_history_key,
        ];
        $message = t('History data not found:  @node_id/@history_key', $args);
        throw new \LogicException($message);
      }

      $new_values['data'] = $this->cleanHistoryStateData($new_values['data']);
      $new_state_id = $new_values['state'];
      $wf_state_next = $this->getWfState($new_state_id);
      $finalize_by_timeout = $new_values['data']['finalize_by_timeout'];
      $finalize_strict = $new_values['data']['finalize_strict'];
      if (!empty($args['severity'])) {
        $new_values['severity'] = $args['severity'];
        $finalize_by_timeout = (boolean) $args['finalize_by_timeout'];
        $finalize_strict = (boolean) $args['finalize_strict'];
        $timestamp = $wf_state_next->getSeverityTimeout($args['severity']);
        $new_values['expire'] = $wf_state_next->getExpireTimestamp($timestamp);
      }

      // prepare
      $this->getWfStatePlugin($new_state_id)->prepareRestoredData($new_values['data'], $args);

//    ********************************************************
//todo::current::debug::tmp
      // prevent changes for debug
//      $xx = 0;
//      if (TRUE /* todo::current::debug */) {
//        $xx_history = $this->getHistory();
//        $xx_values = $this->getValues();
//        throw new \LogicException('...for debug....restoreFromHistory');
//      }
//    ********************************************************

      $started = $args['history_key'] = REQUEST_TIME;
      $this->setHistoryStateData($rollback_values, $restored_by, $args)
          ->setValues($new_values)
          ->resetStateDataState()
          ->setData($started, 'started')
          ->setData($finalize_by_timeout, 'finalize_by_timeout')
          ->setData($finalize_strict, 'finalize_strict')
          ->setAdminData($args['admin_data'])
          ->save();
      $this->resetCache();

//todo::current::debug::tmp
//      $this->resetCache();
//      $xx_history = $this->getHistory();
//      $xx_values = $this->getValues();
//
//      if (TRUE) {
//        $args = [
//          '@node_id' => $this->id(),
//          '@history_key' => $restore_history_key,
//        ];
//        $message = t('....debug..:  @node_id/@history_key', $args);
//        throw new \LogicException($message);
//      }
    }
    catch (\InvalidArgumentException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\LogicException $e) {
      $error_msg = $e->getMessage();
    }
    catch (\Exception $e) {
      $error_msg = $e->getMessage();
    }
    finally {
      if (!empty($error_msg)) {
        $this->setValues($rollback_values)->save();
        SlogXtwf::logger()->error($error_msg);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function findNextState() {
    $next_state_id = $this->getWfStatePlugin()->prepareNextState();
    if (!empty($next_state_id)) {
      return $this->getWfState($next_state_id);
    }

    return $this->getWorkflowTypePlugin()->findNextState($this->getStateId(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function unsubscribeAll() {
    $node_ids = [$this->id()];
    XtwfNodeSubscribe::unsubscribeForNodes($node_ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    XtwfNodeSubscribe::unsubscribeForNodes(array_keys($entities));
  }

}
