<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Entity\XtwfNodeStateInterface.
 */


namespace Drupal\sxt_workflow\Entity;

/**
 *
 */
interface XtwfNodeStateInterface {
  
  /**
   * Whether the current state is a request state.
   */
  public function stateIsRequestType();
      
  public function stateIsCollectContentType();
  
  public function stateIsCompetitionType();
  
}
