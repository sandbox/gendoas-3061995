<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Event\XtwfEventSubscriber.
 */

namespace Drupal\sxt_workflow\Event;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Sxt workflow event subscriber.
 */
class XtwfEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 9999];

    return $events;
  }

  public function onKernelResponse(Event $event) {
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse && !SlogXt::isAdminRoute()) {
      $to_add['drupalSettings']['sxt_workflow']['nodeTypes'] = SlogXtwf::getNodetypesWithWorkflows();
      $response->addAttachments($to_add);
    }
  }

}
