<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Form\Settings\XtwfStateSeverityForm.
 */

namespace Drupal\sxt_workflow\Form\Settings;

use Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfPluginStateBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * The form for editing entity types associated with a workflow.
 *
 * @internal
 */
class XtwfStateSeverityForm extends FormBase {

  static $workflow;
  static $state_id;
  static $severity_id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtwf_state_severity_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WorkflowInterface $workflow = NULL, $state_id = NULL, $severity_id = NULL) {
    self::$workflow = $workflow;
    self::$state_id = $state_id;
    self::$severity_id = $severity_id;

    $xtwf_state = self::$workflow->getTypePlugin()->getState(self::$state_id);
    $ajax_callback = [get_class(), 'ajaxCallback'];
    return XtwfPluginStateBase::buildSeverityForm($xtwf_state, $severity_id, $ajax_callback);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $severity = [
      'number' => $values['number'],
      'timeout' => $values['timeout'],
    ];

    $xtwf_type_plugin = self::$workflow->getTypePlugin();
    $state = $xtwf_type_plugin->getState(self::$state_id);
    $state->setSeverity(self::$severity_id, $severity);
    $xtwf_type_plugin->setStateSettings(self::$state_id, $state->getSettings());
    self::$workflow->save();
  }

  /**
   * Ajax callback to close the modal and update the selected text.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response object.
   */
  public static function ajaxCallback() {
    $state_id = self::$state_id;
    $severity_id = self::$severity_id;
    $state = self::$workflow->getTypePlugin()->getState($state_id);
    $severity = $state->getSeverity($severity_id);

    $value_items = [];
    foreach ($severity as $id => $value) {
      $svalue = is_bool($value) ? ($value ? 'TRUE' : 'FALSE') : (is_array($value) ? 'array' : ($value ?: 'none'));
      $value_items[$id] = "$id: $svalue";
    }
    $value_list = [
      '#theme' => 'item_list',
      '#items' => $value_items,
      '#context' => ['list_style' => 'comma-list'],
    ];

    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    $values_key = "$state_id-$severity_id";
    $response->addCommand(new HtmlCommand('#values-' . $values_key, $value_list));
    return $response;
  }

  /**
   * Route title callback.
   */
  public function getTitle(WorkflowInterface $workflow = NULL, $state_id = NULL, $severity_id = NULL) {
    $args = [
      '@workflow' => $workflow->id(),
      '@state' => $state_id,
      '@severity' => $severity_id,
    ];
    $title = t('Edit @severity severity in @workflow/@state state', $args);
    return $title;
  }

}
