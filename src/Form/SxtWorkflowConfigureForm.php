<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Form\SxtWorkflowConfigureForm.
 */

namespace Drupal\sxt_workflow\Form;

use Drupal\sxt_workflow\SxtWorkflowInstall;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Plugin\WorkflowTypeConfigureFormBase;

/**
 * The configuration form for WorkflowType plugins.
 * 
 * @see \Drupal\sxt_workflow\Plugin\WorkflowType\SxtWorkflowTypeBase
 */
class SxtWorkflowConfigureForm extends WorkflowTypeConfigureFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();

    $options = $defaults = [];
    $types = XtsiNodeTypeData::getXtNodeTypes(TRUE);
    foreach ($types as $type_id => $type) {
      $options[$type_id] = $type->label();
    }

    if (!empty($options)) {
      $form['node_types_container'] = [
        '#type' => 'details',
        '#title' => t('This workflow applies to:'),
        '#open' => TRUE,
      ];

      $node_types = $xtwf_type_plugin->getNodeTypeIds();
      $form['node_types_container']['node_types'] = [
        '#type' => 'checkboxes',
        '#options' => $options,
        '#description' => t('Select the content types for which the workflow applies to.'),
        '#default_value' => $node_types,
      ];
    }

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#description' => t('Show the end user a meaningful description.'),
      '#default_value' => $xtwf_type_plugin->getDescription(),
      '#required' => TRUE,
    ];

    $form['weight'] = [
      '#type' => 'textfield',
      '#title' => t('Weight'),
      '#description' => t('Workflow with the higher weight is preferred.'),
      '#default_value' => $xtwf_type_plugin->getWeight(),
      '#size' => 10,
    ];

    // bundle for nested workflows  
    $form['bundle'] = [
      '#type' => 'textfield',
      '#title' => t('Bundle'),
      '#description' => t('Bundle for nested workflows: "trunk" for root workflow only, "leaf" for no more nesting.')  //
      . '<br />' . t('Bundle is set by workflow type and cannot be changed.'),
      '#value' => $xtwf_type_plugin->getBundle(),
      '#size' => 10,
      '#disabled' => TRUE,
    ];
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();

    $node_types = $form_state->getValue(['node_types_container', 'node_types']);
    foreach ($node_types as $node_type_id => $checked) {
      if ($checked) {
        $xtwf_type_plugin->addNodeType($node_type_id);
      }
      else {
        $xtwf_type_plugin->removeNodeType($node_type_id);
      }
    }

    // weight, bundle, description
    $weight = (integer) $form_state->getValue('weight');
    $bundle = $form_state->getValue('bundle');
    $xtwf_type_plugin->setWeight($weight);
    $xtwf_type_plugin->setBundle($bundle);
    $description = $form_state->getValue('description');
    $xtwf_type_plugin->setDescription($description);
    //
    Cache::invalidateTags(['sxt_workflow.nodetypes.with.workflow']);
  }

  /**
   * Implements hook_form_FORM_ID_alter() for workflow_edit_form.
   * 
   * Prevent deletion of required transitions.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formEditAlter(&$form, FormStateInterface $form_state) {
    $workflow = $form_state->getFormObject()->getEntity();
    if (!SxtWorkflowInstall::wfIsOkStructure($workflow)) {
      $msg = t('There is a requirement missmatch.') . ' ' . SlogXt::txtSeeLogMessages();
      $form['err_notice'] = [
        '#markup' => SlogXt::htmlMessage($msg, 'error'),
        '#weight' => -999,
      ];
    }

    $xtwf_states = $workflow->getTypePlugin()->getStates();
    $transitions = &$form['transitions_container']['transitions'];
    foreach (Element::children($transitions) as &$transition_id) {
      if (isset($xtwf_states[$transition_id])) {
        $links = & $transitions[$transition_id]['operations']['#links'];
        if (isset($links['delete'])) {
          unset($links['delete']);
        }
      }
    }
    
    // remove transition add action
    if (!SlogXt::isSuperUser() && isset($form['transitions_container']['transition_add'])) {
      unset($form['transitions_container']['transition_add']);
    }
  }

}
