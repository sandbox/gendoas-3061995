<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Form\SxtWorkflowStateForm.
 */

namespace Drupal\sxt_workflow\Form;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_workflow\Plugin\WorkflowType\SxtWorkflowTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Plugin\WorkflowTypeStateFormBase;

/**
 * ...
 */
class SxtWorkflowStateForm extends WorkflowTypeStateFormBase {

  private static function getStatePlugin(FormStateInterface $form_state) {
    $state_plugin = $form_state->get('statePlugin');
    if (!isset($state_plugin)) {
      if ($xtwf_state = $form_state->get('state')) {
        $state_plugin = SlogXtwf::getStatePlugin($xtwf_state->id());
        $form_state->set('statePlugin', $state_plugin);
      }
    }
    return $state_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($plugin = self::getStatePlugin($form_state)) {
      // build for edit only, for add do form alter
      $form = $plugin->buildConfigurationForm($form, $form_state);
    }
    return $form;
  }

  /**
   * Add state only from list with available states.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAddAlter(&$form, FormStateInterface $form_state) {
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();
    if ($xtwf_type_plugin && $xtwf_type_plugin instanceof SxtWorkflowTypeInterface) {
      $form['id']['#type'] = $form['label']['#type'] = 'hidden';
      $form['id']['#value'] = $form['label']['#value'] = 'xyz';
      if ($input = $form_state->getUserInput()) {
        $id_selected = $input['id_selected'];
        if ($plugin = SlogXtwf::getStatePlugin($id_selected)) {
          $form_state->set('statePlugin', $plugin);
          $form['id']['#value'] = $id_selected;
          $form['label']['#value'] = $plugin->getLabel();
        }
      }

      $exclude_sids = $xtwf_type_plugin->getStateIds();
      $definitions = SlogXtwf::getDefinitions('state');
      if (!SlogXtsi::isDebugMode()) {
        foreach (['xtwf_dummy1', 'xtwf_dummy2', 'xtwf_dummy3'] as $dummy_id) {
          if (isset($definitions[$dummy_id])) {
            $exclude_sids[] = $dummy_id;
          }
        }
      }

      $options = [];
      foreach ($definitions as $state_id => $state) {
        if (!in_array($state_id, $exclude_sids) && $state['status']) {
          $options[$state_id] = $state['label'];
        }
      }

      if (empty($options)) {
        unset($form['actions']);
        $msg = t('There is no more state to add.');
        $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
      }
      else {
        $form['id_selected'] = [
          '#type' => 'radios',
          '#options' => $options,
          '#title' => t('Select state'),
          '#weight' => -100,
          '#required' => TRUE,
        ];
      }
    }
  }

  /**
   * ....
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formEditAlter(&$form, FormStateInterface $form_state) {
    if ($plugin = self::getStatePlugin($form_state)) {
      $plugin->formEditAlter($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($plugin = self::getStatePlugin($form_state)) {
      $plugin->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($plugin = self::getStatePlugin($form_state)) {
      $plugin->submitConfigurationForm($form, $form_state);
    }
  }

}
