<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Form\SxtWorkflowTransitionForm.
 */

namespace Drupal\sxt_workflow\Form;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_workflow\Plugin\WorkflowType\SxtWorkflowTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Plugin\WorkflowTypeTransitionFormBase;

/**
 */
class SxtWorkflowTransitionForm extends WorkflowTypeTransitionFormBase {
  
  private static function getTransitionPlugin(FormStateInterface $form_state) {
    $transition_plugin = $form_state->get('transitionPlugin');
    if (!isset($transition_plugin)) {
      if ($xtwf_transition = $form_state->get('transition')) {
        $transition_plugin = SlogXtwf::getTransitionPlugin($xtwf_transition->id());
        $form_state->set('transitionPlugin', $transition_plugin);
      }
    }
    return $transition_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($plugin = self::getTransitionPlugin($form_state)) {
      // build for edit only, for add do form alter
      $form = $plugin->buildConfigurationForm($form, $form_state);
    }

    return $form;
  }

  /**
   * Add transition only from list with available transitions.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAddAlter(&$form, FormStateInterface $form_state) {
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();
    if ($xtwf_type_plugin && $xtwf_type_plugin instanceof SxtWorkflowTypeInterface) {
      $form['id']['#type'] = $form['label']['#type'] = 'hidden';
      $form['id']['#value'] = $form['label']['#value'] = 'xyz';
      if ($input = $form_state->getUserInput()) {
        $id_selected = $input['id_selected'];
        if ($plugin = SlogXtwf::getTransitionPlugin($id_selected)) {
          $form_state->set('transitionPlugin', $plugin);
          $form['id']['#value'] = $id_selected;
          $form['label']['#value'] = $plugin->getLabel();
        }
      }

      $state_ids = $xtwf_type_plugin->getStateIds();
      $exclude_tids = $xtwf_type_plugin->getTransitionIds();
      $definitions = SlogXtwf::getDefinitions('transition');
      $options = [];
      foreach ($definitions as $transition_id => $transition) {
        if (in_array($transition_id, $state_ids) //
            && !in_array($transition_id, $exclude_tids) && $transition['status']) {
          $options[$transition_id] = $transition['label'];
        }
      }

      if (empty($options)) {
        unset($form['actions']);
        unset($form['from']);
        unset($form['to']);
        $msg = t('There is no more transition to add.');
        $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
        $msg = t('Note: for each transition there must exist the corrosponding state.');
        $form['notice2']['#markup'] = SlogXt::htmlMessage($msg, 'status');
      }
      else {
        $form['id_selected'] = [
          '#type' => 'select',
          '#options' => $options,
          '#title' => t('Select transition'),
          '#weight' => -100,
          '#required' => TRUE,
        ];
      }
    }
  }

  /**
   * ...
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formEditAlter(&$form, FormStateInterface $form_state) {
    if ($plugin = self::getTransitionPlugin($form_state)) {
      $plugin->formEditAlter($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($plugin = self::getTransitionPlugin($form_state)) {
      $plugin->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($plugin = self::getTransitionPlugin($form_state)) {
      $plugin->submitConfigurationForm($form, $form_state);
    }
  }

}
