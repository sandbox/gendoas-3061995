<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Form\XtwfSettingsForm.
 */

namespace Drupal\sxt_workflow\Form;

use Drupal\sxt_workflow\SlogXtwf;
//use Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfPluginStateBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SlogTx settings form.
 */
class XtwfSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_workflow_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sxt_workflow.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['forced_states'] = [
      '#type' => 'details',
      '#title' => t('Force additional states for workflows'),
      '#open' => TRUE,
    ];

    $forced_states = SlogXtwf::getConfigForcedStates();
    foreach ($forced_states as $state_id => $value) {
      $state_plugin = SlogXtwf::getStatePlugin($state_id);
      if ($state_plugin) {
        $form['forced_states'][$state_id] = [
          '#type' => 'checkbox',
          '#title' => $state_plugin->getLabel(),
          '#default_value' => $forced_states[$state_id],
        ];
      }
    }

    $args = ['%min' => 2, '%max' => 5, '%default' => 3];
    $form['max_depth'] = [
      '#type' => 'textfield',
      '#title' => t('Max. depth'),
      '#description' => t('Set max. depth for nesting workflows. Min: %min, Max: %max, Default=%default', $args),
      '#default_value' => SlogXtwf::getMaxDepth(),
      '#size' => 10,
    ];

    $args = ['%min' => 3, '%max' => 50, '%default' => 10];
    $description = t('Set timeout in days for competition state (each severity).');
    $dminmax = t('Min: %min, Max: %max, Default=%default', $args);
    $form['dynamic_timeout'] = [
      '#type' => 'textfield',
      '#title' => t('Timeout for dynamic severity'),
      '#description' => "$description $dminmax",
      '#default_value' => SlogXtwf::getDynamicTimeout(),
      '#size' => 10,
    ];

    $args = ['%min' => 50, '%max' => 100, '%default' => 80];
    $description = t('Set percentage, that is required for severe voting.');
    $dminmax = t('Min: %min, Max: %max, Default=%default', $args);
    $dmore = t('This may be for voting for finish finally');
    $form['percent_severe'] = [
      '#type' => 'textfield',
      '#title' => t('Percentage for severe voting'),
      '#description' => "$description $dminmax<br />$dmore",
      '#default_value' => SlogXtwf::getPercentSevere(),
      '#size' => 10,
    ];

    $form['admin_id_show'] = [
      '#type' => 'checkbox',
      '#title' => t('Show admin id in history'),
      '#default_value' => SlogXtwf::getAdminUidShow(),
    ];

    $form['do_log_warnings'] = [
      '#type' => 'checkbox',
      '#title' => t('Log warnings'),
      '#default_value' => SlogXtwf::getDoLogWarnings(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $max_depth = (integer) $form_state->getValue('max_depth');
    $msg_not_valid = t('Not a valid value.');
    if ($max_depth < 2 || $max_depth > 5) {
      $form_state->setErrorByName('max_depth', $msg_not_valid);
    }

    $dynamic_timeout = (integer) $form_state->getValue('dynamic_timeout');
    if ($dynamic_timeout < 3 || $dynamic_timeout > 50) {
      $form_state->setErrorByName('dynamic_timeout', $msg_not_valid);
    }

    $percent_severe = (integer) $form_state->getValue('percent_severe');
    if ($percent_severe < 50 || $percent_severe > 100) {
      $form_state->setErrorByName('percent_severe', $msg_not_valid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sxt_workflow.settings');
    $values = $form_state->getValues();
    $forced_new = [];
    $forced_states = SlogXtwf::getConfigForcedStates();
    foreach ($forced_states as $state_id => $value) {
      $forced_new[$state_id] = $values[$state_id];
    }
    $config->set('forced_states', $forced_new)
        ->set('max_depth', $values['max_depth'])
        ->set('dynamic_timeout', $values['dynamic_timeout'])
        ->set('percent_severe', $values['percent_severe'])
        ->set('admin_id_show', $values['admin_id_show'])
        ->set('do_log_warnings', $values['do_log_warnings'])
        ->save();
    drupal_set_message($this->t('Xtwf settings have been saved.'));
  }

}
