<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\WorkflowType\SxtWorkflowTypeBase.
 */

namespace Drupal\sxt_workflow\Plugin\WorkflowType;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\sxt_workflow\SxtWorkflowState;
use Drupal\sxt_workflow\SxtWorkflowTransition;
use Drupal\sxt_workflow\SxtWorkflowStateInterface;
use Drupal\sxt_workflow\SxtWorkflowTransitionInterface;
use Drupal\workflows\StateInterface;
use Drupal\workflows\WorkflowInterface;
use Drupal\workflows\Plugin\WorkflowTypeBase;

/**
 * )
 */
abstract class SxtWorkflowTypeBase extends WorkflowTypeBase implements SxtWorkflowTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $plugin_definition['forms'] += [
      'configure' => '\Drupal\sxt_workflow\Form\SxtWorkflowConfigureForm',
      'state' => '\Drupal\sxt_workflow\Form\SxtWorkflowStateForm',
      'transition' => '\Drupal\sxt_workflow\Form\SxtWorkflowTransitionForm',
    ];

    if (empty($plugin_definition['required_transitions'])) {
      $plugin_definition['required_transitions'] = [];
    }

    $bundle = $plugin_definition['required_bundle'];
    $types = SlogXtwf::getBundleTypes();
    if (empty($bundle) || !in_array($bundle, $types)) {
      $plugin_definition['required_bundle'] = 'trunk';
    }

    $this->entityTypeManager = \Drupal::entityTypeManager();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    static $cache = [];
    $cache_pid = $this->getPluginId();
    if (empty($cache[$cache_pid])) {
      $states = [];
      $transitions = [];
      $required_states = $this->getRequiredStates();
      $required_transitions = $this->getRequiredTransitions();
      if (!empty($required_transitions)) {
        $t_definitions = SlogXtwf::getDefinitions('transition');
      }
      if (!empty($required_states)) {
        $s_definitions = SlogXtwf::getDefinitions('state');
        $state_ids = array_keys($s_definitions);
        foreach ($required_states as $state_id) {
          $state = $s_definitions[$state_id] ?: FALSE;
          if (!$state || !$state['status']) {
            $msg = t('State not found or disabled: @state', ['@state' => $state_id]);
            SlogXtwf::logger()->error($msg);
            continue;
          }

          $states[$state_id] = [
            'label' => $state['label'],
            'settings' => $state['settings'],
            'weight' => $state['weight'],
          ];

          if (in_array($state_id, $required_transitions)) {
            $transition_id = $state_id;
            $transition_plugin = SlogXtwf::getTransitionPlugin($transition_id);
            if ($transition_plugin && $transition_plugin->isEnabled()) {
              $t_settings = $transition_plugin->getSettings();
              $required_from = array_intersect($state_ids, $t_settings['required_from']);
              if (!empty($t_settings['exclude_from'])) {
                $required_from = array_diff($required_from, $t_settings['exclude_from']);
              }

              $transitions[$transition_id] = [
                'label' => $transition_plugin->getLabel(),
                'from' => array_values($required_from),
                'to' => $state_id,
                'settings' => $t_settings,
                'weight' => $transition_plugin->getWeight(),
              ];
            }
            else {
              $msg = t('Transition not found or disabled: @transition', ['@transition' => $transition_id]);
              SlogXtwf::logger()->error($msg);
            }
          }
        }
      }

      $cache[$cache_pid] = [
        'description' => '...description: new workflow',
        'states' => $states,
        'transitions' => $transitions,
        'node_types' => XtsiNodeTypeData::getXtNodeTypeIds(),
        'bundle' => $this->pluginDefinition['required_bundle'],
        'weight' => 999,
      ];
    }

    return $cache[$cache_pid];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredStates() {
    static $cache = [];
    $cache_pid = $this->getPluginId();
    if (empty($cache[$cache_pid])) {
      $required = parent::getRequiredStates();
      $is_trunk = ($this->pluginDefinition['required_bundle'] === 'trunk');
      $all_forced = SlogXtwf::getAllForcedStates($is_trunk);
      foreach ($all_forced as $required_id) {
        if (!in_array($required_id, $required)) {
          $required[] = $required_id;
        }
      }

      $cache[$cache_pid] = $required;
    }

    return $cache[$cache_pid];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredTransitions() {
    static $cache = [];
    $cache_pid = $this->getPluginId();
    if (empty($cache[$cache_pid])) {
      $required = $this->pluginDefinition['required_transitions'];
      $required_bundle = $this->pluginDefinition['required_bundle'];
      if ($required_bundle === 'trunk') {
        // force transition id to be equal as state id where to get there
        // SlogXtwf::XTWF_REQUEST_COLLABORATE is the start state, there is 
        // no transition to get that state
        $required_states = $this->getRequiredStates();
        foreach ($required_states as $state_id) {
          if ($state_id !== SlogXtwf::XTWF_REQUEST_COLLABORATE && !in_array($state_id, $required)) {
            $required[] = $state_id;
          }
        }

        // do not allow transition without state to get there
        foreach ($required as $transition_id) {
          if (!in_array($transition_id, $required_states)) {
            unset($required[$transition_id]);
            $msg = t('Transition without corresponding state found: @transition_id', ['@transition_id' => $transition_id]);
            SlogXtwf::logger()->error($msg);
          }
        }
      }

      $cache[$cache_pid] = $required;
    }

    return $cache[$cache_pid];
  }

  /**
   * {@inheritdoc}
   */
  public function getState($state_id) {
    if (!isset($this->configuration['states'][$state_id])) {
      $wf_label = $this->label();
      throw new \InvalidArgumentException("The state '$state_id' does not exist in workflow ($wf_label).");
    }

    $state = $this->configuration['states'][$state_id];
    return new SxtWorkflowState(//
        $this, //
        $state_id, //
        $state['label'], //
        $state['weight'], //
        ($state['settings'] ?? [])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setStateSettings($state_id, $settings) {
    if (!isset($this->configuration['states'][$state_id])) {
      $wf_label = $this->label();
      throw new \InvalidArgumentException("The state '$state_id' does not exist in workflow ($wf_label).");
    }

    $this->configuration['states'][$state_id]['settings'] = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStateIds() {
    return array_keys($this->configuration['states']);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitionIds() {
    return array_keys($this->configuration['transitions']);
  }

  /**
   * {@inheritdoc}
   * 
   * Append settings to transition.
   * 
   */
  public function getTransition($transition_id, $throw_exeption = TRUE) {
    if (!$this->hasTransition($transition_id)) {
      if ($throw_exeption) {
        $wf_label = $this->label();
        throw new \InvalidArgumentException("The transition '$transition_id' does not exist in workflow ($wf_label).");
      }
      return FALSE;
    }

    $transition = &$this->configuration['transitions'][$transition_id];
    return new SxtWorkflowTransition(//
        $this, //
        $transition_id, //
        $transition['label'], //
        $transition['from'], //
        $transition['to'], //
        $transition['weight'], //
        ($transition['settings'] ?? [])
    );
  }

  public function addTransitionFromStates($transition_id, array $add_state_ids) {
    $from_state_ids = array_merge($this->configuration['transitions'][$transition_id]['from'], $add_state_ids);
    $this->setTransitionFromStates($transition_id, $from_state_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function workflowHasData(WorkflowInterface $workflow) {
    return (bool) $this->entityTypeManager
            ->getStorage('xtwf_node_state')
            ->getQuery()
            ->condition('workflow', $workflow->id())
            ->count()
            ->accessCheck(FALSE)
            ->range(0, 1)
            ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function workflowStateHasData(WorkflowInterface $workflow, StateInterface $state) {
    return (bool) $this->entityTypeManager
            ->getStorage('xtwf_node_state')
            ->getQuery()
            ->condition('workflow', $workflow->id())
            ->condition('state', $state->id())
            ->count()
            ->accessCheck(FALSE)
            ->range(0, 1)
            ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return (isset($this->configuration['description']) ? $this->configuration['description'] : '');
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->configuration['description'] = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return (integer) (isset($this->configuration['weight']) ? $this->configuration['weight'] : 0);
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->configuration['weight'] = (integer) $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle() {
    return $this->configuration['bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function setBundle($bundle) {
    $types = SlogXtwf::getBundleTypes();
    if (!in_array($bundle, $types)) {
      $bundle = 'trunk';
    }
    $this->configuration['bundle'] = $bundle;
    return $this;
  }

  public function hasNextReachable($from_state_id, $to_state_id) {
    $to_state = $this->getState($to_state_id);
    $transition = $this->getTransition($to_state_id);
    if (empty($to_state) || empty($transition)) {
      return FALSE;
    }

    if (!$to_state instanceof SxtWorkflowStateInterface || !$transition instanceof SxtWorkflowTransitionInterface) {
      $message = t('Unvalid state or transition found for: @state_id.', ['@state_id' => $to_state_id]);
      throw new \LogicException($message);
    }

    return $this->getState($from_state_id)->canTransitionTo($to_state_id);
  }

  public function hasNextCompetition($from_state_id) {
    return $this->hasNextReachable($from_state_id, SlogXtwf::XTWF_COMPETITION);
  }

  public function hasNextFinishRenewable($from_state_id) {
    $renewable = $this->hasNextReachable($from_state_id, SlogXtwf::XTWF_FINISH_RENEWABLE);
    $renewal = $this->hasNextReachable(SlogXtwf::XTWF_FINISH_RENEWABLE, SlogXtwf::XTWF_REQUEST_RENEW);
    return ($renewable && $renewal);
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeTypeIds() {
    return ($this->configuration['node_types'] ?? []);
  }

  /**
   * {@inheritdoc}
   */
  public function appliesToNodeType($node_type_id) {
    return in_array($node_type_id, $this->getNodeTypeIds(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function removeNodeType($node_type_id) {
    if ($this->appliesToNodeType($node_type_id)) {
      $idx = array_search($node_type_id, $this->configuration['node_types']);
      unset($this->configuration['node_types'][$idx]);
      sort($this->configuration['node_types']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addNodeType($node_type_id) {
    if (!$this->appliesToNodeType($node_type_id)) {
      $this->configuration['node_types'][] = $node_type_id;
      sort($this->configuration['node_types']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $node_definition = \Drupal::entityTypeManager()->getDefinition('node');
    foreach ($this->getNodeTypeIds() as $node_type_id) {
      $dependency = $node_definition->getBundleConfigDependency($node_type_id);
      $dependencies[$dependency['type']][] = $dependency['name'];
    }
    return $dependencies;
  }

  public function findNextState($state_id, $as_object) {
    $next_state_id = FALSE;
    if ($state_id !== SlogXtwf::XTWF_FINISH_FINALLY) {
      $states = $this->getStates();
      $found = FALSE;
      foreach (array_keys($states) as $sid) {
        if ($sid === $state_id) {
          $found = TRUE;
        }
        elseif ($found && $this->hasNextReachable($state_id, $sid)) {
          $next_state_id = $sid;
          break;
        }
      }
    }

    if ($next_state_id && $as_object) {
      return $states[$next_state_id];
    }

    return $next_state_id;
  }

  public function getStatesOfType($state_type) {
    $result = [];
    $xtwf_states = $this->getStates();
    foreach ($xtwf_states as $state_id => $xtwf_state) {
      if ($xtwf_state->isStateType($state_type)) {
        $result[$state_id] = $xtwf_state;
      }
    }

    return $result;
  }

  public function getStatesContentCollect() {
    return $this->getStatesOfType('collect_content');
  }

}
