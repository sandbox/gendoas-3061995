<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\WorkflowType\SxtWorkflowTypeInterface.
 */

namespace Drupal\sxt_workflow\Plugin\WorkflowType;

use Drupal\workflows\WorkflowTypeInterface;

/**
 * Interface for ContentModeration WorkflowType plugin.
 */
interface SxtWorkflowTypeInterface extends WorkflowTypeInterface {

  /**
   * Return the description of this workflow.
   * 
   * @return string
   */
  public function getDescription();
  
  /**
   * Set the description of this workflow.
   * 
   * @param type $description
   */
  public function setDescription($description);
  
  /**
   * Return the bundle of this workflow.
   * 
   * @see: SlogXtwf::getBundleTypes()
   * 
   * @return string
   *  Valid values: trunk, fork and leaf
   */
  public function getBundle();
  
  /**
   * Set the bundle of this workflow.
   * 
   * @see: SlogXtwf::getBundleTypes()
   * 
   * @param string $bundle
   *  Valid values: trunk, fork and leaf
   */
  public function setBundle($bundle);

  /**
   * Return state ids from configuration.
   */
  public function getStateIds();

  /**
   * Return transition ids from configuration.
   */
  public function getTransitionIds();
    
  /**
   * Gets the ids for node types the workflow is applied to.
   *
   * @return string[]
   *   The entity types the workflow is applied to.
   */
  public function getNodeTypeIds();

  /**
   * Checks if the workflow applies to the supplied node type.
   *
   * @param string $node_type_id
   *   The node type ID to check.
   *
   * @return bool
   *   TRUE if the workflow applies to the supplied node type ID. FALSE if not.
   */
  public function appliesToNodeType($node_type_id);

  /**
   * Removes an node type ID from the workflow.
   *
   * @param string $node_type_id
   *   The node type ID to remove.
   */
  public function removeNodeType($node_type_id);

  /**
   * Add a node type ID to the workflow.
   *
   * @param string $node_type_id
   *   The node type ID to add. It is responsibility of the caller to provide
   *   a valid node type ID.
   */
  public function addNodeType($node_type_id);

}
