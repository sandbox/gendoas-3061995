<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\WorkflowType\XtwfTrunkBase.
 */

namespace Drupal\sxt_workflow\Plugin\WorkflowType;

/**
 * Attaches workflows to content entity types and their bundles.
 *
 * @WorkflowType(
 *   id = "xtwf_trunk_base",
 *   label = @Translation("Base trunk"),
 *   required_states = {
 *     "xtwf_request_collaborate",
 *     "xtwf_request_renew",
 *     "xtwf_request_finish_finally",
 *     "xtwf_finish_renewable",
 *     "xtwf_finish_finally",
 *     "xtwf_competition",
 *   },
 *   required_transitions = {
 *     "xtwf_request_renew",
 *     "xtwf_request_finish_finally",
 *     "xtwf_finish_renewable",
 *     "xtwf_finish_finally",
 *     "xtwf_competition",
 *   },
 *   required_bundle = "trunk",
 * )
 */
class XtwfTrunkBase extends SxtWorkflowTypeBase {

}
