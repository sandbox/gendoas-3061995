<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\WorkflowType\XtwfTrunkEmpty.
 */

namespace Drupal\sxt_workflow\Plugin\WorkflowType;

/**
 * Attaches workflows to content entity types and their bundles.
 *
 * @WorkflowType(
 *   id = "xtwf_trunk_empty",
 *   label = @Translation("Empty trunk"),
 *   required_bundle = "trunk",
 * )
 */
class XtwfTrunkEmpty extends SxtWorkflowTypeBase {

}
