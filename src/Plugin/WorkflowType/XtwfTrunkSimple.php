<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\WorkflowType\XtwfTrunkSimple.
 */

namespace Drupal\sxt_workflow\Plugin\WorkflowType;

/**
 * Attaches workflows to content entity types and their bundles.
 *
 * @WorkflowType(
 *   id = "xtwf_trunk_simple",
 *   label = @Translation("Simple trunk"),
 *   required_states = {
 *     "xtwf_request_collaborate",
 *     "xtwf_finish_finally",
 *   },
 *   required_transitions = {
 *     "xtwf_finish_finally",
 *   },
 *   required_bundle = "trunk",
 * )
 */
class XtwfTrunkSimple extends SxtWorkflowTypeBase {

}
