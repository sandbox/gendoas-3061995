<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\XtwfPluginManagerBase.
 */

namespace Drupal\sxt_workflow\Plugin;

use Drupal\slogxt\Plugin\DefinitionsSortedTrait;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Base plugin manager for some types.
 */
abstract class XtwfPluginManagerBase extends DefaultPluginManager {

  use DefinitionsSortedTrait;

  protected $instances = NULL;

  /**
   * Constructs a ViewsPluginManager object.
   *
   * @param string $type
   *   The plugin type, i.e. state, transition
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $c_type = Container::camelize($type);
    $plugin_definition_annotation_name = "Drupal\sxt_workflow\Annotation\Xtwf{$c_type}";
    $plugin_interface = null;
    parent::__construct("Plugin/sxt_workflow/Xtwf{$c_type}", $namespaces, $module_handler, //
        $plugin_interface, //
        $plugin_definition_annotation_name);
    $cache_key = "sxt_workflow_{$type}_info";
    $this->alterInfo($cache_key);
    $this->setCacheBackend($cache_backend, $cache_key);
  }

  /**
   * Returns the ids of all plugins by definitions.
   * 
   * @return array of ids
   */
  public function getPluginIds() {
    $definitions = $this->getDefinitionsSorted();
    return array_keys($definitions);
  }

  /**
   * Return plugin objects.
   * 
   * @param string $which
   *   Allowed values: all, enabled, disabled, defaults to enabled
   * @return array of plugin objects.
   */
  public function getXtwfPlugins($which = 'enabled') {
    if (empty($this->instances)) {
      $all = $enabled = $disabled = [];
      $ids = $this->getPluginIds();
      foreach ($ids as $plugin_id) {
        $instance = $this->createInstance($plugin_id);
        $all[$plugin_id] = $instance;
        if ($instance->isEnabled()) {
          $enabled[$plugin_id] = $instance;
        } else {
          $disabled[$plugin_id] = $instance;
        }
      }
      
      $this->instances = [
        'all' => $all,
        'enabled' => $enabled,
        'disabled' => $disabled,
      ];
    }

    return ($this->instances[$which] ?? []);
  }

}
