<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\XtwfPluginManagerState.
 */

namespace Drupal\sxt_workflow\Plugin;

use Drupal\Component\Utility\NestedArray;

/**
 */
class XtwfPluginManagerState extends XtwfPluginManagerBase {

  protected $settings_defaults_severity = [
    'number_description' => '...not implemented...',
    'number_suffix' => '...not implemented...',
    'severity' => [
      'leight' => [
        'number' => 10,
        'timeout' => 10,
      ],
      'medium' => [
        'number' => 20,
        'timeout' => 20,
      ],
      'severe' => [
        'number' => 50,
        'timeout' => 30,
      ],
      'severest' => [
        'number' => 100,
        'timeout' => 30,
      ],
    ]
  ];

  protected function findDefinitions() {
    //cache_discovery.cid=sxt_workflow_state_info
    //see $cache_key = "sxt_workflow_{$type}_info";
    $definitions = parent::findDefinitions();
    foreach ($definitions as $state_id => &$definition) {
      $settings = $definition['settings'] ?? [];
      $is_request = ($settings['state_type'] === 'request');
      $is_collect_content = ($settings['state_type'] === 'collect_content');
      $is_subworkflow = $is_collect_content ? (boolean) $settings['is_subworkflow'] : FALSE;
      $is_subwf_waiting = ($settings['state_type'] === 'subwf_waiting');
      $use_severity = (boolean) $settings['use_severity'];
      $use_dynamic_severity = (boolean) $settings['use_dynamic_severity'];
      $is_finished = ($settings['state_type'] === 'finished');
            
      if ($is_subworkflow || $is_subwf_waiting) {
        $use_severity = FALSE;
        $use_dynamic_severity = FALSE;        
        $is_subworkflow = !$is_subwf_waiting ? $is_subworkflow : FALSE;
      }
      elseif ($use_dynamic_severity || $is_finished) {
        $use_severity = FALSE;
      }
      elseif ($is_request || $is_collect_content) {
        $use_severity = TRUE;
      }

      $settings['is_subworkflow'] = $is_subworkflow;
      $settings['use_severity'] = $use_severity;
      $settings['use_dynamic_severity'] = $use_dynamic_severity;
      if ($use_severity) {
        $s_settings = ($settings['severity'] ?? []);
        $s_defaults = $this->settings_defaults_severity['severity'];
        $settings['severity'] = NestedArray::mergeDeep($s_defaults, $s_settings);
        $settings += $this->settings_defaults_severity;
      }
      
      $definition['settings'] = $settings;
    }

    return $definitions;
  }

}
