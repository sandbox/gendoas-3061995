<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfPluginBase.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_workflow\Entity\XtwfNodeStateInterface;
use Drupal\slogxt\XtDateTimeTrait;
use Drupal\slogxt\XtDateFormaterTrait;
use Drupal\slogxt\XtNotHandledTrait;
use Drupal\slogxt\XtExtrasTrait;
use Drupal\Core\Plugin\PluginBase;

/**
 */
class XtwfPluginBase extends PluginBase {

  use XtDateTimeTrait;
  use XtDateFormaterTrait;
  use XtNotHandledTrait;
  use XtExtrasTrait;
  
  protected $node_state;  
  
  public function setNodeState(XtwfNodeStateInterface $node_state) {
    $this->node_state = $node_state;
  }
  public function getNodeState($log_error = TRUE) {
    if (!empty($this->node_state) && $this->node_state instanceof XtwfNodeStateInterface) {
      return $this->node_state;
    }
    
    if ($log_error) {
      $msg = t('Node state not set in plugin @plugin', ['@plugin' => $this->getPluginId()]);
      SlogXtwf::logger()->error($msg);      
    }
    return NULL;
  }

  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  public function isEnabled() {
    return (boolean) $this->pluginDefinition['status'];
  }

  public function getSettings() {
    return ($this->pluginDefinition['settings'] ?? []);
  }

  public function getSetting($key) {
    if ($settings = $this->getSettings()) {
      return ($settings[$key] ?: NULL);
    }
  }

}
