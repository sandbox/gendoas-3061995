<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfPluginSCollectContentBase.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_workflow\SlogXtwf;

//use Drupal\slogxt\SlogXt;

/**
 */
class XtwfPluginSCollectContentBase extends XtwfPluginStateBase {

  /**
   * Defaults for collect content state plugin
   *
   * @var array 
   */
  protected $defaults = [
    'state_type' => 'collect_content',
  ];

  public function getPostsRelevant() {
    $state_data = $this->getNodeState()->getStateData();
    $number = (integer) $state_data['shared'];
    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    $prefix = SlogXtwf::XTWF_PREFIX_DASH;
    $posts_all = $this->getPostsAll();
    $posts_relevant = $this->getPostsRelevant();
    $mark_shared = $mark_draft = '';

    $user_data = $this->getNodeState()->getUserData(-1);  // currentuser
    if (!empty($user_data) && is_array($user_data)) {
      if (!empty($user_data['shared'])) {
        $mark_shared = $this->getMarker();
      } 
      else {
        $mark_draft = $this->getMarker(TRUE);
      }
    }

    $more_info = [
      'more_title' => t('Posts details'),
      'shared' => $prefix . t('Shared posts') . ': ' . $posts_relevant . $mark_shared,
      'severity' => $prefix . t('Drafts') . ': ' . ($posts_all - $posts_relevant). $mark_draft,
    ];

    return $more_info;
  }

  public function setUserData($user_id, array $value) {
    $node_state = $this->getNodeState();
    $user_data = $node_state->getUserData();
    $user_data[$user_id] = $value;
    $node_state->setUserData(-1, $user_data);
  }

  public function deleteUserData($user_id, $preserve = FALSE) {
    $node_state = $this->getNodeState();
    if ($node_state->hasUserData($user_id)) {
      $user_data = $node_state->getUserData();

      if (!empty($user_data['shared'])) {
        // remove shared sum
        $this->_changeSharedSummarize(FALSE);
      }

      unset($user_data[$user_id]);
      $node_state->setUserData(-1, $user_data)->save();
    }
  }

  public function shareUserData($user_id) {
    $node_state = $this->getNodeState();
    $node_id = $node_state->id();
    $user_data = $node_state->getUserData($user_id) ?? [];
    $sid = $user_data['sid'] ?? '???';

    $args = [
      '@module' => 'shareUserData (nid/uid/sid)',
      '@ids' => "$node_id/$user_id/$sid",
    ];


    $slogitem = SlogXtsi::getSlogitem($user_data['sid']);
    if (empty($slogitem)) {
      $error_msg = t('@module: slogitem not found (@ids).', $args);
      SlogXtwf::logger()->error($error_msg);
      return FALSE;
    }

    $menu_label = $node_state->buildNodeTitleByState();
    $menu_term = SlogXtsi::getMwCollectBaseDraftMenuTerm($node_id, $menu_label, TRUE);
    if (empty($menu_term)) {
      $error_msg = t('@module: menu term not found (@ids).', $args);
      SlogXtwf::logger()->error($error_msg);
      return FALSE;
    }
    if ($menu_term->id() != $slogitem->getTermId()) {
      $slogitem->setTermId($menu_term->id())->save();
      $error_msg = t('@module: tid missmatch (@ids).', $args);
      SlogXtwf::logger()->error($error_msg);
      return FALSE;
    }
    if (!$menu_term->isMenuTerm() || !$menu_term->isBaseMenuTerm()) {
      $error_msg = t('@module: menu term is not base (@ids).', $args);
      SlogXtwf::logger()->error($error_msg);
      return FALSE;
    }

    $description = t('Container for shared content (@ids).', $args);
    $shared_menuterm = $menu_term->getChildTermByName('Shared', TRUE, $description);
    if (!$shared_menuterm) {
      $error_msg = t('@module: shared term not found (@ids).', $args);
      SlogXtwf::logger()->error($error_msg);
      return FALSE;
    }

    $shared_tid = $shared_menuterm->id();
    $slogitem->setTermId($shared_tid)->save();

    $user_data['shared'] = REQUEST_TIME;
    $this->setUserData($user_id, $user_data);
    // add shared sum
    $this->_changeSharedSummarize(TRUE);
    // save changes
    $node_state->save();

    return TRUE;
  }

  protected function _changeSharedSummarize($add_shared) {
    $target_id = 'shared';
    $value = $add_shared ? 1 : -1;
    $node_state = $this->getNodeState();
    $state_data = $node_state->getStateData();
    if (empty($state_data[$target_id])) {
      $state_data[$target_id] = 0;
    }

    $state_data[$target_id] += $value;
    if ($state_data[$target_id] < 0) {
      $state_data[$target_id] = 0;
    }
    $node_state->setData($state_data, 'state_data');
  }

}
