<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfPluginSRequestBase.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;

/**
 */
class XtwfPluginSRequestBase extends XtwfPluginStateBase {

  /**
   * Defaults for request state plugin
   *
   * @var array 
   */
  protected $defaults = [
    'state_type' => 'request',
  ];
  protected $preferred = [];

  /**
   * {@inheritdoc}
   */
  public function toFinalizeByTimeout() {
    $state_data = $this->getNodeState()->getStateData();
    $data = $state_data['request_finalize'] ?? [];
    $timeout = (integer) ($data['timeout'] ?? 0);
    $requests = (integer) ($data['requests'] ?? 0);
    return ($timeout > $requests);
  }

  public function toFinalizeStrict() {
    $state_data = $this->getNodeState()->getStateData();
    $data = $state_data['request_strict'] ?? [];
    $strict = (integer) ($data['yes'] ?? 0);
    $not_strict = (integer) ($data['no'] ?? 0);
    return ($strict > $not_strict);
  }

  public function toDiscuss() {
    if ($this->getNodeState()->canDiscussRequest()) {
      $state_data = $this->getNodeState()->getStateData();
      $data = $state_data['request_discuss'] ?? [];
      $discuss = (integer) ($data['yes'] ?? 0);
      $not_discuss = (integer) ($data['no'] ?? 0);
      return ($discuss > $not_discuss);
    }
    
    return FALSE;
  }

  public function prepareNextState() {
    $node_state = $this->getNodeState();
    // no competition for admin action (finalize or terminate)
    $on_admin_finalize = $node_state->getData('onAdminFinalize') ?: FALSE;
    if (!$on_admin_finalize) {
      if ($competitions = $this->getNeededCompetitions()) {
        $next_state_id = SlogXtwf::XTWF_COMPETITION;

        $next_state_plugin = SlogXtwf::getStatePlugin($next_state_id);
        $next_state_plugin->setNodeState($node_state);
        $next_state_plugin->prepareCompetitionState($competitions);

        return $next_state_id;
      }
      else {
        $best_wf_id = $this->getPreferredBest('workflow', 'id');
        $best_sv_id = $this->getPreferredBest('severity', 'id');
        if (!empty($best_wf_id)) {
          $node_state->setWorkflowId($best_wf_id);
        }
        if (!empty($best_sv_id)) {
          $node_state->setSeverity($best_sv_id);
        }
      }
    }

    // $next_state_id not provided
    return FALSE;
  }

  public function getPreferredBest($target, $key = 'id') {
    if ($target === 'discuss') {
      $node_state = $this->getNodeState();
      $state_data = $node_state->getData('state_data');
      $target_data = ($state_data["request_discuss"] ?? []);
    }
    else {
      $preferred = $this->getPreferred($target);
      if (isset($preferred['best'])) {
        return $preferred['best'][$key];
      }
    }
  }

  public function getPreferredNext($target, $key = 'id') {
    $preferred = $this->getPreferred($target);
    if (isset($preferred['next'])) {
      return $preferred['next'][$key];
    }
  }

  public function getPreferredDiscuss() {
    $target = 'discuss';
    $node_state = $this->getNodeState();
    $state_data = $node_state->getData('state_data');
    $target_data = ($state_data["request_$target"] ?? []);
    if (!empty($target_data)) {
      
    }

    if (empty($preferred)) {
      $preferred = ['best' => [
          'id' => SlogXtwf::XTWF_SELECT_NO,
          'number' => 0,
      ]];
    }
    $this->preferred[$target] = $preferred;
    return $this->preferred[$target];
  }

  public function getPreferred($target) {
    if (!empty($this->preferred[$target])) {
      return $this->preferred[$target];
    }

    if ($target === 'discuss') {
      return $this->getPreferredDiscuss();
    }


    if (!in_array($target, ['workflow', 'severity', 'finalize', 'strict'])) {
      $message = t('Target not handled: @target', ['@target' => $target]);
      throw new \LogicException($message);
    }

    $best_id = '';
    $best_number = 0;
    $node_state = $this->getNodeState();
    $state_data = $node_state->getData('state_data');
    $target_data = ($state_data["request_$target"] ?? []);
    $s_values = [
      'workflow' => $node_state->getWorkflowId(),
      'severity' => $node_state->getSeverityId(),
      'finalize' => $node_state->getFinalizeByTimeoutKey(),
      'strict' => $node_state->getFinalizeStrictKey(),
    ];

//                  if (FALSE) {
//              //todo::current:: ----sxt_sxt_opentalk --- finalize
//                    if ($target === 'workflow') {
//                      $target_data = [
//                        'trunk_b01' => 5,
//                        'trunk_s01' => 5,
//                        'trunk_e01' => 5,
//                      ];
//                    }
//                    else {
//                      $target_data = [
//                        'leight' => 5,
//                        'medium' => 5,
//                        'severe' => 5,
//                        'severest' => 5,
//                      ];
//                    }
//                  }


    $requests = 0;
    foreach ($target_data as $key => $number) {
      $requests += $number;
      $number_test = $number;
      if ($key === $s_values[$target]) {
        $number_test += 0.5;
      }
      if (empty($best_id) || $number_test > $best_number) {
        $best_id = $key;
        $best_number = $number_test;
      }
    }

    $preferred = FALSE;
    if ($target === 'workflow' && count($target_data) === 2) {
      $workflows = $this->getNodeState()->getWorkflows();
      if (count($workflows) === 2) {
        $preferred = ['best' => [
            'id' => $best_id,
            'number' => $best_number,
        ]];
      }
    }

    if (empty($preferred) && !empty($best_id)) {
      $preferred = ['best' => [
          'id' => $best_id,
          'number' => $best_number,
      ]];

      $d_factor = in_array($target, ['workflow', 'severity']) ? (3 / 4) : (1 / 2);
      $decisive1 = (integer) ($requests * $d_factor);
      if ($best_number < $decisive1) {
        // search for next
        $next_id = '';
        $next_number = 0;
        foreach ($target_data as $key => $number) {
          if ($key !== $best_id) {
            $number_test = $number;
            if ($key === $s_values[$target]) {
              $number_test += 0.5;
            }
            if (empty($next_id) || $number_test > $next_number) {
              $next_id = $key;
              $next_number = $number_test;
            }
          }
        }

        if ($next_number > $best_number) {
          // flip values
          $tmp_id = $best_id;
          $tmp_number = $best_number;
          $best_id = $next_id;
          $best_number = $next_number;
          $next_id = $tmp_id;
          $next_number = $tmp_number;
        }

        // has equals ?
        $best_data = [];
        foreach ($target_data as $key => $number) {
          if ($number >= $target_data[$next_id]) {
            $best_data[$key] = $number;
          }
        }

        $decisive2 = (integer) ($requests * 3 / 8);
        $decisive3 = (integer) ($requests * 1 / 2);
        if (($best_number > $next_number || count($best_data) < 3) && $next_number >= $decisive2) {
          $preferred['next'] = [
            'id' => $next_id,
            'number' => $next_number,
          ];
        }
        elseif ($best_number < $decisive3) {
          if ($target === 'workflow') {
            $preferred = $this->getPreferredWorkflow($best_id, $next_id, $best_data);
          }
        }

        if ($target === 'severity' && !empty($preferred['next'])) {
          $preferred = $this->getPreferredSeverity($best_id, $next_id, $target_data);
        }
      }
    }

    if (empty($preferred)) {
      $preferred = ['best' => [
          'id' => $s_values[$target],
          'number' => 0,
      ]];
    }
    $this->preferred[$target] = $preferred;
    return $this->preferred[$target];
  }

  protected function getPreferredWorkflow($best_id, $next_id, $best_data) {
    $preferred = [];
    if ($best_data[$best_id] > $best_data[$next_id]) {
      $set_best_id = $best_id;
      $preferred['best'] = [
        'id' => $best_id,
        'number' => $best_data[$best_id],
      ];
    }

    //for equal votes prefer by order of workflows
    $workflows = $this->getNodeState()->getWorkflows();
    foreach ($workflows as $workflow_id => $workflow) {
      if (empty($preferred['best'])) {
        $set_best_id = $workflow_id;
        $preferred['best'] = [
          'id' => $workflow_id,
          'number' => $best_data[$workflow_id],
        ];
      }
      elseif ($workflow_id !== $set_best_id) {
        $preferred['next'] = [
          'id' => $workflow_id,
          'number' => $best_data[$workflow_id],
        ];
        break;
      }
    }

    return $preferred;
  }

  protected function getPreferredSeverity($best_id, $next_id, $all_data) {
    $last_num = 0;
    $severities = SlogXtwf::getStateSeverityOptions();
    foreach ($severities as $severity_id => $severity) {
      $num = ($all_data[$severity_id] ?? 0);
      if ($severity_id !== $best_id && $num > $last_num) {
        $next_id = $severity_id;
        $last_num = $num;
      }
    }

    $weight = [
      'leight' => 0,
      'medium' => 1,
      'severe' => 2,
      'severest' => 3,
    ];

    $best_plus = 0;
    $next_plus = 0;
    $divider = 1000;
    $precision = 3;
    if (abs($weight[$best_id] - $weight[$next_id]) > 1) {
      if ($best_id === 'leight' && $next_id === 'severest') {
        $best_id = 'medium';
        $next_id = 'severe';
        $best_plus = $all_data['leight'] / $divider;
        $next_plus = $all_data['severest'] / $divider;
      }
      elseif ($best_id === 'leight') {
        $next_id = 'medium';
        $next_plus = $all_data['leight'] / $divider;
      }
      elseif ($best_id === 'severest' && $next_id === 'leight') {
        $best_id = 'severe';
        $next_id = 'medium';
        $best_plus = $all_data['severest'] / $divider;
        $next_plus = $all_data['leight'] / $divider;
      }
      elseif ($best_id === 'severest') {
        $next_id = 'severe';
        $next_plus = $all_data['severest'] / $divider;
      }
      elseif ($best_id === 'severe') {
        $next_id = 'medium';
        $next_plus = $all_data['severe'] / $divider;
      }
      elseif ($best_id === 'medium') {
        $next_id = 'severe';
        $next_plus = $all_data['medium'] / $divider;
      }
    }

    $best_num = ($all_data[$best_id] ?? 0) + $best_plus;
    $next_num = ($all_data[$next_id] ?? 0) + $next_plus;
    $preferred = [
      'best' => [
        'id' => $best_id,
        'number' => round($best_num, $precision),
      ],
      'next' => [
        'id' => $next_id,
        'number' => round($next_num, $precision),
      ],
    ];

    return $preferred;
  }

  protected function severitySplitter($val) {
    $str = (string) $val;
    $splitted = explode(".", $str);
    $whole = (integer) $splitted[0];
    $ret_val = $whole;
    $num = (integer) ($splitted[1] ?? 0);
    if ($num > 0) {
      $ret_val = "$whole/$num";
    }

    return $ret_val;
  }

  protected function getTargetKeys() {
    $keys =  ['workflow', 'severity', 'finalize', 'strict'];
    $node_state = $this->getNodeState();
    if ($node_state->hasDiscussionState()) {
      $keys[] = 'discuss';
    }
    return $keys;
  }

  protected function getDefaultOption($target) {
    $node_state = $this->getNodeState();
    if ($target === 'workflow') {
      return $node_state->getWorkflowId();
    }
    elseif ($target === 'severity') {
      return $node_state->getSeverityId();
    }
    elseif ($target === 'finalize') {
      return 'requests';
    }

    return FALSE;
  }

  public function setUserData($uid, array $value) {
    $node_state = $this->getNodeState();
    $user_data = $node_state->getUserData();
    $target_keys = $this->getTargetKeys();
    foreach ($target_keys as $target) {
      if (empty($value[$target])) {
        $args = [
          '%target' => $target,
          '%node' => $node_state->id(),
          '%uid' => $uid,
        ];
        $message = t('Target %target not found in user request:  node: %node, user: %uid.', $args);
        throw new \LogicException($message);
      }

      $new_target = $value[$target];
      if (!empty($user_data[$uid])) {
        $old_target = $user_data[$uid][$target] ?: FALSE;
        if (!empty($new_target) && $new_target !== $old_target) {
          $this->_removeRequestSummarize($target, $old_target);
          $this->_addRequestSummarize($target, $new_target);
        }
      }
      else {
        $this->_addRequestSummarize($target, $new_target);
      }
    }

    $user_data[$uid] = $value;
    $node_state->setUserData(-1, $user_data);
  }

  public function deleteUserData($uid, $preserve = FALSE) {
    $node_state = $this->getNodeState();
    if ($node_state->hasUserData($uid)) {
      $user_data = $node_state->getUserData();
      $history = $node_state->getHistory();
      if (count($user_data) < 2 && !$preserve && empty($history)) {
        $node_state->delete();
      }
      else {
        $target_keys = $this->getTargetKeys();
        foreach ($target_keys as $target) {
          $target_id = $user_data[$uid][$target] ?: FALSE;
          $this->_removeRequestSummarize($target, $target_id);
        }
        unset($user_data[$uid]);
        $node_state->setUserData(-1, $user_data)->save();
      }
    }
  }

  protected function _sortRequestSummarize($target, $data) {
    arsort($data);
    $values = array_values($data);
    if (count($values) > 1 && $values[0] === $values[1]) {
      $keys = array_keys($data);
      $preferred = $this->getDefaultOption($target);
      if (!empty($preferred) && $preferred == $keys[1]) {
        $tmp_data = $data;
        $data = array_splice($tmp_data, 1, 1) + $tmp_data;
      }
    }

    return $data;
  }

  protected function _addRequestSummarize($target, $target_id) {
    $node_state = $this->getNodeState();
    $node_state_data = $node_state->getData('state_data');
    $target_key = "request_$target";
    $data = ($node_state_data[$target_key] ?? []);
    if (empty($data[$target_id])) {
      $data[$target_id] = 0;
    }

    $data[$target_id] += 1;
    $node_state_data[$target_key] = $this->_sortRequestSummarize($target, $data);
    $node_state->setData($node_state_data, 'state_data');
  }

  protected function _removeRequestSummarize($target, $target_id) {
    $node_state = $this->getNodeState();
    $node_state_data = $node_state->getData('state_data');
    $target_key = "request_$target";
    $data = ($node_state_data[$target_key] ?? []);
    if (!empty($data[$target_id]) && $data[$target_id] > 0) {
      $data[$target_id] -= 1;
      if ($data[$target_id] === 0) {
        unset($data[$target_id]);
      }
      $node_state_data[$target_key] = $this->_sortRequestSummarize($target, $data);
      $node_state->setData($node_state_data, 'state_data');
    }
    else {
      $args = [
        '%node' => $node_state->id(),
        '%target' => $target_id,
      ];
      $msg = t("Request $target < 0: node: %node, $target: %target.", $args);
      SlogXtwf::logger()->error($msg);
    }
  }

  protected function getNeededCompetitions() {
    return FALSE;
  }

  /**
   * 
   * @return string
   */
  protected function getRequestInfo() {
    $notice = $this->getNodeState()->getRequestNotice();
    $info = [
      'label' => t('Reason for request'),
      'notice' => SlogXt::htmlHighlightText($notice),
    ];

    return implode(': ', $info);
  }

  protected function getToPrependInfos($for_subtitle, array $prepend = []) {
    $request_info = ['rinfo' => $this->getRequestInfo()];
    return parent::getToPrependInfos($for_subtitle, $request_info);
  }

  public function prepareRestoredData(&$data, $args) {
    if (!empty($args['request_data'])) {
      $data['request_data'] = $args['request_data'];
    }
  }

}
