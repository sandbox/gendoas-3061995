<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfPluginStateBase.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_workflow\SxtWorkflowStateInterface;
use Drupal\slogxt\SlogXt;
use Drupal\user\Entity\User;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfPluginBase;

/**
 */
class XtwfPluginStateBase extends XtwfPluginBase implements XtwfPluginStateInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $workflow = $form_state->getFormObject()->getEntity();
    $workflow_id = $workflow->id();
    $xtwf_state = $form_state->get('state');
    $state_id = $xtwf_state->id();
    $use_severity = $xtwf_state->getUseSeverity();
    $severity_settings = $xtwf_state->getSeveritySettings() ?: FALSE;

    $form['state_type'] = [
      '#type' => 'textfield',
      '#title' => t('State type'),
      '#value' => $xtwf_state->getStateType(),
      '#disabled' => TRUE,
    ];

    if ($state_id === SlogXtwf::XTWF_FINISH_FINALLY) {
      $msg = t('State for closed workflow. The state has no more settings.');
      $form['err_notice'] = [
        '#markup' => SlogXt::htmlMessage($msg, 'warning'),
        '#weight' => -999,
      ];
    }
    elseif ($use_severity && $severity_settings) {
      $container_title = t('Severity settings');
      $route = [
        'name' => 'sxt_workflow.state_severity_edit_form',
        'params' => [
          'workflow' => $workflow_id,
          'state_id' => $state_id,
        ],
      ];

      $form['severity_container']['number_description'] = [
        '#type' => 'textfield',
        '#title' => t('Number description'),
        '#description' => t('Description for the kind of required items.'),
        '#default_value' => $xtwf_state->getNumberDescription(),
      ];
      $form['severity_container']['number_suffix'] = [
        '#type' => 'textfield',
        '#title' => t('Number target'),
        '#description' => t('The label for required items.'),
        '#default_value' => $xtwf_state->getNumberSuffix(),
      ];

      $form['severity_container'] += self::buildSeverityList($state_id, $container_title, $severity_settings, $route);
    }
    else {
      if ($xtwf_state->getUseDynamicSeverity()) {
        $args = ['@timeout' => SlogXtwf::getDynamicTimeout()];
        $msg = t('Requested number is set dynamically, timeout (=@timeout) is set by global configuration.', $args);
        $form['err_notice'] = [
          '#markup' => SlogXt::htmlMessage($msg, 'warning'),
          '#weight' => -999,
        ];
      }
      else {
        $msg = t('....not handled state');
        $form['err_notice'] = [
          '#markup' => SlogXt::htmlMessage($msg, 'error'),
          '#weight' => -999,
        ];
      }
    }

    return $form;
  }

  public static function buildSeverityList($state_id, $title, array $settings, array $route) {
    $list_container = [
      '#type' => 'details',
      '#title' => $title,
      '#open' => TRUE,
    ];
    $list_container['severity'] = [
      '#type' => 'table',
      '#header' => [
        'type' => t('Severity'),
        'operations' => t('Operations')
      ],
      '#empty' => t('There are no severity items.'),
    ];

    $route_name = $route['name'];
    $route_params = $route['params'];
    $severity_labels = SlogXtwf::getStateSeverityOptions();
    foreach ($settings as $severity_id => $severity) {

      $value_items = [];
      foreach ($severity as $id => $value) {
        $svalue = is_bool($value) ? ($value ? 'TRUE' : 'FALSE') : (is_array($value) ? 'array' : ($value ?: 'none'));
        $value_items[$id] = "$id: $svalue";
      }

      $value_list = [
        '#theme' => 'item_list',
        '#items' => $value_items,
        '#context' => ['list_style' => 'comma-list'],
      ];

      $values_key = "$state_id-$severity_id";
      $route_params['severity_id'] = $severity_id;
      $list_container['severity'][$values_key] = [
        'type' => [
          '#type' => 'inline_template',
          '#template' => '<strong>{{ label }}</strong></br><span id="values-{{ values_key }}">{{ value_list }}</span>',
          '#context' => [
            'label' => ($severity_labels[$severity_id] ?? '_???_buildSeverityList'),
            'values_key' => $values_key,
            'value_list' => $value_list,
          ]
        ],
        'operations' => [
          '#type' => 'operations',
          '#links' => [
            'select' => [
              'title' => t('Edit'),
              'url' => Url::fromRoute($route_name, $route_params),
              'attributes' => [
                'class' => ['use-ajax'],
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode([
                  'width' => 700,
                ]),
              ],
            ],
          ],
        ],
      ];
    }

    return $list_container;
  }

  public static function buildSeverityForm(SxtWorkflowStateInterface $xtwf_state, $severity_id, $ajaxcallback) {
    $severity = $xtwf_state->getSeverity($severity_id);

    $form['number'] = [
      '#type' => 'textfield',
      '#title' => t('Min items'),
      '#description' => t('Number of items required to finish the state.'),
      '#default_value' => $severity['number'],
      '#size' => 10,
      '#field_suffix' => $xtwf_state->getNumberSuffix(),
    ];

    $form['timeout'] = [
      '#type' => 'textfield',
      '#title' => t('Timeout'),
      '#description' => t('Number of days after which the state is finished at the latest.'),
      '#default_value' => $severity['timeout'],
      '#size' => 10,
      '#field_suffix' => t('days'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('Save'),
      '#ajax' => [
        'callback' => $ajaxcallback,
      ],
    ];
    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => t('Cancel'),
      '#ajax' => [
        'callback' => $ajaxcallback,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formEditAlter(&$form, FormStateInterface $form_state) {
    $state_id = $this->getPluginId();
    if ($state_id !== SlogXtwf::XTWF_REQUEST_COLLABORATE) {
      $workflow = $form_state->getFormObject()->getEntity();
      $xtwf_type_plugin = $workflow->getTypePlugin();
      $t_required = $xtwf_type_plugin->getTransition($state_id, FALSE);
      if (!$t_required) {
        $msg = t('There is a requirement missmatch. Save again to add required transition.');
        $form['err_notice'] = [
          '#markup' => SlogXt::htmlMessage($msg, 'error'),
          '#weight' => -999,
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $state_id = $this->getPluginId();
    $needs_required = FALSE;
    if ($state_id !== SlogXtwf::XTWF_REQUEST_COLLABORATE) {
      $needs_required = TRUE;
      if (!$required_transition = SlogXtwf::getTransitionPlugin($state_id)) {
        $form_state->setErrorByName('id_selected', t('Internal error. Action not executable.'));
        $msg = t('There is no transition corresponding to the status: @state', ['@state' => $state_id]);
        SlogXtwf::logger()->error($msg);
        // stop handling
        return;
      }
    }

    $values = $form_state->getCompleteFormState()->getValues();
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();
    $configuration = $xtwf_type_plugin->getConfiguration();
    $xtwf_state = $form_state->get('state');
    $use_severity = $xtwf_state ? $xtwf_state->getUseSeverity() : FALSE;

    $has_required = FALSE;
    if ($xtwf_state) {
      // transition with identical id is required
      $xtwf_transition = $xtwf_type_plugin->getTransition($state_id, FALSE /* no exception */);
      $has_required = (boolean) $xtwf_transition;
    }

    if ($xtwf_state && $use_severity) {
      // prepare values for update
      $values = $form_state->getValues();
      $settings = & $configuration['states'][$state_id]['settings'];
      $settings['number_description'] = $values['severity_container']['number_description'];
      $settings['number_suffix'] = $values['severity_container']['number_suffix'];
    }
    else {
      // has to be handled here, not in submitConfigurationForm()
      // new state is already added to $xtwf_type_plugin
      // add settings to added state from definition
      $configuration['states'][$state_id]['settings'] = $this->getSettings();
    }

    $xtwf_type_plugin->setConfiguration($configuration);
    if ($needs_required && !$has_required) {
      $t_label = $required_transition->getLabel();
      $s_definitions = SlogXtwf::getDefinitions('state');
      $state_ids = array_keys($s_definitions);
      $t_settings = $required_transition->getSettings();
      $required_from = array_intersect($state_ids, $t_settings['required_from']);
      if (!empty($t_settings['exclude_from'])) {
        $required_from = array_diff($required_from, $t_settings['exclude_from']);
      }

      $settings = $configuration['states'][$state_id]['settings'];
      $state_type = $settings['state_type'] ?: FALSE;
//todo::current:: ----sxt_sxt_workflow --- transition/from/to
      if ($state_type && $state_type !== 'finished') {
        $required_from[] = SlogXtwf::XTWF_FINISH_RENEWABLE;
        $xtwf_type_plugin->addTransitionFromStates(SlogXtwf::XTWF_FINISH_RENEWABLE, [$state_id]);
      }

      $xtwf_type_plugin->addTransition($state_id, $t_label, $required_from, $state_id);
    }

//    $form_state->setErrorByName('xxxxx', t('.XtwfPluginStateBase....validateConfigurationForm.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // nothing by default
  }

  public function toFinalizeByTimeout() {
    return $this->getNodeState()->getFinalizeByTimeout();
  }

  public function toFinalizeStrict() {
    return $this->getNodeState()->getFinalizeStrict();
  }

  public function toDiscuss() {
    return FALSE;
  }

  protected function getStartedPrefix($action) {
    $labels = [
      'renew' => t('Renewed by'),
      'restore' => t('Restored by'),
      'terminate' => t('Terminated by'),
    ];
    $default = t('Started by');
    return (string) ($labels[$action] ?? $default);
  }

  protected function getFinalizedPrefix($action) {
    $labels = [
      'terminate' => t('Terminated by'),
    ];
    $default = t('Finalized by');
    return (string) ($labels[$action] ?? $default);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoFinalized($for_subtitle = FALSE) {
    $node_state = $this->getNodeState();
    $finalized_by = $node_state->getData('finalized_by') ?: FALSE;
    if ($finalized_by) {
      $info = [];
      if ($finalized_by === 'admin') {
        $info = $this->getAdminFinalizedInfo($for_subtitle);
      }
      else {
        $fin_label = $this->getFinalizedPrefix($finalized_by);
        $fin_info = SlogXt::htmlHighlightText(SlogXtwf::getFinalizedByText($finalized_by));
        $info = "$fin_label: $fin_info";
      }

      return $info;
    }

    return FALSE;
  }

  /**
   * 
   * 
   * @param boolean $for_subtitle
   * @return string or FALSE
   */
  protected function getAdminFinalizedInfo($for_subtitle = FALSE) {
    $node_state = $this->getNodeState();
    if ($for_subtitle) {
      // from historie
      $history_data = $node_state->getHistoryData($node_state->getStarted());
      $admin_data = $history_data['data']['admin_data'] ?? FALSE;
    }
    else {
      $admin_data = $node_state->getAdminData() ?? FALSE;
    }

    if ($admin_data) {
      $prefix = $for_subtitle ? '' : SlogXtwf::XTWF_PREFIX_DASH;
      $admin_action = $admin_data['admin_action'] ?? FALSE;
      $admin_notice = $admin_data['admin_notice'] ?? '_???_getAdminFinalizedInfo';
      $admin_uid = $admin_data['admin_uid'] ?? FALSE;
      $alabel = SlogXtwf::getFinalizedByText('admin');

      if (SlogXt::isSuperUser()) {
        $user_name = $admin_uid ? User::load($admin_uid)->label() : '';
        $ainfo = "$alabel ($admin_uid, $user_name)";
      }
      elseif (SlogXtwf::getAdminUidShow()) {
        $ainfo = "$alabel ($admin_uid)";
      }

      if ($for_subtitle) {
        if ($node_state->isStateDiscussion()) {
          $fin_label = $this->getStartedPrefix('default');
        }
        else {
          $fin_label = $this->getStartedPrefix($admin_action);
        }
      }
      else {
        $fin_label = $this->getFinalizedPrefix($admin_action);
      }
      $fin_info = SlogXt::htmlHighlightText($ainfo);
      $info['admin_finalized'] = "$fin_label: $fin_info";

      $reason = t('Reason');
      $notice = SlogXt::htmlHighlightText($admin_notice);
      $info['admin_reason'] = "$prefix{$reason} ($admin_action): $notice";

      $separator = $for_subtitle ? '; ' : '<br />';
      return implode($separator, $info);
    }

    return FALSE;
  }

  /**
   * Return the number of all posts
   * 
   * - Do not override this function.
   * - Override getPostsRelevant instead
   * 
   * @return integer
   */
  protected function getPostsAll() {
    $user_data = $this->getNodeState()->getUserData();
    $number = is_array($user_data) ? count($user_data) : (integer) $user_data;
    return $number;
  }

  /**
   * Return the number of all posts
   * 
   * - Override this function if some posts are not relevant.
   * - See collect contents: drafts are not relevant
   * 
   * @return integer
   */
  public function getPostsRelevant() {
    return $this->getPostsAll();
  }

  protected function getMarker($warn = FALSE) {
    return SlogXt::htmlHighlightText('**', $warn);
  }

  protected function getInfoGenerated() {
    $generated = $this->getNodeState()->getData('generated') ?: FALSE;
    if ($generated) {
      $gtmp = explode(';', $generated);
      $number = count($gtmp);
      if ($number > 12) {
        $gtmp = array_slice($gtmp, 0, 10);
        $gtmp[] = '...';
        $generated = implode(';', $gtmp);
      }

      $label = t('Generated');
      return SlogXt::htmlHighlightText("$label: $number ($generated)", TRUE);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStateInfo($prepend_base = TRUE) {
    $node_state = $this->getNodeState();
    $nstate_data = $node_state->getData();
    $user_data = $node_state->getUserData();
    $started = $nstate_data['started'];
    $changed = $nstate_data['changed'];
    if (!is_numeric($started) || !is_numeric($changed)) {
      $args = ['@started' => $started, '@changed' => $changed];
      return [t('ERROR:Unvalid started or changed (@started/@changed)', $args)];
    }

    $sv_required = $node_state->getSeverityRequired();
    $sv_timeout = $node_state->getSeverityTimeout();
    $items_label = $node_state->getWfStateNumSuffix();
    $posts_number = $this->getPostsRelevant();

    $date_formater = $this->getXtDateFormater();
    $timeout_timestamp = $this->xtDateAddDays($started, $sv_timeout);
    $days_left = $this->xtDateDiffDays($changed, $timeout_timestamp);
    $unlimited = ($days_left > 3000);
    $timeout_dt = $unlimited ? t('unlimited') : $date_formater->format($timeout_timestamp, 'xt_date_only');
    $f_started = $date_formater->format($started, 'short');
    $f_changed = $date_formater->format($changed, 'short');
    $marker = $node_state->hasUserData(-1) ? $this->getMarker() : '';
    $args = [
      '@started' => $f_started,
      '@changed' => $f_changed,
      '@items_label' => $items_label,
      '@posts' => $posts_number,
      '@timeout_dt' => $timeout_dt,
      '@left' => max($days_left, 0),
      '@timeout' => $sv_timeout,
      '@required' => $sv_required,
    ];

    $prefix = SlogXtwf::XTWF_PREFIX_DASH;
    if ($node_state->stateIsFinishedType()) {
      if ($node_state->isStateFinishedFinally()) {
        $extra = (string) t('Collaboration can no longer be continued');
        $info = [
          'started' => (string) t('Finished finally on @started', $args),
          'extra' => SlogXt::htmlHighlightText($extra),
        ];
      }
      else {
        $request_renew = t('Request for renew');
        $info = [
          'started' => (string) t('Finished on @started', $args),
          'extra' => (string) t('Collaboration can be continued by: ') . SlogXt::htmlHighlightText($request_renew),
          'timeout' => $prefix . t('Timeout: @timeout_dt', $args),
        ];
      }
    }
    else {
      $info = [
        'started' => (string) t('Started on @started', $args),
        'changed' => (string) t('Last changed on @changed', $args),
        'posts' => $prefix . t('@items_label: @posts', $args) . $marker,
        'timeout' => $prefix . t('Timeout: @timeout_dt', $args),
      ];
      if ($this->toFinalizeByTimeout()) {
        $info['timeout'] = SlogXt::htmlHighlightText($info['timeout']);
      }
      else {
        $info['posts'] = SlogXt::htmlHighlightText($info['posts']);
      }

      $info['posts'] .= ' (' . t('Required: @required', $args) . ')';
      if (!$unlimited) {
        $info['timeout'] .= ' (' . t('Left: @left of @timeout days', $args) . ')';
      }

      if ($f_changed === $f_started) {
        unset($info['changed']);
      }
    }

    if ($finalized = $this->getInfoFinalized()) {
      $info['finalized_by'] = $finalized;
    }
    if ($generated = $this->getInfoGenerated()) {
      $info['generated'] = $generated;
    }

    $state_info = [];
    if ($prepend_base && $prepend_infos = $this->getToPrependInfos(FALSE)) {
      $state_info['prepend'] = $prepend_infos;
    }
    $state_info = array_merge($state_info, $info);
    if ($more_info = $this->getInfoMore()) {
      $state_info = array_merge($state_info, $more_info);
    }
    return $state_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoBase($as_array = FALSE) {
    $node_state = $this->getNodeState();
    $severity_labels = SlogXtwf::getStateSeverityOptions();
    $sv_id = $node_state->getSeverityId();
    $info = [
      'workflow' => t('Workflow: ') . SlogXt::htmlHighlightText($node_state->getWorkflow()->label()),
      'severity' => t('Severity: ') . SlogXt::htmlHighlightText($severity_labels[$sv_id]),
    ];
    if (!$node_state->isStateRequestFinishFinally() && !$node_state->isStateFinishedFinally()) {
      if ($node_state->getFinalizeByTimeout()) {
        $finalize_label = t('Timeout');
      }
      else {
        $finalize_label = $node_state->getWfStateNumSuffix();
      }
      $info['finalize'] = t('Finalize by') . ': ' . SlogXt::htmlHighlightText($finalize_label);

      if ($node_state->getFinalizeStrict()) {
        $finalize_label = t('Yes');
      }
      else {
        $finalize_label = t('No');
      }
      $info['strict'] = t('Strict: ') . SlogXt::htmlHighlightText($finalize_label);
    }

    if ($as_array) {
      return $info;
    }

    return implode(', ', $info);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    return FALSE;
  }

  /**
   * Return infos to prepend.
   * 
   * - Used for subtitle in listings (history)
   * - Used as part of state info
   * 
   * @param boolean $for_subtitle
   *  
   * @param array $prepend
   * @return string or FALSE
   *  return FALSE if there is no info to prepend
   */
  protected function getToPrependInfos($for_subtitle, array $prepend = []) {
    $_tpp_ = FALSE ? '_tpp_' : '';
    $info = $prepend;
    $finalized_by = $this->getNodeState()->getData('finalized_by') ?? FALSE;
    if ($finalized_by) {
      if ($finalized_info = $this->getInfoFinalized($for_subtitle)) {
        $info['finfo'] = $_tpp_ . $finalized_info;
      }
    }
    elseif ($admin_info = $this->getAdminFinalizedInfo(TRUE)) {
      $info['ainfo'] = $_tpp_ . $admin_info;
    }
    if ($base_info = $this->getInfoBase()) {
      $info['binfo'] = $_tpp_ . $base_info;
    }

    if (!empty($info)) {
      return implode('<br />', $info);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubtitleInfo() {
    return ($this->getToPrependInfos(TRUE) ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityInfos(SxtWorkflowStateInterface $xtwf_state, $severity_id = NULL) {
    if ($xtwf_state->getUseSeverity()) {
      $settings = ($xtwf_state->getSettings() ?? []);
      $severities = ($settings['severity'] ?? []);

      $infos = [];
      foreach (array_keys($severities) as $severity_id) {
        $infos[$severity_id] = $xtwf_state->getSeverityDescription($severity_id);
      }
      return $infos;
    }

    return t('State does not use severity.');
  }

  public function prepareNextState() {
    return FALSE;
  }

  public function prepareRestoredData(&$data, $args) {
    return;
  }

  public function preFinalize($finalized_by, array $args) {
    $next_state = FALSE;
    $node_state = $this->getNodeState();
    if ($finalized_by === 'admin') {
      $action = $args['action'] ?: FALSE;
      if (in_array($action, ['terminate', 'abort'])) {
        if (!empty($args['severity']) && !empty($args['finalize_by_timeout'])) {
          $this->setSeverity($args['severity'])->setData($args['finalize_by_timeout'], 'finalize_by_timeout');
        }
        $next_state_id = $node_state->isStateFinishedRenewable() ? SlogXtwf::XTWF_FINISH_FINALLY : SlogXtwf::XTWF_FINISH_RENEWABLE;
        $next_state = $node_state->getWfState($next_state_id);
      }
      else {
        $next_state = $this->preFinalizeAdmin($args);
      }
    }

    return $next_state;
  }

  public function postFinalize() {
    return FALSE;
  }

  protected function preFinalizeAdmin($args) {
    return FALSE;
  }

  public function postFinalizeAdmin($args) {
    return FALSE;
  }

}
