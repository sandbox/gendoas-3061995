<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfPluginStateInterface.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SxtWorkflowStateInterface;
use Drupal\sxt_workflow\Entity\XtwfNodeStateInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the interface for xtwf state plugins.
 */
interface XtwfPluginStateInterface {

  /**
   * Build configuration form for the state plugin.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @param StateInterface $state
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state);

  /**
   * 
   */
  public function formEditAlter(&$form, FormStateInterface $form_state);

  /**
   * 
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state);

  /**
   * 
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state);

  /**
   * Return verbose infos for for the current state of the node state.
   * 
   * @param boolean $prepend_base
   * @return array
   */
  public function getStateInfo($prepend_base = TRUE);

  /**
   * Return base state info (eg. workflow and severity) or FALSE.
   * 
   * @param boolean $as_array
   *  Maybe infos as array is needed
   * @return string or array or FALSE
   *  Return FALSE if no info should be shown
   */
  public function getInfoBase($as_array = FALSE);

  /**
   * Return additional infos for node state (state dependend).
   * 
   * This infos are appended to state infos.
   * 
   * @return array or FALSE
   *  FALSE if there are no additional infos
   */
  public function getInfoMore();

  /**
   * Return finalized by info if state has been finalized.
   * 
   * This infos are appended to state infos.
   * 
   * @return string or FALSE
   */
  public function getInfoFinalized($for_subtitle = FALSE);

  /**
   * Return infos for subtitle in listings (history).
   * 
   * @return string
   */
  public function getSubtitleInfo();

  /**
   * 
   */
  public function getSeverityInfos(SxtWorkflowStateInterface $xtwf_state, $severity_id = NULL);

  public function prepareNextState();

  public function preFinalize($finalized_by, array $args);

  /**
   * Do tasks after transition to a new state.
   * 
   * This may be to cleanup node state data, that is good for the state only..
   * 
   */
  public function postFinalize();

}
