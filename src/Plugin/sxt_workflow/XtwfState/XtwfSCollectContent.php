<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSCollectContent.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_collect_content",
 *   label = @Translation("Content collection"),
 *   settings = {
 *     "state_type" = "collect_content",
 *     "number_suffix" = @Translation("relevant posts"),
 *     "number_description" = @Translation("NOTE: Posts are relevant after they have been shared."),
 *   },
 *   weight = 100
 * )
 */
class XtwfSCollectContent extends XtwfPluginSCollectContentBase {

}
