<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSCompetition.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_competition",
 *   label = @Translation("Competition"),
 *   settings = {
 *     "state_type" = "competition",
 *     "use_dynamic_severity" = TRUE,
 *     "number_suffix" = @Translation("votes"),
 *   },
 *   weight = 2000
 * )
 */
class XtwfSCompetition extends XtwfPluginStateBase {

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    $node_state = $this->getNodeState();
    $nstate_data = $node_state->getData();
    $competition_data = $node_state->getCompetitionData();
    $competitions = $competition_data['competitions'];
    if (empty($competitions)) {
      $err_msg = t('Error: There are no competition data.');
      $more_info['error'] = SlogXt::htmlHighlightText($err_msg, TRUE);
      //
      //
      return $more_info;
      //
    //
    }

    $for_state = $node_state->getWfState($competition_data['state_id']);
    $for_line = t('Competition for: @label', ['@label' => $for_state->label()]);
    $prefix = SlogXtwf::XTWF_PREFIX_DASH;
    $more_info = ['for_title' => SlogXt::htmlHighlightText($for_line)];
    foreach ($competitions as $competition) {
      $more_info[] = $prefix . $competition['description'];
    }
    $more_info['more_title'] = t('Vote data:');

    $txt_info = (string) t('none');
    if ($node_state->hasUserData()) {
      $state_data = $nstate_data['state_data'];
      $winner_data = $this->getWinnerData();
      foreach ($competitions as $competition) {
        $target = $competition['target'];
        $highlight_id = ($winner_data[$target] ?? FALSE);
        $target_key = "vote_$target";
        $best = $competition['preferred']['best'];

        $detail_info = [];
        if ($target === 'workflow' && !empty($state_data[$target_key])) {
          $d_info_text = t('Workflow');
          $wf_labels = $node_state->getWorkflowLabels();
          foreach ($state_data[$target_key] as $key => $value) {
            $detail_info[$key] = $wf_labels[$key] . ' (' . $value . ')';
          }
        }
        elseif ($target === 'renew_id' && !empty($state_data[$target_key])) {
          $d_info_text = t('Next');
          $rid_labels = $node_state->getOptionsRequestRenew();
          foreach ($state_data[$target_key] as $key => $value) {
            $detail_info[$key] = $rid_labels[$key] . ' (' . $value . ')';
          }
        }
        elseif ($target === 'severity' && !empty($state_data[$target_key])) {
          $d_info_text = t('Severity');
          $severity_labels = SlogXtwf::getStateSeverityOptions();
          foreach ($state_data[$target_key] as $key => $value) {
            $detail_info[$key] = $severity_labels[$key] . ' (' . $value . ')';
          }
        }


        $d_info = $txt_info;
        if (!empty($detail_info)) {
          if (!$highlight_id) {
            $highlight_id = $best['id'];
          }
          $detail_info[$highlight_id] = SlogXt::htmlHighlightText($detail_info[$highlight_id]);
          $d_info = implode('; ', $detail_info);
        }
        if (!empty($d_info_text)) {
          $more_info[$target] = $prefix . $d_info_text . ': ' . $d_info;
        }
      }
    }
    else {
      $more_info['more_title'] .= ' ' . SlogXt::htmlHighlightText($txt_info);
    }

    return $more_info;
  }

  protected function getDynamicRequired($required) {
    return (integer) ($required * 3 / 4);
  }

  public function prepareCompetitionState($competitions) {
    $node_state = $this->getNodeState();
    $data = $node_state->getData();
    $data['competition_data'] = [
      'competitions' => $competitions,
      'state_id' => $node_state->getStateId(),
      'state_data' => $data['state_data'],
      'finalize_by_saved' => $data['finalize_by_timeout'],
    ];
    $required = $node_state->getSeverityRequired();
    $data['dynamic_required'] = $this->getDynamicRequired($required);
    $data['finalize_by_timeout'] = FALSE;
    $data['preserve_prepared'] = ['competition_data', 'dynamic_required'];

    $node_state->setData($data);
  }

  public function prepareNextState() {
    $node_state = $this->getNodeState();
    $data = $node_state->getData();
    $parent_state_id = $data['competition_data']['state_id'];
    $next_state = $node_state->getWorkflowTypePlugin()->findNextState($parent_state_id, TRUE);

    return $next_state->id();
  }

  public function preFinalize($finalized_by, array $args) {
    $node_state = $this->getNodeState();
    $all_values = $node_state->getValues();
    $winner_data = $this->getWinnerData();

    $competition_data = $node_state->getCompetitionData();
    $new_state_data = $competition_data['state_data'];
    $new_state_data['finalize_by_timeout'] = (boolean) $competition_data['finalize_by_saved'];
    foreach ($winner_data as $target => $winner_id) {
      $key = "request_$target";
      $new_state_data[$key] = [$winner_id => 99];
    }

    $all_values['state'] = $competition_data['state_id'];
    $all_values['data']['state_data'] = $new_state_data;
    $node_state->setValues($all_values);
  }

  /**
   * {@inheritdoc}
   */
  public function postFinalize() {
    $node_state = $this->getNodeState();
    $data = $node_state->getData();
    unset($data['competition_data']);
    unset($data['dynamic_required']);
    $node_state->setData($data)->save();
  }

  protected function preFinalizeAdmin($args) {
    $node_state = $this->getNodeState();
    $all_values = $node_state->getValues();
    if (!empty($args['workflow'])) {
      $all_values['workflow'] = $args['workflow'];
    }
    if (!empty($args['renew_id'])) {
      $all_values['renew_id'] = $args['renew_id'];
    }
    if (!empty($args['severity'])) {
      $all_values['severity'] = $args['severity'];
    }

    $node_state->setValues($all_values);
    return FALSE;   // no $next_state
  }

  public function prepareRestoredData(&$data, $args) {
    $data['finalize_by_timeout'] = FALSE;
    $competition_data = & $data['competition_data'];
    $competition_data['finalize_by_saved'] = (boolean) $args['finalize_by_timeout'];

    $request_state_id = $competition_data['state_id'] ?: FALSE;
    $severity_id = $args['severity'] ?: FALSE;
    if ($request_state_id && $severity_id) {
      $xtwf_state = $this->getNodeState()->getWfState($request_state_id);
      $dynamic_required = $data['dynamic_required'];
      $required = $xtwf_state->getSeverityRequired($severity_id, $dynamic_required);
      if ($required !== $dynamic_required) {
        $data['dynamic_required'] = $this->getDynamicRequired($required);
      }
    }
  }

  public function setUserData($uid, array $value) {
    $node_state = $this->getNodeState();
    $user_data = $node_state->getUserData();
    foreach (['severity', 'workflow', 'renew_id'] as $target) {
      if (!empty($value[$target])) {
        $new_target = $value[$target];
        if (!empty($user_data[$uid])) {
          $old_target = $user_data[$uid][$target] ?: FALSE;
          if (!empty($new_target) && $new_target !== $old_target) {
            $this->_removeVoteSummarize($target, $old_target);
            $this->_addVoteSummarize($target, $new_target);
          }
        }
        else {
          $this->_addVoteSummarize($target, $new_target);
        }
      }
    }

    $user_data[$uid] = $value;
    $node_state->setUserData(-1, $user_data);
  }

  private function _addVoteSummarize($target, $target_id) {
    $node_state = $this->getNodeState();
    $node_state_data = $node_state->getData('state_data');
    $target_key = "vote_$target";
    $data = ($node_state_data[$target_key] ?? []);
    if (empty($data[$target_id])) {
      $data[$target_id] = 0;
    }

    $data[$target_id] += 1;
    $node_state_data[$target_key] = $data;
    $node_state->setData($node_state_data, 'state_data');
    $this->setWinnerTargetData($data, $target);
  }

  private function _removeVoteSummarize($target, $target_id) {
    $node_state = $this->getNodeState();
    $node_state_data = $node_state->getData('state_data');
    $target_key = "vote_$target";
    $data = ($node_state_data[$target_key] ?? []);
    if (!empty($data[$target_id]) && $data[$target_id] > 0) {
      $data[$target_id] -= 1;
      if ($data[$target_id] === 0) {
        unset($data[$target_id]);
      }
      $node_state_data[$target_key] = $data;
      $node_state->setData($node_state_data, 'state_data');
      $this->setWinnerTargetData($data, $target);
    }
    else {
      $args = [
        '%node' => $node_state->id(),
        '%target' => $target_id,
      ];
      $msg = t("Vote $target < 0: node: %node, $target: %target.", $args);
      SlogXtwf::logger()->error($msg);
    }
  }

  private function _getWinner($data, $target) {
    $winner = FALSE;

    $competitions = $this->getNodeState()->getCompetitionData('competitions') ?? [];
    foreach ($competitions as $competition) {
      if ($competition['target'] === $target) {
        $best_id = $competition['preferred']['best']['id'];
        break;
      }
    }

    $last_nummber = -1;
    foreach ($data as $id => $number) {
      if ($id === $best_id) {
        $number += 0.5;
      }
      if ($number > $last_nummber) {
        $winner = $id;
      }
      $last_nummber = $number;
    }

    return $winner;
  }

  protected function getWinnerData() {
    $state_data = $this->getNodeState()->getData('state_data');
    return ($state_data['winner_data'] ?? []);
  }

  protected function setWinnerData($winner_data) {
    $node_state = $this->getNodeState();
    $state_data = $node_state->getData('state_data');
    $state_data['winner_data'] = $winner_data;
    $node_state->setData($state_data, 'state_data');
  }

  protected function setWinnerTargetData($data, $target) {
    $winner_data = $this->getWinnerData();
    $winner_data[$target] = $this->_getWinner($data, $target);
    $this->setWinnerData($winner_data);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoBase($as_array = FALSE) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubtitleInfo() {
    $info = [];
    $winner_data = $this->getWinnerData();
    $node_state = $this->getNodeState();
    // renew_id
    if (!empty($winner_data['renew_id'])) {
      $winner = $winner_data['renew_id'];
      $rid_labels = $node_state->getOptionsRequestRenew();
      $info[] = t('Next: ') . $rid_labels[$winner];
    }
    // workflow
    if (!empty($winner_data['workflow'])) {
      $winner = $winner_data['workflow'];
      $wf_labels = $node_state->getWorkflowLabels();
      $info[] = t('Workflow: ') . $wf_labels[$winner];
    }
    // severity
    if (!empty($winner_data['severity'])) {
      $winner = $winner_data['severity'];
      $severity_labels = SlogXtwf::getStateSeverityOptions();
      $info[] = t('Severity: ') . $severity_labels[$winner];
    }

    if (empty($info)) {
      $info[] = (string) t('There are no votes yet.');
    }

    $subtitle_info = implode('; ', $info);
    if ($base_info = parent::getSubtitleInfo()) {
      $subtitle_info .= "<br />$base_info";
    }
    return $subtitle_info;
  }

}
