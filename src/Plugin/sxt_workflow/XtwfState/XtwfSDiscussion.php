<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSDiscussion.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_discussion",
 *   label = @Translation("Discussion"),
 *   settings = {
 *     "state_type" = "discussion",
 *     "use_dynamic_severity" = TRUE,
 *   },
 *   weight = 2001
 * )
 */
class XtwfSDiscussion extends XtwfPluginStateBase {

  /**
   * {@inheritdoc}
   */
  public function getStateInfo($prepend_base = TRUE) {
    $node_state = $this->getNodeState();

    $date_formater = $this->getXtDateFormater();
    $started = $node_state->getData('started');
    $sv_timeout = $node_state->getSeverityTimeout();
    $timeout_timestamp = $this->xtDateAddDays($started, $sv_timeout);
    $days_left = $this->xtDateDiffDays($started, $timeout_timestamp);
    $timeout_dt = $date_formater->format($timeout_timestamp, 'xt_date_only');
    $f_started = $date_formater->format($started, 'short');
    $args = [
      '@started' => $f_started,
      '@timeout_dt' => $timeout_dt,
      '@left' => max($days_left, 0),
      '@timeout' => $sv_timeout,
    ];

    $state_info = [
      'discuss_started' => (string) t('Started on @started', $args),
      'discuss_timeout' => t('Timeout: @timeout_dt', $args),
    ];
    $state_info['discuss_timeout'] .= ' (' . t('Left: @left of @timeout days', $args) . ')';

    if ($more_info = $this->getInfoMore()) {
      $discuss_for = t('Suspended state to discuss');
      $state_info['discuss_for'] = SlogXt::htmlHighlightText(">>> $discuss_for <<<", FALSE);
      $state_info = array_merge($state_info, $more_info);
    }

    return $state_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    $node_state = $this->getNodeState();
    $rollback_values = $node_state->getValues();
    $history_data = $node_state->getHistoryData($node_state->getStarted());
    try {
      $node_state->setValues($history_data);
      $state_info = $node_state->getStateInfo(TRUE, TRUE, FALSE, TRUE);
    }
    catch (Exception $e) {
      $error_msg = $e->getMessage();
      SlogXtwf::logger()->error($error_msg);
    }
    
    // restore
    $node_state->setValues($rollback_values);
    
    return $state_info;
  }

}
