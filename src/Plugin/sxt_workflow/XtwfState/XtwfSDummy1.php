<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSDummy1.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_dummy1",
 *   label = @Translation("Dummy001"),
 *   settings = {
 *     "state_type" = "dummy",
 *     "use_severity" = TRUE,
 *     "dev_only" = TRUE,
 *   },
 *   weight = 9997
 * )
 */
class XtwfSDummy1 extends XtwfPluginStateBase {

}
