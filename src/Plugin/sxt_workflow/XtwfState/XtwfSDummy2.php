<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSDummy2.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_dummy2",
 *   label = @Translation("Dummy002"),
 *   settings = {
 *     "state_type" = "dummy",
 *     "use_severity" = TRUE,
 *     "dev_only" = TRUE,
 *   },
 *   weight = 9998
 * )
 */
class XtwfSDummy2 extends XtwfPluginStateBase {

}
