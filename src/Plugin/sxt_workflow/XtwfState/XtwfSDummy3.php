<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSDummy3.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_dummy3",
 *   label = @Translation("Dummy003"),
 *   settings = {
 *     "state_type" = "dummy",
 *     "use_severity" = TRUE,
 *     "dev_only" = TRUE,
 *   },
 *   weight = 9999
 * )
 */
class XtwfSDummy3 extends XtwfPluginStateBase {

}
