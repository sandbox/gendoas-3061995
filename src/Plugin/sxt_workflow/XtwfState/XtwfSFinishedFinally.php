<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSFinishedFinally.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_finish_finally",
 *   label = @Translation("Finished finally"),
 *   settings = {
 *     "state_type" = "finished",
 *     "use_severity" = FALSE,
 *   },
 *   weight = 1995
 * )
 */
class XtwfSFinishedFinally extends XtwfPluginStateBase {

}
