<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSFinishedRenewable.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_finish_renewable",
 *   label = @Translation("Finished renewable"),
 *   settings = {
 *     "state_type" = "finished",
 *     "use_severity" = FALSE,
 *   },
 *   weight = 1990
 * )
 */
class XtwfSFinishedRenewable extends XtwfPluginStateBase {

  /**
   * {@inheritdoc}
   */
  public function postFinalize() {
    $node_state = $this->getNodeState();
    if ($node_state->isStateFinishedFinally()) {
      $node_state->unsubscribeAll();
    }
  }

}
