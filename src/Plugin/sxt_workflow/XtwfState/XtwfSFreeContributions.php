<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSFreeContributions.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_free_contributions",
 *   label = @Translation("Free contributions"),
 *   settings = {
 *     "state_type" = "collect_content",
 *   },
 *   weight = 1000
 * )
 */
class XtwfSFreeContributions extends XtwfPluginSCollectContentBase {

}
