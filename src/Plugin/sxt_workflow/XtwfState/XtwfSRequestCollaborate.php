<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSRequestCollaborate.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_request_collaborate",
 *   label = @Translation("Collaboration request"),
 *   settings = {
 *     "state_type" = "request",
 *     "use_severity" = TRUE,
 *     "number_description" = @Translation(".....description...xtwf_request_collaborate"),
 *     "number_suffix" = @Translation("requests"),
 *     "severity" = {
 *       "leight" = {
 *         "number" = 10,
 *         "timeout" = 10,
 *       },
 *       "medium" = {
 *         "number" = 20,
 *         "timeout" = 20,
 *       },
 *       "severe" = {
 *         "number" = 50,
 *         "timeout" = 30,
 *       },
 *       "severest" = {
 *         "number" = 100,
 *         "timeout" = 30,
 *       },
 *     }
 *   },
 *   weight = 0
 * )
 */
class XtwfSRequestCollaborate extends XtwfPluginSRequestBase {

  protected function preFinalizeAdmin($args) {
    $next_state = FALSE;
    $node_state = $this->getNodeState();
    $node_state->setData(TRUE, 'onAdminFinalize');

    $action = $args['admin_data']['admin_action'] ?? '';
    if (!in_array($action, ['finalize', 'terminate'])) {
      $message = t('Admin action not handled:  @action', ['@action' => $action]);
      throw new \InvalidArgumentException($message);
    }

    if ($action === 'finalize') {
      if ($args['workflow'] === SlogXtwf::XTWF_DISCUSSION) {
        $next_state = $node_state->getWfState(SlogXtwf::XTWF_DISCUSSION);
      }
      else {
        $node_state->setWorkflowId($args['workflow'])
            ->setSeverity($args['severity']);
      }
    }
    else {  // terminate
      $next_state_id = ($args['next_state_id'] ?? '');
      if (!in_array($next_state_id, [SlogXtwf::XTWF_FINISH_RENEWABLE, SlogXtwf::XTWF_FINISH_FINALLY])) {
        $next_state_id = SlogXtwf::XTWF_FINISH_RENEWABLE;
      }
      $next_state = $node_state->getWfState($next_state_id);
    }

    return $next_state;
  }

  protected function getNeededCompetitions() {
    $node_state = $this->getNodeState();

    // no competition for admin action (finalize or terminate)
    $on_admin_finalize = $node_state->getData('onAdminFinalize') ?: FALSE;
    if ($on_admin_finalize) {
      return FALSE;
    }

    $needed_competitions = [];
    $node_state_id = $node_state->getStateId();
    foreach (['workflow', 'severity'] as $target) {
      $preferred = $this->getPreferred($target);
      if (empty($preferred)) {
        continue;
      }

      $best_id = $preferred['best']['id'];
      $pref_count = count($preferred);
      if ($pref_count === 1) {
        if ($node_state->{$target}->value !== $best_id) {
          $node_state->{$target}->setValue($best_id);
        }
      }
      else {
        $next_id = $preferred['next']['id'];
        if ($target === 'workflow') {
          $t_label = t('workflow');
          if ($best_id === SlogXtwf::XTWF_DISCUSSION) {
            $l_best = SlogXtwf::getTxtLetsDiscuss();
          }
          else {
            $l_best = SlogXtwf::getWorkflow($best_id)->label();
          }
          if ($next_id === SlogXtwf::XTWF_DISCUSSION) {
            $l_next = SlogXtwf::getTxtLetsDiscuss();
          }
          else {
            $l_next = SlogXtwf::getWorkflow($next_id)->label();
          }
          $n_best = (integer) $preferred['best']['number'];
          $n_next = (integer) $preferred['next']['number'];
        }
        else {  // severity
          $t_label = t('severity');
          $severities = SlogXtwf::getStateSeverityOptions();
          $l_best = $severities[$best_id];
          $l_next = $severities[$next_id];
          $n_best = $this->severitySplitter($preferred['best']['number']);
          $n_next = $this->severitySplitter($preferred['next']['number']);
        }

        $args = [
          '@t_label' => $t_label,
          '@l_best' => $l_best,
          '@n_best' => $n_best ?: '-',
          '@l_next' => $l_next,
          '@n_next' => $n_next ?: '-',
        ];
        $needed_competitions[] = [
          'target' => $target,
          'preferred' => $preferred,
          'description' => (string) t('Decision is required for @t_label: @l_best(@n_best), @l_next(@n_next)', $args),
        ];
      }
    }

    if ($needed_competitions && !$node_state->hasNextCompetition($node_state_id)) {
      if (SlogXtwf::getDoLogWarnings()) {
        $args = [
          '@from' => $node_state_id,
          '@to' => SlogXtwf::XTWF_COMPETITION,
        ];
        $msg = t('There is no transition to competition: from=@from, to=@to', $args);
        SlogXtwf::logger()->warning($msg);
      }
      $node_state->workflow->setValue($this->getPreferredBest('workflow'));
      $node_state->severity->setValue($this->getPreferredBest('severity'));
      $needed_competitions = FALSE;
    }

    return ($needed_competitions ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    $node_state = $this->getNodeState();
    if ($node_state->hasUserData()) {
      // more data
      $state_data = $node_state->getData('state_data');
      $wf_labels = $node_state->getWorkflowLabels();

      //workflow
      $wf_info = [];
      if (!empty($wf_labels)) {
        foreach ($state_data['request_workflow'] as $key => $value) {
          $tmp = $wf_labels[$key] . ' (' . $value . ')';
          if (empty($wf_info)) {
            $tmp = SlogXt::htmlHighlightText($tmp);
          }
          $wf_info[] = $tmp;
        }
      }

      //severity
      $sv_info = [];
      $severity_labels = SlogXtwf::getStateSeverityOptions();
      foreach ($state_data['request_severity'] as $key => $value) {
        $tmp = $severity_labels[$key] . ' (' . $value . ')';
        if (empty($sv_info)) {
          $tmp = SlogXt::htmlHighlightText($tmp);
        }
        $sv_info[] = $tmp;
      }

      //finalize
      $fn_info = [];
      $fn_labels = SlogXtwf::getStateFinalizeOptions();
      $by_request = !$this->toFinalizeByTimeout();
      if (!empty($fn_labels)) {
        foreach ($state_data['request_finalize'] as $key => $value) {
          $is_requests_key = ($key === 'requests');
          $tmp = $fn_labels[$key] . ' (' . $value . ')';
          if (($by_request && $is_requests_key) || (!$by_request && !$is_requests_key)) {
            $tmp = SlogXt::htmlHighlightText($tmp);
          }
          $fn_info[] = $tmp;
        }
      }

      //strict
      $strict_info = [];
      $st_labels = SlogXtwf::getStrictOptions();
      $strict = $this->toFinalizeStrict();
      foreach ($state_data['request_strict'] as $key => $value) {
        $is_strict_key = ($key === 'yes');
        $tmp = $st_labels[$key] . ' (' . $value . ')';
        if (($strict && $is_strict_key) || (!$strict && !$is_strict_key)) {
          $tmp = SlogXt::htmlHighlightText($tmp);
        }
        $strict_info[] = $tmp;
      }

      $prefix = SlogXtwf::XTWF_PREFIX_DASH;
      $more_info = [
        'more_title' => t('Request data:'),
        'workflow' => $prefix . t('Workflow') . ': ' . implode('; ', $wf_info),
        'severity' => $prefix . t('Severity') . ': ' . implode('; ', $sv_info),
        'finalize' => $prefix . t('Finalize by') . ': ' . implode('; ', $fn_info),
        'strict' => $prefix . t('Strict') . ': ' . implode('; ', $strict_info),
      ];
      
      //discuss
      if (!empty($state_data['request_discuss'])) {
        $discuss_info = [];
        $ds_labels = SlogXtwf::getDiscussOptions();
        $discuss = $this->toDiscuss();
        foreach ($state_data['request_discuss'] as $key => $value) {
          $is_discuss_key = ($key === 'yes');
          $tmp = $ds_labels[$key] . ' (' . $value . ')';
          if (($discuss && $is_discuss_key) || (!$discuss && !$is_discuss_key)) {
            $tmp = SlogXt::htmlHighlightText($tmp);
          }
          $discuss_info[] = $tmp;
        }
        $more_info['discuss'] = $prefix . SlogXtwf::getTxtLetsDiscuss() . ': ' . implode('; ', $discuss_info);
      }
      

      return $more_info;
    }

    return FALSE;
  }

}
