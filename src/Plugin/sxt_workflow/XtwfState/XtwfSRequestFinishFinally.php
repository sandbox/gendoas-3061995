<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSRequestFinishFinally.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_request_finish_finally",
 *   label = @Translation("Request for finish finally"),
 *   settings = {
 *     "state_type" = "request",
 *     "use_severity" = TRUE,
 *     "number_description" = @Translation(".....description...xtwf_request_renew"),
 *     "number_suffix" = @Translation("requests"),
 *   },
 *   weight = 1991
 * )
 */
class XtwfSRequestFinishFinally extends XtwfPluginSRequestBase {

  /**
   * {@inheritdoc}
   */
  public function toFinalizeByTimeout() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function preFinalize($finalized_by, array $args) {
    // abort request if not enough votes or not enough yes votes,
    // or (for admin) if 'no' selected
    $next_state_id = SlogXtwf::XTWF_FINISH_RENEWABLE;
    $node_state = $this->getNodeState();
    
    if ($finalized_by === 'admin') {
      $vote = $args['vote'] ?? SlogXtwf::XTWF_SELECT_NO;
      if ($vote === SlogXtwf::XTWF_SELECT_YES) {
        $next_state_id = SlogXtwf::XTWF_FINISH_FINALLY;
      }
      elseif ($vote === SlogXtwf::XTWF_DISCUSSION) {
        $next_state_id = SlogXtwf::XTWF_DISCUSSION;
      }
    }
    else {
      $v_required = $node_state->getSeverityRequired();
      $votes = $this->getPostsRelevant();

      if ($votes > 0 && $votes >= $v_required) {
        $p_required = SlogXtwf::getPercentSevere();
        $state_data = $node_state->getData('state_data');
        $yes_vote = $state_data['request_vote'][SlogXtwf::XTWF_SELECT_YES] ?? 0;
        $has_percent = $yes_vote * 100 / $votes;
        if ($has_percent >= $p_required) {
          // there are enough votes and enough yes votes
          $next_state_id = SlogXtwf::XTWF_FINISH_FINALLY;
        }
      }
    }

    $next_state = $node_state->getWfState($next_state_id);
    return $next_state;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    $node_state = $this->getNodeState();
    $prefix = SlogXtwf::XTWF_PREFIX_DASH;
    $more_info['more_title'] = t('Vote data:');
    $txt_info = t('none');

    if ($node_state->hasUserData()) {
      $target = 'vote';
      $votes = $this->getPostsRelevant();
      $v_required = $node_state->getSeverityRequired();
      $show_percent = ($votes * 2) >= $v_required;
      $state_data = $node_state->getData('state_data');
      $vote_data = $state_data["request_$target"] ?? [];

      foreach ($vote_data as $key => $value) {
        $txt_value = (string) $value;
        if ($key === SlogXtwf::XTWF_SELECT_YES && $show_percent) {
          $has_percent = (round($value * 100 / $votes, 1));
          $p_required = SlogXtwf::getPercentSevere();
          $txt_has_percent = $has_percent . '%';
          $txt_value .= ' (' . $txt_has_percent . ' - ' . t('required: ') . $p_required . '%)';
          if ($votes >= $v_required && $has_percent >= $p_required) {
            $txt_value = SlogXt::htmlHighlightText($txt_value);
          }
        }
        $more_info[] = $prefix . SlogXtwf::getVoteLabel($key) . ': ' . $txt_value;
      }
    }
    else {
      $more_info['more_title'] .= ' ' . SlogXt::htmlHighlightText($txt_info);
    }

    return $more_info;
  }

  protected function getTargetKeys() {
    return ['vote'];
  }

}
