<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSRequestRenew.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;

/**
 * ...
 *
 * @XtwfState(
 *   id = "xtwf_request_renew",
 *   label = @Translation("Request for renew"),
 *   settings = {
 *     "state_type" = "request",
 *     "use_severity" = TRUE,
 *     "number_description" = @Translation(".....description...xtwf_request_renew"),
 *     "number_suffix" = @Translation("requests"),
 *   },
 *   weight = 1991
 * )
 */
class XtwfSRequestRenew extends XtwfPluginSRequestBase {

  protected function getTargetKeys() {
    $keys =  ['renew_id', 'severity', 'finalize', 'strict'];
    $node_state = $this->getNodeState();
    if ($node_state->hasDiscussionState()) {
      $keys[] = 'discuss';
    }
    return $keys;
  }

  protected function getDefaultOption($target) {
    if ($target === 'renew_id') {
      $opts = $this->getNodeState()->getOptionsRequestRenew();
      return (!empty($opts[SlogXtwf::XTWF_DISCUSSION]) ? SlogXtwf::XTWF_DISCUSSION : SlogXtwf::XTWF_SELECT_NO);
    }

    return parent::getDefaultOption($target);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoMore() {
    $node_state = $this->getNodeState();
    $more_info['more_title'] = t('Vote data:');
    $txt_info = t('none');

    if ($node_state->hasUserData()) {
      $state_data = $node_state->getData('state_data');

      //renew_id
      $rid_info = [];
      $rid_labels = $node_state->getOptionsRequestRenew();
      foreach ($state_data['request_renew_id'] as $key => $value) {
        $tmp = $rid_labels[$key] . ' (' . $value . ')';
        if (empty($rid_info)) {
          $tmp = SlogXt::htmlHighlightText($tmp);
        }
        $rid_info[] = $tmp;
      }

      //finalize
      $fn_info = [];
      $fn_labels = SlogXtwf::getStateFinalizeOptions();
      $by_request = !$this->toFinalizeByTimeout();
      if (!empty($fn_labels)) {
        foreach ($state_data['request_finalize'] as $key => $value) {
          $is_requests_key = ($key === 'requests');
          $tmp_info = $fn_labels[$key] . ' (' . $value . ')';
          if (($by_request && $is_requests_key) || (!$by_request && !$is_requests_key)) {
            $tmp_info = SlogXt::htmlHighlightText($tmp_info);
          }
          $fn_info[] = $tmp_info;
        }
      }

      //severity
      $sv_info = [];
      $severity_labels = SlogXtwf::getStateSeverityOptions();
      foreach ($state_data['request_severity'] as $key => $value) {
        $tmp = $severity_labels[$key] . ' (' . $value . ')';
        if (empty($sv_info)) {
          $tmp = SlogXt::htmlHighlightText($tmp);
        }
        $sv_info[] = $tmp;
      }

      //strict
      $strict_info = [];
      $st_labels = SlogXtwf::getStrictOptions();
      $strict = $this->toFinalizeStrict();
      foreach ($state_data['request_strict'] as $key => $value) {
        $is_strict_key = ($key === 'yes');
        $tmp = $st_labels[$key] . ' (' . $value . ')';
        if (($strict && $is_strict_key) || (!$strict && !$is_strict_key)) {
          $tmp = SlogXt::htmlHighlightText($tmp);
        }
        $strict_info[] = $tmp;
      }

      $prefix = SlogXtwf::XTWF_PREFIX_DASH;
      $more_info += [
        'renew_id' => $prefix . t('Next') . ': ' . implode('; ', $rid_info),
        'severity' => $prefix . t('Severity') . ': ' . implode('; ', $sv_info),
        'finalize' => $prefix . t('Finalize by') . ': ' . implode('; ', $fn_info),
        'strict' => $prefix . t('Strict') . ': ' . implode('; ', $strict_info),
      ];
      //discuss
      if (!empty($state_data['request_discuss'])) {
        $discuss_info = [];
        $ds_labels = SlogXtwf::getDiscussOptions();
        $discuss = $this->toDiscuss();
        foreach ($state_data['request_discuss'] as $key => $value) {
          $is_discuss_key = ($key === 'yes');
          $tmp = $ds_labels[$key] . ' (' . $value . ')';
          if (($discuss && $is_discuss_key) || (!$discuss && !$is_discuss_key)) {
            $tmp = SlogXt::htmlHighlightText($tmp);
          }
          $discuss_info[] = $tmp;
        }
        $more_info['discuss'] = $prefix . SlogXtwf::getTxtLetsDiscuss() . ': ' . implode('; ', $discuss_info);
      }
    }
    else {
      $more_info['more_title'] .= ' ' . SlogXt::htmlHighlightText($txt_info);
    }

    return $more_info;
  }

  public function getPreferred($target) {
    if ($target === 'renew_id') {
      if (empty($this->preferred[$target])) {
        $this->preferred[$target] = $this->getPreferredRenewId();
      }
      return $this->preferred[$target];
    }

    return parent::getPreferred($target);
  }

  protected function getPreferredRenewId() {
    $preferred = [];
    $node_state = $this->getNodeState();
    $rid_all = $node_state->getOptionsRequestRenew();

    $best_id = '';
    $best_number = 0;
    $target = 'renew_id';
    $state_data = $node_state->getData('state_data');
    $target_data = $state_data["request_$target"] ?? [];

    $requests = 0;
    foreach ($target_data as $key => $number) {
      $requests += $number;
      $number_test = $number;
      if ($key === SlogXtwf::XTWF_SELECT_NO) {
        $number_test += 0.5;
      }
      if (empty($best_id) || $number_test > $best_number) {
        $best_id = $key;
        $best_number = $number_test;
      }
    }

    $preferred['best'] = [
      'id' => $best_id,
      'number' => $best_number,
    ];


    $decisive1 = (integer) ($requests * 3 / 4);
    if ($best_number < $decisive1) {
      // search for next
      $next_id = '';
      $next_number = 0;
      foreach ($target_data as $key => $number) {
        if ($key !== $best_id) {
          $number_test = $number;
          if ($key === SlogXtwf::XTWF_SELECT_NO) {
            $number_test += 0.5;
          }
          if (empty($next_id) || $number_test > $next_number) {
            $next_id = $key;
            $next_number = $number_test;
          }
        }
      }

      if ($next_number > $best_number) {
        // flip values
        $tmp_id = $best_id;
        $tmp_number = $best_number;
        $best_id = $next_id;
        $best_number = $next_number;
        $next_id = $tmp_id;
        $next_number = $tmp_number;
      }

      // has equals ?
      $best_data = [];
      foreach ($target_data as $key => $number) {
        if ($number >= $target_data[$next_id]) {
          $best_data[$key] = $number;
        }
      }

      $decisive2 = (integer) ($requests * 3 / 8);
      $decisive3 = (integer) ($requests * 1 / 2);
      if (($best_number > $next_number || count($best_data) < 3) && $next_number >= $decisive2) {
        $preferred['next'] = [
          'id' => $next_id,
          'number' => $next_number,
        ];
      }
      elseif ($best_number < $decisive3) {
        $preferred = [];
        if ($best_data[$best_id] > $best_data[$next_id]) {
          $set_best_id = $best_id;
          $preferred['best'] = [
            'id' => $best_id,
            'number' => $best_data[$best_id],
          ];
        }

        foreach ($rid_all as $rid => $rlabel) {
          if (empty($preferred['best'])) {
            $set_best_id = $rid;
            $preferred['best'] = [
              'id' => $rid,
              'number' => $best_data[$rid],
            ];
          }
          elseif ($rid !== $set_best_id) {
            $preferred['next'] = [
              'id' => $rid,
              'number' => $best_data[$rid],
            ];
            break;
          }
        }
      }
    }

    return $preferred;
  }

  /**
   * {@inheritdoc}
   */
  public function preFinalize($finalized_by, array $args) {
    // abort request if not enough votes or vote is for not renew
    $next_state_id = SlogXtwf::XTWF_FINISH_RENEWABLE;

    $node_state = $this->getNodeState();
    $v_required = $node_state->getSeverityRequired();
    $votes = $this->getPostsRelevant();

    if ($votes > 0 && $votes >= $v_required) {
      $rid_all = $node_state->getOptionsRequestRenew();
      $preferred_renew_id = $this->getPreferred('renew_id');
      $best_id = $preferred_renew_id['best']['id'];
      if (!key_exists($best_id, $rid_all)) {
        $best_id = SlogXtwf::XTWF_SELECT_NO;
      }

      $needs_competition = count($preferred_renew_id) > 1;
      if ($needs_competition) {
        $competitions = $this->getNeededCompetitions();
        $next_state_id = SlogXtwf::XTWF_COMPETITION;

        $next_state_plugin = SlogXtwf::getStatePlugin($next_state_id);
        $next_state_plugin->setNodeState($node_state);
        $next_state_plugin->prepareCompetitionState($competitions);
      }
      else {
        if ($best_id === SlogXtwf::XTWF_DISCUSSION) {
          $next_state_id = SlogXtwf::XTWF_DISCUSSION;
        }
        elseif ($best_id !== SlogXtwf::XTWF_SELECT_NO) {
          $next_state_id = $best_id;
        }
      }
    }

    $next_state = $node_state->getWfState($next_state_id);
    return $next_state;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNeededCompetitions() {
    $node_state = $this->getNodeState();

    // no competition for admin action (finalize or terminate)
    $on_admin_finalize = $node_state->getData('onAdminFinalize') ?: FALSE;
    if ($on_admin_finalize) {
      return FALSE;
    }

    $rid_all = $node_state->getOptionsRequestRenew();
    $needed_competitions = [];
    $node_state_id = $node_state->getStateId();
    foreach (['renew_id', 'severity'] as $target) {
      $preferred = $this->getPreferred($target);
      if (empty($preferred)) {
        continue;
      }

      $best_id = $preferred['best']['id'];
      $pref_count = count($preferred);
      if ($pref_count === 1) {
        if ($target === 'severity' && $node_state->{$target}->value !== $best_id) {
          $node_state->{$target}->setValue($best_id);
        }
      }
      else {
        $next_id = $preferred['next']['id'];
        if ($target === 'renew_id') {
          $t_label = t('next');
          $l_best = $rid_all[$best_id];
          $l_next = $rid_all[$next_id];
          $n_best = (integer) $preferred['best']['number'];
          $n_next = (integer) $preferred['next']['number'];
        }
        else {  // severity
          $t_label = t('severity');
          $severities = SlogXtwf::getStateSeverityOptions();
          $l_best = $severities[$best_id];
          $l_next = $severities[$next_id];
          $n_best = $this->severitySplitter($preferred['best']['number']);
          $n_next = $this->severitySplitter($preferred['next']['number']);
        }

        $args = [
          '@t_label' => $t_label,
          '@l_best' => $l_best,
          '@n_best' => $n_best ?: '-',
          '@l_next' => $l_next,
          '@n_next' => $n_next ?: '-',
        ];
        $needed_competitions[] = [
          'target' => $target,
          'preferred' => $preferred,
          'description' => (string) t('Decision is required for @t_label: @l_best(@n_best), @l_next(@n_next)', $args),
        ];
      }
    }


    if ($needed_competitions && !$node_state->hasNextCompetition($node_state_id)) {
      if (SlogXtwf::getDoLogWarnings()) {
        $args = [
          '@from' => $node_state_id,
          '@to' => SlogXtwf::XTWF_COMPETITION,
        ];
        $msg = t('There is no transition to competition: from=@from, to=@to', $args);
        SlogXtwf::logger()->warning($msg);
      }
      $node_state->severity->setValue($this->getPreferredBest('severity'));
      $needed_competitions = FALSE;
    }

    return ($needed_competitions ?? FALSE);
  }

}
