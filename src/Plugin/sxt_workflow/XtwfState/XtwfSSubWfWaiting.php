<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState\XtwfSSubWfWaiting.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfState;
//todo::current:: ----sxt_sxt_workflow --- xtwf_subwf_waiting

/**
 * ...
 *
 * @XtwfStateXXX(
 *   id = "xtwf_subwf_waiting",
 *   label = @Translation("Waiting for sub workflow"),
 *   settings = {
 *     "state_type" = "subwf_waiting",
 *   },
 *   weight = 1980
 * )
 */
class XtwfSSubWfWaiting extends XtwfPluginStateBase {

}
