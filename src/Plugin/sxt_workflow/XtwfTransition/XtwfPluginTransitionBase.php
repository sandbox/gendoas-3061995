<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfPluginTransitionBase.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfPluginBase;

/**
 */
class XtwfPluginTransitionBase extends XtwfPluginBase implements XtwfPluginTransitionInterface {

  public function getWeight() {
    return (integer) (isset($this->pluginDefinition['weight']) ? $this->pluginDefinition['weight'] : 0);
  }

  public function doesMeetRequirements() {
    $required_from = $this->getSetting('required_from') ?? [];
    $exclude_from = $this->getSetting('exclude_from') ?? [];
    $intersect = array_intersect($required_from, $exclude_from);

    $does_meet = empty($intersect);
    if (!$does_meet) {
      $args = [
        '@transition' => $this->getLabel(),
        '@states' => implode(', ', $intersect),
      ];
      $msg = t('Required/Exclude intersection found in transition @transition: @states', $args);
      SlogXtwf::logger()->error($msg);
    }

    return $does_meet;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredFromFixed(array $xtwf_states) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  public function formEditAlter(&$form, FormStateInterface $form_state) {
    $class = get_class($this);
    $form['#validate'][] = "$class::formEditValidate";

    $transition_id = $this->getPluginId();
    $workflow = $form_state->getFormObject()->getEntity();
    $workflow_label = $workflow->label();
    $xtwf_type_plugin = $workflow->getTypePlugin();
    $xtwf_states = $xtwf_type_plugin->getStates();

    // prevent deletion of required transition
    if (!empty($xtwf_states[$transition_id]) && isset($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }

    $xtwf_transition = $form_state->get('transition');
    
    // transition id is equal to target state
    $form['to']['#options'] = [$transition_id => $form['to']['#options'][$transition_id]];
    $form['to']['#description'] = t('The target state is fixed, the state id is forced to be identical to the id of the transition');

    $args = ['@requirements' => $xtwf_transition->getInfoFromRequirements()];
    $form['from']['#description'] = t('REQUIREMENTS: @requirements', $args);

    $does_meet_requirements = $xtwf_transition->doesMeetRequirements($workflow_label);
    if (!$does_meet_requirements) {
      $msg = t('There is a requirement missmatch.') . ' ' . SlogXt::txtSeeLogMessages();
      $form['err_notice'] = [
        '#markup' => SlogXt::htmlMessage($msg, 'error'),
        '#weight' => -999,
      ];
    }
    else {
      $value_from = $form['from']['#default_value'];
      $opts_from = $form['from']['#options'];
      $opts_new = [];
      
      $transition_plugin = $form_state->get('transitionPlugin');
      $t_settings = $transition_plugin->getSettings();
      $exclusive_from = (boolean) $t_settings['exclusive_from'];
      $required_from = $t_settings['required_from'] ?? [];

      if ($transition_id === SlogXtwf::XTWF_FINISH_RENEWABLE) {
        $exclude = [$transition_id, SlogXtwf::XTWF_FINISH_FINALLY];
        $required_from = [];
        foreach ($xtwf_states as $state_id => $state) {
          if ($state_id !== $transition_id && !$state->isFinishedType()) {
            $opts_new[$state_id] = $opts_from[$state_id];
          }
        }

        $form['from']['#options'] = $opts_new;
        $form['from']['#description'] .= '<br />' .  t('The set of source states is fixed. ');
        $form['from']['#disabled'] = TRUE;
      }
      elseif ($exclusive_from && $required_from) {
        foreach ($required_from as $state_id) {
          $opts_new[$state_id] = $opts_from[$state_id];
        }
        $has_err = (count($value_from) !== count($required_from));
        if (!$has_err) {
          foreach ($required_from as $state_id) {
            if (!in_array($state_id, $value_from)) {
              $has_err = TRUE;
              break;
            }
          }
        }

        $form['from']['#options'] = $opts_new;
        $form['from']['#description'] .= '<br />' .  t('The set of source states is fixed. ');
        $form['from']['#disabled'] = !$has_err;
      }
      else {
//todo::current:: ----sxt_sxt_workflow --- transition/from/to
        $to_state = $xtwf_states[$transition_id];
        $to_s_is_request = $to_state->isRequestType();

        $exclude = [$transition_id, SlogXtwf::XTWF_FINISH_FINALLY];
        if (!empty($t_settings['exclude_from']) && is_array($t_settings['exclude_from'])) {
          $exclude = array_merge($exclude, $t_settings['exclude_from']);
        }
//todo::current:: ----sxt_sxt_workflow --- transition/from/to
        foreach ($xtwf_states as $state_id => $state) {
          if ($to_s_is_request && $state->isRequestType()) {
            $exclude[] = $state_id;
          }
//          elseif ($state->getUseDynamicSeverity()) {
//            $exclude[] = $state_id;
//          }
        }

        $opts_from = $form['from']['#options'];
        $opts_new = [];
        foreach (array_keys($opts_from) as $state_id) {
          if (!in_array($state_id, $exclude)) {
            $opts_new[$state_id] = $opts_from[$state_id];
          }
        }
        $form['from']['#options'] = $opts_new;
        // prepare for disabling required states
        $form['from']['#xtwf_transition_required'] = $required_from;
      }
    }
  }

  /**
   * Disable required states in from-field.
   * 
   * @param array $element
   * @return array
   */
  public static function fromFieldDisableRequired(array $element) {
    if (!empty($element['#xtwf_transition_required'])) {
      foreach ($element['#xtwf_transition_required'] as $state_id) {
        if (!empty($element[$state_id]) && !empty($element['#value'][$state_id])) {
          $element[$state_id]['#attributes']['disabled'] = TRUE;
        }
      }
    }
    return $element;
  }

  public static function formEditValidate(&$form, FormStateInterface $form_state) {
    $xtwf_transition = $form_state->get('transition');
    
    $transition_plugin = $form_state->get('transitionPlugin');
    $t_settings = $transition_plugin->getSettings();
    $exclusive_from = (boolean) $t_settings['exclusive_from'];
    $required_from = $t_settings['required_from'] ?? [];
    $exclude_from = $t_settings['exclude_from'] ?? [];

    $values = $form_state->getValues();
    $fromIds = array_keys(array_filter($values['from']));

    $valid = TRUE;

    // Exclusive from
    if ($exclusive_from) {
      $valid = (count($fromIds) === count($required_from));
      if ($valid) {
        $valid = empty(array_diff($fromIds, $required_from));
      }
    }

    // Required from
    if ($valid && !empty($required_from)) {
      $valid = (count(array_intersect($fromIds, $required_from)) === count($required_from));
    }

    // Excluded from
    if ($valid && !empty($exclude_from)) {
      $valid = empty(array_intersect($fromIds, $exclude_from));
    }

    if (!$valid) {
      $args = ['@requirements' => $xtwf_transition->getInfoFromRequirements()];
      $form_state->setErrorByName('from', t('From: requirements does not meet. REQUIREMENTS: @requirements', $args));
    }

    $workflow = $xtwf_transition->getWorkflow();
    $state_id = $xtwf_transition->id();
    $unselectable = array_intersect([SlogXtwf::XTWF_FINISH_FINALLY, $state_id], $fromIds);
    if (!empty($unselectable)) {
      $form_state->setErrorByName('from', t('Do not select this states in "From": @states.', ['@states' => implode(', ', $unselectable)]));
    }


    // finish renewable for all except finished states
    if ($state_id === SlogXtwf::XTWF_FINISH_RENEWABLE) {
      $xtwf_states = $workflow->getStates();
      $missed = [];
      foreach ($xtwf_states as $state_id => $xtwf_state) {
        if (!$xtwf_state->isFinishedType() && !in_array($state_id, $fromIds)) {
          $missed[] = $xtwf_state->label();
        }
      }

      if (!empty($missed)) {
        $args = ['@states' => implode(', ', $missed)];
        $form_state->setErrorByName('from', t('For "Finish renewable" select this states: @states.', $args));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $transition_id = $this->getPluginId();
    $values = $form_state->getCompleteFormState()->getValues();
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();
    $configuration = $xtwf_type_plugin->getConfiguration();
    if ($xtwf_transition = $form_state->get('transition')) {
      // prepare values for update
      $this->isOnAddNew = FALSE;
      $values = $form_state->getValues();
      $transition_settings = &$configuration['transitions'][$transition_id]['settings'];
      $xtwf_type_plugin->setConfiguration($configuration);
    }
    else {
      $this->isOnAddNew = TRUE;
      // has to be handled in submitConfigurationForm(), not here
      // new transition is NOT added to $xtwf_type_plugin, and we can't add it here
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $transition_id = $this->getPluginId();
    $values = $form_state->getCompleteFormState()->getValues();
    $workflow = $form_state->getFormObject()->getEntity();
    $xtwf_type_plugin = $workflow->getTypePlugin();
    $configuration = $xtwf_type_plugin->getConfiguration();
    if ($xtwf_transition = $form_state->get('transition') && $this->isOnAddNew) {
      // new transition is now added to $xtwf_type_plugin and we can add settings
      $configuration['transitions'][$transition_id]['settings'] = $this->getSettings();
      $xtwf_type_plugin->setConfiguration($configuration);
    }
  }

}
