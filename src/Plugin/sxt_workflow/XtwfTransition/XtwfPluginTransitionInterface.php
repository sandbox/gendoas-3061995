<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfPluginTransitionInterface.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

use Drupal\sxt_workflow\Entity\XtwfNodeStateInterface;
use Drupal\sxt_workflow\SxtWorkflowStateInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the interface for xtwf state plugins.
 */
interface XtwfPluginTransitionInterface {

  /**
   * Build configuration form for the state plugin.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @param StateInterface $state
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state);

  /**
   * 
   */
  public function formEditAlter(&$form, FormStateInterface $form_state);

  /**
   * 
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state);

  /**
   * 
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state);

}
