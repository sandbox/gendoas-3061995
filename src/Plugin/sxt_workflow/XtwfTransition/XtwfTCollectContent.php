<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTCollectContent.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_collect_content",
 *   label = @Translation("Collect contents"),
 *   settings = {},
 *   weight = 100
 * )
 */
class XtwfTCollectContent extends XtwfPluginTransitionBase {

}
