<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTCompetition.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

use Drupal\sxt_workflow\Entity\XtwfNodeStateInterface;
use Drupal\sxt_workflow\SxtWorkflowStateInterface;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_competition",
 *   label = @Translation("Competition"),
 *   settings = {
 *     "exclusive_from" = FALSE,
 *     "required_from" = {
 *       "xtwf_request_collaborate",
 *       "xtwf_request_renew",
 *     },
 *     "exclude_from" = {
 *       "xtwf_finish_renewable",
 *     }
 *   },
 *   weight = 2000
 * )
 */
class XtwfTCompetition extends XtwfPluginTransitionBase {

}
