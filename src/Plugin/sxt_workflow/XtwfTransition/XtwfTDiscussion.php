<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTDiscussion.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_discussion",
 *   label = @Translation("Discussion"),
 *   settings = {
 *     "required_from" = {
 *       "xtwf_request_renew",
 *     }
 *   },
 *   weight = 2001
 * )
 */
class XtwfTDiscussion extends XtwfPluginTransitionBase {

}
