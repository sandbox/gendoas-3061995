<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTDummy1.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_dummy1",
 *   label = @Translation("Dummy001"),
 *   settings = {
 *     "dev_only" = TRUE,
 *   },
 *   weight = 9997
 * )
 */
class XtwfTDummy1 extends XtwfPluginTransitionBase {

}
