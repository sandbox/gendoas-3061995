<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTDummy2.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_dummy2",
 *   label = @Translation("Dummy002"),
 *   settings = {
 *     "dev_only" = TRUE,
 *   },
 *   weight = 9998
 * )
 */
class XtwfTDummy2 extends XtwfPluginTransitionBase {

}
