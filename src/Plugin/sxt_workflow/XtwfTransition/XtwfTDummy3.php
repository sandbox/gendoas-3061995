<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTDummy3.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_dummy3",
 *   label = @Translation("Dummy003"),
 *   settings = {
 *     "dev_only" = TRUE,
 *   },
 *   weight = 9999
 * )
 */
class XtwfTDummy3 extends XtwfPluginTransitionBase {

}
