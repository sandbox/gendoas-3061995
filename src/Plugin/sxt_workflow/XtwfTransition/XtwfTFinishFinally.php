<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTFinishFinally.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_finish_finally",
 *   label = @Translation("Finish finally"),
 *   settings = {
 *     "exclusive_from" = TRUE,
 *     "required_from" = {
 *       "xtwf_finish_renewable",
 *       "xtwf_request_finish_finally",
 *     }
 *   },
 *   weight = 1995
 * )
 */
class XtwfTFinishFinally extends XtwfPluginTransitionBase {

}
