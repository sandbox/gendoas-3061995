<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTFinishRenewable.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_finish_renewable",
 *   label = @Translation("Finish renewable"),
 *   settings = {
 *     "exclusive_from" = FALSE,
 *     "required_from" = {
 *       "xtwf_request_collaborate",
 *       "xtwf_request_renew",
 *     }
 *   },
 *   weight = 1990
 * )
 */
class XtwfTFinishRenewable extends XtwfPluginTransitionBase {

  /**
   * {@inheritdoc}
   */
  public function getRequiredFromFixed(array $xtwf_states) {
    $required_from = [];
    foreach ($xtwf_states as $state_id => $xtwf_state) {
      if (!$xtwf_state->isFinishedType()) {
        $required_from[] = $state_id;
      }      
    }
    
    return $required_from;
  }

}
