<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTFreeContributions.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_free_contributions",
 *   label = @Translation("Free contributions"),
 *   settings = {},
 *   weight = 1000
 * )
 */
class XtwfTFreeContributions extends XtwfPluginTransitionBase {

}
