<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTRequestFinishFinally.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_request_finish_finally",
 *   label = @Translation("Request for finish finally"),
 *   settings = {
 *     "exclusive_from" = TRUE,
 *     "required_from" = {
 *       "xtwf_finish_renewable",
 *     }
 *   },
 *   weight = 1991
 * )
 */
class XtwfTRequestFinishFinally extends XtwfPluginTransitionBase {

}
