<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTRequestRenew.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;

/**
 * ...
 *
 * @XtwfTransition(
 *   id = "xtwf_request_renew",
 *   label = @Translation("Request for renew"),
 *   settings = {
 *     "exclusive_from" = TRUE,
 *     "required_from" = {
 *       "xtwf_finish_renewable",
 *     }
 *   },
 *   weight = 1991
 * )
 */
class XtwfTRequestRenew extends XtwfPluginTransitionBase {

}
