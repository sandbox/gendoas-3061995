<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition\XtwfTSubWfWaiting.
 */

namespace Drupal\sxt_workflow\Plugin\sxt_workflow\XtwfTransition;
//todo::current:: ----sxt_sxt_workflow --- xtwf_subwf_waiting

/**
 * ...
 *
 * @XtwfTransitionXXX(
 *   id = "xtwf_subwf_waiting",
 *   label = @Translation("Waiting for sub workflow"),
 *   settings = {
 *     "required_from" = {
 *       "xtwf_request_collaborate",
 *     }
 *   },
 *   weight = 1980
 * )
 */
class XtwfTSubWfWaiting extends XtwfPluginTransitionBase {

}
