<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\SlogXtwf.
 */

namespace Drupal\sxt_workflow;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_group\SxtGroup;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\Role;
use Drupal\node\NodeInterface;
use Drupal\workflows\WorkflowInterface;
use Drupal\Core\Cache\Cache;

/**
 */
class SlogXtwf {

  const XTWF_REQUEST_COLLABORATE = 'xtwf_request_collaborate';
  const XTWF_COLLECT_CONTENT = 'xtwf_collect_content';
//todo::current:: ----sxt_sxt_workflow --- xtwf_subwf_waiting
//  const XTWF_SUBWF_WAITING = 'xtwf_subwf_waiting';
  const XTWF_REQUEST_RENEW = 'xtwf_request_renew';
  const XTWF_REQUEST_FINISH_FINALLY = 'xtwf_request_finish_finally';
  const XTWF_FINISH_RENEWABLE = 'xtwf_finish_renewable';
  const XTWF_FINISH_FINALLY = 'xtwf_finish_finally';
  const XTWF_COMPETITION = 'xtwf_competition';
  const XTWF_DISCUSSION = 'xtwf_discussion';
  const XTWF_FREE_CONTRIB = 'xtwf_free_contributions';

  const XTWF_SELECT_YES = 'yes';
  const XTWF_SELECT_NO = 'no';
  
  protected static $workflows_all;
  protected static $workflows = [];
  protected static $workflows_sorted = [];
  protected static $nodes = [];
  protected static $node_states = [];

  const XTWF_PREFIX_DASH = '- ';
  
  /**
   * @var array
   */
  protected static $cacheDefaultSettings = NULL;
  protected static $cachePermCollaborateAdmin = [];

  /**
   * Returns the plugin manager for a certain plugin type.
   *
   * @param string $type
   *   The plugin type, e.g. state, transition.
   * @return object 
   *   A plugin manager instance.
   */
  public static function pluginManager($type) {
    return \Drupal::service("plugin.manager.sxt_workflow.$type");
  }

  /**
   * Return state severity options.
   * 
   * @return array for leight, medium, severe and severest.
   */
  public static function getSeverityLabel($key) {
    $labels = self::getSeverityOptions();
    return ($labels[$key] ?? '_???_getSeverityLabel');
  }

  /**
   * Return state severity options.
   * 
   * @return array for leight, medium, severe and severest.
   */
  public static function getSeverityOptions() {
    return [
      'leight' => t('Leight'),
      'medium' => t('Medium'),
      'severe' => t('Severe'),
      'severest' => t('Extra severe'),
    ];
  }

  public static function getYesNoOptions() {
    return [
      SlogXtwf::XTWF_SELECT_NO => t('No'),
      SlogXtwf::XTWF_SELECT_YES => t('Yes'),
    ];
  }

  public static function getStrictOptions() {
    return self::getYesNoOptions();
  }

  public static function getDiscussOptions() {
    return self::getYesNoOptions();
  }

  public static function getVoteLabel($key) {
    $labels =  [
      SlogXtwf::XTWF_SELECT_YES => t('Yes'),
      SlogXtwf::XTWF_SELECT_NO => t('No'),
      SlogXtwf::XTWF_DISCUSSION => t("Let's discuss"),
    ];
    
    return ($labels[$key] ?? '_???_getVoteLabel');
  }

  /**
   * Return state finilize request  options.
   * 
   * @return array for requests and timeout.
   */
  public static function getFinalizeOptions() {
    return [
      'requests' => t('Requests'),
      'timeout' => t('Timeout'),
    ];
  }

  public static function getFinalizedByText($key) {
    $labels =  [
      'request' => t('Request'),
      'required' => t('Required'),
      'timeout' => t('Timeout'),
      'admin' => t('Admin'),
      'abort' => t('Abort'),
    ];
    return (string) ($labels[$key] ?? $key); 
  }

  public static function getTxtLetsDiscuss() {
    return (string) t("Let's discuss it first");
  }

  /**
   * Return state severity options.
   * 
   * @return array for leight, medium, severe and severest.
   */
  public static function getStateSeverityOptions() {
//    return self::pluginManager('state')->getSeverityOptions();
    return self::getSeverityOptions();
  }

  /**
   * Return state finilize request  options.
   * 
   * @return array for requests and timeout.
   */
  public static function getStateFinalizeOptions() {
//    return self::pluginManager('state')->getFinalizeOptions();
    return self::getFinalizeOptions();
  }

  public static function getWorkflowOptions(array $workflows) {
    $options = [];
    foreach ($workflows as $workflow_id => $workflow) {
      $options[$workflow_id] = $workflow->label();
    }
    return $options;
  }

  public static function getAllForcedStates($is_trunk) {
    $base_states = self::getBaseForcedStates($is_trunk);
    $additional_states = array_keys(self::getConfigForcedStates());
    return ($base_states + $additional_states);
  }

  public static function getBaseForcedStates($is_trunk) {
    $forced = [
      self::XTWF_FINISH_RENEWABLE,
      self::XTWF_FINISH_FINALLY,
      self::XTWF_REQUEST_RENEW,
      self::XTWF_REQUEST_FINISH_FINALLY,
    ];
    if ($is_trunk) {
      $forced[] = self::XTWF_REQUEST_COLLABORATE;
    }
    
    return $forced;
  }

  public static function getConfigForcedStates() {
    $default = [
      self::XTWF_COMPETITION => TRUE,
      self::XTWF_DISCUSSION => TRUE,
    ];
    $forced_states = (\Drupal::config('sxt_workflow.settings')->get('forced_states')) ?? [];
    $base_states = self::getBaseForcedStates(TRUE);
    $forced_states = array_diff_key($forced_states, array_combine($base_states, $base_states));
    
    return ($forced_states + $default);
  }

  /**
   * Maximal depth for nesting workflows (defaults to 3).
   * 
   * May be changed in sxt_workflow.settings
   * 
   * @return integer
   */
  public static function getMaxDepth() {
//todo::current::sxt_workflow::bundle
    return (\Drupal::config('sxt_workflow.settings')->get('max_depth') ?: 3);
  }

  /**
   * 
   * @return integer
   */
  public static function getDynamicTimeout() {
    return (integer) (\Drupal::config('sxt_workflow.settings')->get('dynamic_timeout') ?: 10);
  }

  /**
   * 
   * @return integer
   */
  public static function getPercentSevere() {
    return (integer) (\Drupal::config('sxt_workflow.settings')->get('percent_severe') ?: 80);
  }

  /**
   * 
   * @return boolean
   */
  public static function getAdminUidShow() {
    return (boolean) (\Drupal::config('sxt_workflow.settings')->get('admin_id_show'));
  }

  /**
   * 
   * @return boolean
   */
  public static function getDoLogWarnings() {
    return (boolean) (\Drupal::config('sxt_workflow.settings')->get('do_log_warnings'));
  }

  /**
   * Get definitions of a plugin type.
   * 
   * @param string $type
   *  The plugin type, e.g. target_entity.
   * 
   * @return array
   *  Collection of found definitions, e.g. slog vocabulary definitions.
   */
  public static function getDefinitions($type) {
    return self::pluginManager($type)->getDefinitionsSorted();
  }

  /**
   * Return workflow plugins by type.
   * 
   * @param string $type
   *  One of 'state' or 'transition'
   * @param string $which optional, defaults to 'enabled'
   * @return array
   */
  public static function getWorkflowPlugins($type, $which = 'enabled') {
    return self::pluginManager($type)->getXtwfPlugins($which);
  }

  public static function getStatePlugin($state_id, $which = 'enabled') {
    $plugins = self::getWorkflowPlugins('state', $which);
    return $plugins[$state_id];
  }

  public static function getTransitionPlugin($transition_id, $which = 'enabled') {
    $plugins = self::getWorkflowPlugins('transition', $which);
    return $plugins[$transition_id];
  }

  /**
   * Return the available bundle types.
   * 
   * Values: 'trunk', 'fork', 'leaf'
   * 
   * @return array of strings
   */
  public static function getBundleTypes() {
    return ['trunk', 'fork', 'leaf'];
  }

  /**
   * Return workflow instance, if exists.
   * 
   * @param string $workflow_id
   * @return Drupal\workflows\Entity\Workflow or NULL
   */
  public static function getWorkflow($workflow_id) {
    $workflows = self::getWorkflowsAll();
    return ($workflows[$workflow_id] ?: NULL);
  }

  /**
   * Return all available workflows
   * 
   * @return array of workflow objects
   */
  public static function getWorkflowsAll() {
    if (!isset(self::$workflows_all)) {
      $storage = \Drupal::entityTypeManager()->getStorage('workflow');
      $workflow_ids = $storage->getQuery()->execute();
      $workflows = $storage->loadMultipleOverrideFree($workflow_ids);
      uasort($workflows, ['self', 'wfSort']);
      self::$workflows_all = $workflows;
    }
    return self::$workflows_all;
  }

  /**
   * {@inheritdoc}
   */
  public static function wfSort(WorkflowInterface $a, WorkflowInterface $b) {
    $a_weight = $a->getTypePlugin()->getWeight();
    $b_weight = $b->getTypePlugin()->getWeight();
    if ($a_weight == $b_weight) {
      $a_label = $a->label();
      $b_label = $b->label();
      return strnatcasecmp($a_label, $b_label);
    }
    return ($a_weight < $b_weight) ? -1 : 1;
  }

  /**
   * Return all active workflows
   * 
   * @return array of workflow objects
   */
  public static function getWorkflowsAktive() {
    $active = [];
    $workflows = self::getWorkflowsAll();
    foreach ($workflows as $workflow_id => $workflow) {
      if ($workflow->status()) {
        $active[$workflow_id] = $workflow;
      }
    }

    return $active;
  }

  /**
   * Return a array of workflows applied to node type and bundle.
   * 
   * Bundle is one of trunk, fork and leaf,
   * 
   * @param string $node_type
   * @param string $bundle optional, defaults to 'trunk'
   * @return array of workflow objects
   */
  public static function getWorkflows($node_type, $bundle) {
    if (!isset(self::$workflows[$node_type][$bundle])) {
      $workflows = self::getWorkflowsAll();

      $result = [];
      foreach ($workflows as $workflow_id => $workflow) {
        $xtwf_type_plugin = $workflow->getTypePlugin();
        if ($xtwf_type_plugin->appliesToNodeType($node_type) && $bundle === $xtwf_type_plugin->getBundle()) {
          $result[$workflow_id] = $workflow;
        }
      }
      self::$workflows[$node_type][$bundle] = $result;
    }
    
    return self::$workflows[$node_type][$bundle];
  }

  /**
   * Returns all node types with their applicable workflows.
   * 
   * @return array
   */
  public static function getNodetypesWithWorkflows() {
    $cid = 'sxt_workflow.nodetypes.with.workflow';
    if (!$cache = SlogXt::cache()->get($cid)) {
      $node_types = [];
      $workflows = self::getWorkflowsAll();
      foreach ($workflows as $workflow_id => $workflow) {
        $type_settings = $workflow->getPluginCollections()['type_settings'];
        $types = $type_settings->getConfiguration()['node_types'] ?? [];
        if (!empty($types)) {
          foreach ($types as $nodetype) {
            if (!isset($node_types[$nodetype])) {
              $node_types[$nodetype] = [];
            }
            $node_types[$nodetype][] = $workflow_id;
          }
        }
      }
      $tags = [$cid];
      SlogXt::cache()->set($cid, $node_types, Cache::PERMANENT, $tags);
    }
    else {
      $node_types = $cache->data;
    }

    return $node_types;
  }

  /**
   * Whether there is a workflow running for node id.
   * 
   * @param integer $node_id
   * @return boolean
   */
  public static function hasNodeState($node_id, $reload = FALSE) {
    return !empty(self::getNodeState($node_id, $reload));
  }

  /**
   * Return the xtwf state entity for node id if exists.
   * 
   * @param type $node_id
   * @return mixed
   *  \Drupal\sxt_workflow\Entity\SxtWorkflowState if found, FALSE otherwise
   */
  public static function getNodeState($node_id, $reload = FALSE) {
    if ($reload && isset(self::$node_states[$node_id])) {
      unset(self::$node_states[$node_id]);
    }
    if (!isset(self::$node_states[$node_id])) {
      $storage = \Drupal::entityTypeManager()->getStorage('xtwf_node_state');
      if ($reload) {
        $storage->resetCache([$node_id]);
      }
      $node_state = $storage->load($node_id);
      self::$node_states[$node_id] = $node_state ?: FALSE;
    }

    return self::$node_states[$node_id];
  }

  /**
   * Whether notifikation is available.
   * 
   * @return boolean
   */
  public static function canNotify() {
//todo::next:: ----sxt_workflow --- notifikation
    return TRUE;
  }

  /**
   * Return local cached node.
   * 
   * @param integer $node_id
   * @param boolean $reload
   * @return Drupal\node\NodeInterface
   */
  public static function getNode($node_id, $reload = FALSE) {
    if ($reload && isset(self::$nodes[$node_id])) {
      unset(self::$nodes[$node_id]);
    }
    if (!isset(self::$nodes[$node_id])) {
      self::$nodes[$node_id] = Node::load($node_id);
    }

    return self::$nodes[$node_id];
  }

  /**
   * Whether there are workflows defined for the node.
   * 
   * Workflows are defined by node's type.
   * 
   * @param integer $node_id
   * @return boolean
   */
  public static function hasNodeWorkflows($node_id) {
    $node = self::getNode($node_id);
    $workflows = self::getWorkflows($node->getType(), 'trunk');
    return !empty($workflows);
  }

  /**
   * Whether user can administrate collaboration of the node.
   * 
   * @param NodeInterface $node
   * @param mixed $account optional
   *  \Drupal\Core\Session\AccountProxyInterface or NULL for current user
   * @return boolean
   */
  public static function hasNodePermCollaborateAdmin(NodeInterface $node, $account = NULL) {
    $account = $account ?: \Drupal::currentUser();
    if (!$account->isAuthenticated()) {
      return FALSE;
    }
    elseif (SlogXt::isSuperUser($account)) {
      return TRUE;
    }

    $has_perm = FALSE;
    $uid = $account->id();
    $nid = $node->id();
    if (isset(self::$cachePermCollaborateAdmin[$uid][$nid])) {
      $has_perm = self::$cachePermCollaborateAdmin[$uid][$nid];
    }
    else {
      $toolbar_id = XtsiNodeTypeData::getPermBaseTbIdByNode($node);
      if (empty($toolbar_id) || $toolbar_id === 'user' || $toolbar_id === '__error__') {
        //do nothing, return FALSE;
      }
      elseif ($toolbar_id === 'role') {
        $xtnodebase_sid = $node->get('xtnodebase')->base_sid;
        $role_id = XtsiNodeTypeData::getTargetEntityIdFromBaseSid($xtnodebase_sid);
        if ($role_id && $role = Role::load($role_id)) {
          $has_perm = SxtGroup::hasPermission('admin sxtrole collaborate', $account, $role);
        }
      }
      else {
        $vg_tbs = SlogTx::getToolbarsVisibleGlobal();
        if (in_array($toolbar_id, array_keys($vg_tbs))) {
          $has_perm = $account->hasPermission("administer collaborate $toolbar_id");
        }
      }

      self::$cachePermCollaborateAdmin[$uid][$nid] = $has_perm;
    }

    return $has_perm;
  }

  public static function logger() {
    return \Drupal::logger('sxt_workflow');
  }

}
