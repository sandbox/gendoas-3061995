<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\SxtWorkflowInstall.
 */

namespace Drupal\sxt_workflow;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\slogxt\SlogXt;
use Drupal\workflows\WorkflowInterface;

/**
 * Static install functions for sxt_workflow module
 */
class SxtWorkflowInstall {

  /**
   * Implements hook_requirements().
   */
  public static function requirements() {
    $requirements = self::_requirementsTransitions() + self::_requirementsWorkflowsStructure();

    return $requirements;
  }

  private static function _requirementsTransitions() {
    $requirements = [];
    $err_transitions = [];
    foreach (SlogXtwf::getWorkflowPlugins('transition') as $tid => $transition) {
      if (!$transition->doesMeetRequirements()) {
        $err_transitions[] = $transition->getLabel();
      }
    }

    if (!empty($err_transitions)) {
      $requirements['sxt_workflow_trans_err'] = [
        'title' => 'SlogSys Xtwf Transitions',
        'value' => t('Plugin requirement missmatch found: @trans', ['@trans' => implode(', ', $err_transitions)]),
        'description' => SlogXt::txtSeeLogMessages(),
        'severity' => REQUIREMENT_ERROR,
      ];
    }

    return $requirements;
  }

  private static function _requirementsWorkflowsStructure() {
    $requirements = [];
    $err_workflows = [];
    $workflow_labels = [];
    $req_id = 'sxt_workflow_wf_structure';
    $req_title = 'SlogSys Xtwf Structure';

    foreach (SlogXtwf::getWorkflowsAktive() as $workflow_id => $workflow) {
      $workflow_label = $workflow->label();
      $workflow_labels[] = $workflow_label;

      if (!self::wfIsOkStructure($workflow)) {
        $err_workflows[$workflow_id] = $workflow_label;
      }
    }

    if (!empty($err_workflows)) {
      $str_workflows = implode(', ', array_values($err_workflows));
      $requirements[$req_id] = [
        'title' => $req_title,
        'value' => t('Workflows with bad structure found: @workflows', ['@workflows' => $str_workflows]),
        'description' => SlogXt::txtSeeLogMessages(),
        'severity' => REQUIREMENT_ERROR,
      ];
    }

    if (empty($requirements)) {
      $str_workflows = implode(', ', $workflow_labels);
      $requirements[$req_id] = [
        'title' => $req_title,
        'value' => "Structures of workflows has been verified: $str_workflows",
        'severity' => REQUIREMENT_OK,
      ];
    }

    return $requirements;
  }

  public static function wfIsOkStructure(WorkflowInterface $workflow) {
    $xtwf_type_plugin = $workflow->getTypePlugin();
    $workflow_label = $workflow->label();

    $structure_is_ok = TRUE;

    // required states
    $s_required = $xtwf_type_plugin->getRequiredStates();
    $state_ids = $xtwf_type_plugin->getStateIds();
    foreach ($s_required as $state_id) {
      if (!in_array($state_id, $state_ids)) {
        $structure_is_ok = FALSE;
        $args = [
          '@workflow' => $workflow_label,
          '@state' => $state_id,
        ];
        $msg = t('State not found in workflow: workflow=@workflow, id=@state', $args);
        SlogXtwf::logger()->error($msg);
      }
    }

    // states reachable
    $transition_ids = $xtwf_type_plugin->getTransitionIds();
    if ($structure_is_ok) {
      foreach ($state_ids as $state_id) {
        if ($state_id !== SlogXtwf::XTWF_REQUEST_COLLABORATE && !in_array($state_id, $transition_ids)) {
          $structure_is_ok = FALSE;
          $args = [
            '@workflow' => $workflow_label,
            '@state' => $state_id,
          ];
          $msg = t('State not reachable in workflow: workflow=@workflow, id=@state', $args);
          SlogXtwf::logger()->error($msg);
        }
      }
    }

    // required transitions
    if ($structure_is_ok) {
      $t_required = $xtwf_type_plugin->getRequiredTransitions();
      foreach ($t_required as $transition_id) {
        if (!in_array($transition_id, $transition_ids)) {
          $structure_is_ok = FALSE;
          $args = [
            '@workflow' => $workflow_label,
            '@transition' => $transition_id,
          ];
          $msg = t('Transition not found in workflow: workflow=@workflow, id=@transition', $args);
          SlogXtwf::logger()->error($msg);
        }
      }
    }

    // required 'from' and 'to' by transition
    if ($structure_is_ok) {
      foreach ($transition_ids as $transition_id) {
        $xtwf_transition = $xtwf_type_plugin->getTransition($transition_id);
        if (!$xtwf_transition->doesMeetRequirements($workflow_label)) {
          $structure_is_ok = FALSE;
        }
      }
    }

    return $structure_is_ok;
  }

  public static function wfFixTransitionFrom(WorkflowInterface $workflow) {
    $type_plugin = $workflow->getTypePlugin();
    $xtwf_states = $type_plugin->getStates();
    $changed = FALSE;
    foreach ($type_plugin->getTransitions() as $transition_id => $xtwf_transition) {
      $from_state_ids = $xtwf_transition->getRequiredFromFixed($xtwf_states);
      if (!empty($from_state_ids)) {
        $changed = TRUE;
        $type_plugin->setTransitionFromStates($transition_id, $from_state_ids);
      }
    }
    
    if ($changed) {
      $workflow->setOriginalId($workflow->id());
      $workflow->save();
      $msg = t('Run wfFixTransitionFrom for workflow @workflow', ['@workflow' => $workflow->label()]);
      SlogXtwf::logger()->warning($msg);
    }
  }

}
