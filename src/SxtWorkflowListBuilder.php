<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\SxtWorkflowListBuilder.
 */

namespace Drupal\sxt_workflow;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\workflows\WorkflowListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a listing of Workflow entities.
 */
class SxtWorkflowListBuilder extends WorkflowListBuilder implements FormInterface {

  /**
   * {@inheritdoc}
   */
  protected $limit = FALSE;   // no pager

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_workflow_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = parent::buildHeader();
    $op = $header['operations'];
    unset($header['operations']);
    $header['weight'] = t('Weight');
    $header['operations'] = $op;
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $xtwf_type_plugin = $entity->getTypePlugin();
    $weight = $xtwf_type_plugin->getWeight();

    $row = parent::buildRow($entity);
    $op = $row['operations'];
    unset($row['operations']);

    $row['#attributes']['class'][] = 'draggable';
    $row['#weight'] = $weight;
    // Add weight column.
    $row['weight'] = [
      '#type' => 'weight',
      '#title' => t('Weight for @title', ['@title' => $entity->label()]),
      '#title_display' => 'invisible',
      '#default_value' => $weight,
      '#attributes' => ['class' => ['weight']],
    ];
    
    $row['operations'] = $op;
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = $this->formBuilder()->getForm($this);
    $workflow_types_count = count($this->workflowTypeManager->getDefinitions());
    if ($workflow_types_count === 0) {
      $build['table']['#empty'] = $this->t('There are no workflow types available. In order to create workflows you need to install a module that provides a workflow type. For example, the <a href=":content-moderation">Content Moderation</a> module provides a workflow type that enables workflows for content entities.', [':content-moderation' => '/admin/modules#module-content-moderation']);
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->entitiesKey = 'workflows';
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('There is no @label yet.', ['@label' => $this->entityType->getLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);
      $row['label'] = ['#markup' => $row['label']];
      $row['weight']['#delta'] = $delta;
      $form[$this->entitiesKey][$entity->id()] = $row;
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    return SlogXtwf::getWorkflowsAll();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      $workflow = $this->entities[$id]; 
      if ($workflow) {
        $weight_old = $workflow->getTypePlugin()->getWeight();
        if ($weight_old != $value['weight']) {
          $workflow->getTypePlugin()->setWeight($value['weight']);
          $workflow->save();
        }
      }
    }
  }

  /**
   * Returns the form builder.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   The form builder.
   */
  protected function formBuilder() {
    if (!$this->formBuilder) {
      $this->formBuilder = \Drupal::formBuilder();
    }
    return $this->formBuilder;
  }

}
