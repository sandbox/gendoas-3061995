<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\SxtWorkflowState.
 */

namespace Drupal\sxt_workflow;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\workflows\WorkflowTypeInterface;
use Drupal\slogxt\XtDateTimeTrait;
use Drupal\workflows\State;

/**
 * A value object representing a workflow state for SxtWorkflow.
 */
class SxtWorkflowState extends State implements SxtWorkflowStateInterface {

//todo::current:: ----sxt_workflow --- SxtWorkflowStateInterface
//todo::current:: ----sxt_workflow --- SxtWorkflowStateInterface
  use XtDateTimeTrait;

  /**
   * Settings for single state
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Data required for finalizing last state and transit to this.
   *
   * @var array
   */
  protected $finalize_args = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(WorkflowTypeInterface $workflow, $id, $label, $weight = 0, $settings = []) {
    parent::__construct($workflow, $id, $label, $weight);
    $this->settings = $settings;
  }

  /**
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   */
  public function getSetting($key) {
    return isset($this->settings[$key]) ? $this->settings[$key] : NULL;
  }

  /**
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isStateType($type) {
    return ($this->getSetting('state_type') === $type);
  }

  /**
   * {@inheritdoc}
   */
  public function isRequestType() {
    return $this->isStateType('request');
  }

  /**
   * {@inheritdoc}
   */
  public function isSubWorkflowState() {
    return $this->isCollectContentType() && $this->getSetting('is_subworkflow');
  }

  /**
   * {@inheritdoc}
   */
  public function isSubWfWaitingState() {
    return $this->isStateType('subwf_waiting');
  }

  /**
   * {@inheritdoc}
   */
  public function isCollectContentType() {
    return $this->isStateType('collect_content');
  }

  /**
   * {@inheritdoc}
   */
  public function isCompetitionType() {
    return $this->isStateType('competition');
  }

  /**
   * {@inheritdoc}
   */
  public function isFinishedType() {
    return $this->isStateType('finished');
  }

  /**
   */
  public function getStateType() {
    return $this->getSetting('state_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getUseSeverity() {
    return (boolean) $this->getSetting('use_severity');
  }

  /**
   * {@inheritdoc}
   */
  public function getUseDynamicSeverity() {
    return (boolean) $this->getSetting('use_dynamic_severity');
  }

  /**
   * {@inheritdoc}
   */
  public function getUseAnySeverity() {
    return ($this->getUseSeverity() || $this->getUseDynamicSeverity());
  }

  /**
   */
  public function getNumberDescription() {
    return $this->getSetting('number_description') ?: t('...not implemented...');
  }

  /**
   */
  public function getNumberSuffix() {
//todo::current:: ----sxt_workflow --- getSetting('target_text')
    return $this->getSetting('number_suffix') ?: t('items');
  }

  /**
   */
  public function getSeverityInfos($severity_id = NULL) {
    $state_plugin = SlogXtwf::getStatePlugin($this->id());
    return $state_plugin->getSeverityInfos($this, $severity_id);
  }

  /**
   */
  public function getSeverity($severity_id) {
    if ($this->getUseSeverity()) {
      return ($this->getSetting('severity')[$severity_id] ?? []);
    }
  }

  /**
   */
  public function getSeveritySettings() {
    if ($this->getUseSeverity()) {
      return $this->getSetting('severity');
    }
    return FALSE;
  }

  /**
   */
  public function setSeverity($severity_id, array $values) {
    if ($this->getUseSeverity()) {
      $settings = $this->getSettings();
      $settings['severity'][$severity_id] = $values;
      $this->setSettings($settings);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityDescription($severity_id) {
    if ($this->getUseSeverity()) {
      $suffix = $this->getNumberSuffix();
      $severity = $this->getSeverity($severity_id);
      $lines = [
        t('Required: @required @target', ['@target' => $suffix, '@required' => $severity['number']]),
        t('Timeout: @timeout days', ['@timeout' => $severity['timeout']]),
      ];

      return implode('<br />', $lines);
    }

    return t('State does not use severity.');
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityRequired($severity_id, $dynamic_required) {
    if ($this->getUseSeverity()) {
      $severity = $this->getSeverity($severity_id);
      return (integer) $severity['number'];
    }
    elseif ($this->getUseDynamicSeverity()) {
      return (integer) $dynamic_required;
    }

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityTimeout($severity_id) {
    if ($this->getUseSeverity()) {
      $severity = $this->getSeverity($severity_id);
      return (integer) $severity['timeout'];
    }
    elseif ($this->getUseDynamicSeverity()) {
      return SlogXtwf::getDynamicTimeout();
    }
    else {
      return $this->getMaxDaysToAdd();
    }
  }

}
