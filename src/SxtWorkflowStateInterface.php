<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\SxtWorkflowStateInterface.
 */

namespace Drupal\sxt_workflow;

/**
 * Description of ...
 */
interface SxtWorkflowStateInterface {

//todo::current:: ----sxt_workflow --- SxtWorkflowStateInterface

  public function getSettings();

  public function getSetting($key);

  public function setSettings(array $settings);

  public function isRequestType();

  public function isSubWorkflowState();

  public function isSubWfWaitingState();

  public function isCollectContentType();

  public function isCompetitionType();

  public function isFinishedType();

  public function getUseSeverity();

  public function getUseDynamicSeverity();

  public function getUseAnySeverity();

  public function getNumberDescription();

  public function getNumberSuffix();

  public function getSeverityInfos($severity_id = NULL);

  public function getSeverity($severity_id);

  public function setSeverity($severity_id, array $values);

  public function getSeverityDescription($severity_id);
}
