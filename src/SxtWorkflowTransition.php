<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\SxtWorkflowTransition.
 */

namespace Drupal\sxt_workflow;

use Drupal\sxt_workflow\SlogXtwf;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_workflow\SxtWorkflowTransitionInterface;
use Drupal\workflows\WorkflowTypeInterface;
use Drupal\workflows\Transition;

/**
 * A value object representing a workflow transition for SxtWorkflow.
 */
class SxtWorkflowTransition extends Transition implements SxtWorkflowTransitionInterface {

  /**
   * Settings for single transition
   *
   * @var bool
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function __construct(WorkflowTypeInterface $workflow, $id, $label, array $from_state_ids, $to_state_id, $weight = 0, $settings = []) {
    parent::__construct($workflow, $id, $label, $from_state_ids, $to_state_id, $weight);
    $this->settings = $settings;
  }

  /**
   */
  public function getWorkflow() {
    return $this->workflow;
  }

  /**
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   */
  public function getSetting($key) {
    return isset($this->settings[$key]) ? $this->settings[$key] : NULL;
  }

  /**
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    return $this;
  }

  /**
   */
  public function getInfoFromRequirements() {
    $transition_plugin = SlogXtwf::getTransitionPlugin($this->id());
    $t_settings = $transition_plugin->getSettings();
    $exclusive_from = (boolean) $t_settings['exclusive_from'];
    $required_from = $t_settings['required_from'] ?? [];
    $exclude_from = $t_settings['exclude_from'] ?? [];
    $args = [
      '@exclusive' => ($exclusive_from ? 'TRUE' : 'FALSE'),
      '@required' => !empty($required_from) ? implode(', ', $required_from) : 'none',
      '@exclude' => !empty($exclude_from) ? implode(', ', $exclude_from) : 'none',
    ];

    return t('exclusive=@exclusive, required=@required, excluded=@exclude', $args);
  }

  /**
   * Whether requirements are met.
   * 
   * @param string $workflow_label
   * @return boolean
   */
  public function doesMeetRequirements($workflow_label) {
    $transition_plugin = SlogXtwf::getTransitionPlugin($this->id());
    $t_settings = $transition_plugin->getSettings();
    $exclusive_from = (boolean) $t_settings['exclusive_from'];
    $required_from = $t_settings['required_from'] ?? [];
    $exclude_from = $t_settings['exclude_from'] ?? [];

    $fromIds = $this->fromStateIds;
    $this_transition_id = $this->id();

    $args = [
      '@preface' => t('Requirement missmatch in workflow/transition'),
      '@workflow' => $workflow_label,
      '@transition' => $this->label(),
    ];
    // Equal ids
    $does_meet = ($this->toStateId === $this_transition_id);
    if (!$does_meet) {
      $args['@info'] = 'Transition id and toStateId has to be equal.';
      $msg = t('@preface: @workflow / @transition, @info', $args);
      SlogXtwf::logger()->error($msg);
    }

    // Exclusive from
    if ($does_meet && $exclusive_from) {
      $does_meet = (count($fromIds) === count($required_from));
      if ($does_meet) {
        $does_meet = empty(array_diff($fromIds, $required_from));
      }
      if (!$does_meet) {
        $args['@info'] = 'Exclusive from: ' . implode(', ', $required_from);
        $msg = t('@preface: @workflow / @transition, @info', $args);
        SlogXtwf::logger()->error($msg);
      }
    }

    // Required from
    if ($does_meet && !empty($required_from)) {
      $does_meet = (count(array_intersect($fromIds, $required_from)) === count($required_from));
      if (!$does_meet) {
        $args['@info'] = 'Required: ' . implode(', ', $required_from);
        $msg = t('@preface: @workflow / @transition, @info', $args);
        SlogXtwf::logger()->error($msg);
      }
    }

    // Excluded from
    if ($does_meet && !empty($exclude_from)) {
      $does_meet = empty(array_intersect($fromIds, $exclude_from));
      if (!$does_meet) {
        $args['@info'] = 'Excluded: ' . implode(', ', $exclude_from);
        $msg = t('@preface: @workflow / @transition, @info', $args);
        SlogXtwf::logger()->error($msg);
      }
    }

    // unselectable: self and finish finally
    $unselectable = array_intersect([SlogXtwf::XTWF_FINISH_FINALLY, $this_transition_id], $fromIds);
    if (!empty($unselectable)) {
      $does_meet = FALSE;
      $args['@info'] = 'Unselectable from: ' . implode(', ', $unselectable);
      $msg = t('@preface: @workflow / @transition, @info', $args);
      SlogXtwf::logger()->error($msg);
    }
    
    // finish renewable for all except finished states
    if ($this_transition_id === SlogXtwf::XTWF_FINISH_RENEWABLE) {
      $missed = [];
      $xtwf_states = $this->getWorkflow()->getStates();
      foreach ($xtwf_states as $state_id => $xtwf_state) {
        if (!$xtwf_state->isFinishedType() && !in_array($state_id, $fromIds)) {
          $missed[] = $xtwf_state->label();
        }
      }
      
      if (!empty($missed)) {
        $does_meet = FALSE;
        $args['@info'] = 'Missing from: ' . implode(', ', $missed);
        $msg = t('@preface: @workflow / @transition, @info', $args);
        SlogXtwf::logger()->error($msg);
      }
    }

    if (!SlogXtsi::isDebugMode()) {
      foreach (['xtwf_dummy1', 'xtwf_dummy2', 'xtwf_dummy3'] as $dummy_id) {
        if ($this_transition_id === $dummy_id) {
          $does_meet = FALSE;
          $args['@info'] = 'Dummy is not a real state/transition.';
          $msg = t('@preface: @workflow / @transition, @info', $args);
          SlogXtwf::logger()->error($msg);
          break;
        }
      }
    }

    return $does_meet;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredFromFixed(array $xtwf_states) {
    $required_from = SlogXtwf::getTransitionPlugin($this->id())->getRequiredFromFixed($xtwf_states);
    if (!empty($required_from)) {
      $this->fromStateIds = array_values(array_unique(array_merge($this->fromStateIds, $required_from)));
      return $this->fromStateIds;
    }
    
    return FALSE;
  }

}
