<?php

/**
 * @file
 * Contains \Drupal\sxt_workflow\XtwfNodeSubscribe.
 */

namespace Drupal\sxt_workflow;

/**
 */
class XtwfNodeSubscribe {

  /**
   * Whether the user is subscribed for the node.
   * 
   * Subscibed users are notified on each state transition.
   * 
   * @param integer $nid node id
   * @param integer $uid user id
   * @return boolean
   */
  public static function hasSubscribed($nid, $uid) {
    $nid = \Drupal::database()->select('xtwf_node_subscribe', 'nf')
        ->fields('nf', ['nid'])
        ->condition('nf.nid', $nid)
        ->condition('nf.uid', $uid)
        ->execute()
        ->fetchCol();

    return !empty($nid);
  }

  /**
   * Subscribe the user for the node.
   * 
   * Subscibed users are notified on each state transition.
   * 
   * @param integer $nid node id
   * @param integer $uid user id
   */
  public static function subscribe($nid, $uid) {
    if (!self::hasSubscribed($nid, $uid)) {
      \Drupal::database()->insert('xtwf_node_subscribe')
          ->fields(['nid', 'uid', 'changed'])
          ->values(['nid' => $nid, 'uid' => $uid, 'changed' => REQUEST_TIME])
          ->execute();
    }
  }

  /**
   * Unsubscribe the user for the node.
   * 
   * Subscibed users are notified on each state transition.
   * 
   * @param integer $nid node id
   * @param integer $uid user id
   */
  public static function unsubscribe($nid, $uid) {
    if (self::hasSubscribed($nid, $uid)) {
      \Drupal::database()->delete('xtwf_node_subscribe')
          ->condition('nid', $nid)
          ->condition('uid', $uid)
          ->execute();
    }
  }

  /**
   * Unsubscribe all subscriptions for nodes
   * 
   * @param array $node_ids
   *  Array of node ids
   */
  public static function unsubscribeForNodes(array $node_ids) {
    \Drupal::database()->delete('xtwf_node_subscribe')
        ->condition('nid', $node_ids, 'IN')
        ->execute();
  }

  /**
   * Unsubscribe all subscriptions for users
   * 
   * @param array $user_ids
   *  Array of user ids
   */
  public static function unsubscribeForUsers(array $user_ids) {
    \Drupal::database()->delete('xtwf_node_subscribe')
        ->condition('uid', $user_ids, 'IN')
        ->execute();
  }

  /**
   * Return all node ids for an user id.
   * 
   * @param integer $uid
   * @return array of integer
   */
  public static function getSubscribedNodeIds($uid) {
    $nids = \Drupal::database()->select('xtwf_node_subscribe', 'nf')
        ->fields('nf', ['nid'])
        ->condition('nf.uid', $uid)
        ->orderBy('nf.changed', 'DESC')
        ->execute()
        ->fetchCol();

    return $nids;
  }

  /**
   * Return all subscribed user ids for a node id.
   * 
   * @param integer $uid
   * @return array of integer
   */
  public static function getSubscribedUserIds($nid) {
    $uids = \Drupal::database()->select('xtwf_node_subscribe', 'nf')
        ->fields('nf', ['uid'])
        ->condition('nf.nid', $nid)
        ->orderBy('nf.changed', 'DESC')
        ->execute()
        ->fetchCol();

    return $uids;
  }

}
